<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Login_model extends CI_Model {
	public function login($username, $password) {
		$this->db->where ( 'email', $username );
		$this->db->where ( 'password', md5 ( $password ) );
		$query = $this->db->get ( 'admin' );
		
		$result = $query->num_rows ();
		
		if ($result == 1) {
			
			$row = $query->row ();
			$data = array (
					'id' => $row->id,
					'email' => $row->email,
					'name' => $row->name,
					'validated' => true 
			);
			$this->session->set_userdata ( $data );
			return true;
		}
	}
	public function client_list() {
		$query = $this->db->query ( 'SELECT * FROM client_info ORDER BY id DESC' );
		return $query->result ();
	}
	public function talent_list() {
		$query = $this->db->query ( 'SELECT * FROM talent_info ORDER BY id DESC' );
		return $query->result ();
	}
	public function talent_list1($talents_id) {
		$this->db->where ( 'talents_id', $talents_id );
		$query = $this->db->get ( 'talent_info' );
		$this->db->order_by ( "id", "desc" );
		
		$result = $query->row ();
		return $result;
	}
	public function talents_info($id) {
		$this->db->where ( 'talents_id', $id );
		$query = $this->db->get ( 'talent_info' );
		$this->db->order_by ( "id", "desc" );
		
		$result = $query->row ();
		return $result;
	}
	
	/* Get the all accepted jobs details */
	public function accepted_jobs() {
		$query = $this->db->query ( "select event_details.id as detail_id,event.id as event_id,event.event_title as event_name,event.event_start as event_start,event.event_end as event_end,event_details.total_hours as total_hours,event_details.talents_cost as talents_cost,event.venue as venue  from event_details
	
			INNER JOIN event on event_details.event_id=event.id
			where  event_details.status=1
	
	
			" );
		
		$result = $query->result_array ();
		return $result;
	}
	
	/* Declined jobs in talent port */
	public function declined_jobs() {
		$query = $this->db->query ( "select event_details.id as detail_id, event.id as event_id,event.event_title as event_name,event.event_start as event_start,event.event_end as event_end,event_details.total_hours as total_hours,event_details.talents_cost as talents_cost,event.venue as venue  from event_details

			INNER JOIN event on event_details.event_id=event.id
			where  (event_details.status=2 OR event_details.status=3)


			" );
		
		$result = $query->result_array ();
		return $result;
	}
	
	/* Event detail page get talent provider,client and event */
	public function event_detail($detail_id) {
		
		$query=$this->db->query("
				select event.event_title as event_title,event.event_type as event_type,event.event_start as event_start,event.event_end as event_end ,event.venue as venue,event.pax as pax,event.attire as attire,event.budget as budget,event_details.status as status,
				event_details.event_id as event_id,
				client_info.firstname as client_name,client_info.email as client_email,client_info.phone as client_phone,client_info.country as client_country ,
				talent_info.name as talent_name,talent_info.phone_number as talent_number,(select talent from talent_category where id=talent_info.act_type) as act_type,talent_info.email as talent_email
				
				from event_details INNER JOIN event on event_details.event_id=event.id
				INNER JOIN talent_info on event_details.talents_id=talent_info.talents_id
				INNER JOIN client_info on event_details.clients_id=client_info.client_id
				
				where event_details.id=$detail_id
				
				
				");
		$result = $query->row();
		return $result;
		
	}
	
	/*Enquiry details from company*/
	public function company_enquiry(){
		
		$query=$this->db->query("select * from company_enquiry");
		return $query->result_array();
	}
	
	/*Enquiry details from talent*/
	public function talent_enquiry(){
	
		$query=$this->db->query("select * from talent_enquiry");
		return $query->result_array();
	}
	
}