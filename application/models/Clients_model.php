<?php
if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Clients_model extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'date' );
	}
	public function check_email($email) {
		$this->db->where ( 'email', $email );
		
		$query = $this->db->get ( 'user' );
		
		$result = $query->num_rows ();
		
		return $result;
	}
	
	/* Verfiy the hash code in restuser table */
	public function verify_hash($hash) {
		$this->db->where ( 'hash', $hash );
		$query = $this->db->get ( 'user' );
		$result = $query->num_rows ();
		if ($result == 1) {
			
			$row = $query->row ();
			$data = array (
					'id' => $row->id,
					'email' => $row->email,
					'updated' => $row->updated,
					'type' => $row->type,
					'validated' => true 
			);
			$this->session->set_userdata ( $data );
			$time = (round ( microtime ( true ) * 1000 ));
			$data = array (
					'status' => 1,
					'last_login' => $time 
			);
			$this->db->where ( 'hash', $hash );
			
			$this->db->update ( 'user', $data );
		}
		return $result;
	}
	
	/* login for talent provider */
	public function login($email, $password) {
		$this->db->where ( 'email', $email );
		$this->db->where ( 'password', md5 ( $password ) );
		$this->db->where ( 'status', 1 );
		$query = $this->db->get ( 'user' );
		$result = $query->num_rows ();
		if ($result == 1) {
			
			$row = $query->row ();
			$data = array (
					'id' => $row->id,
					'updated' => $row->updated,
					'type' => $row->type,
					'email' => $row->email,
					'validated' => true 
			);
			$this->session->set_userdata ( $data );
			return true;
		}
	}
	
	/* Types of talents */
	public function talents_type() {
		$this->db->order_by ( 'talent' );
		$query = $this->db->get ( 'talent_category' );
		
		return $query->result_array ();
	}
	
	/* Check talents id for insert or update function in dashboard section general info part */
	public function check_talents($id) {
		$this->db->where ( 'id', $id );
		
		$query = $this->db->get ( 'client' );
		
		$result = $query->num_rows ();
		
		return $result;
	}
	public function clients_info($clients_id){
	
		$this->db->where('client_id',$this->session->userdata ( 'id' ));
		$query=$this->db->get('client_info');
		$this->db->order_by("id", "desc");
		$result=$query->row();
		return $result;
	
	}
	public function user_info($clients_id){
	
		$this->db->where('client_id',$this->session->userdata ( 'id' ));
		$query=$this->db->get('client_info');
		$result=$query->row();
		return $result;
	
	}
	/*Client Settings*/
	public function setting() {
		$query = $this->db->query ( "SELECT * FROM client_info WHERE client_id = " . $this->session->userdata ( 'id' ) . "" );
		return $query->result ();
	}
	
	/*Upcoming event details list*/
	public function upcomming_event() {
		$query = $this->db->query ( "SELECT * ,(select count(status) from event_details where event_details.event_id=event.id AND event_details.status=0 ) as pending,
				(select count(status) from event_details where event_details.event_id=event.id AND event_details.status=1 ) as accepted,
				(select count(status) from event_details where event_details.event_id=event.id AND (event_details.status=2 || event_details.status=3 ) ) as rejected

				FROM event WHERE client_id = " . $this->session->userdata ( 'id' ) . " and event_end > NOW()" );
		return $query->result ();
	}
	
	/* Past events details */
	public function past_event() {
		$query = $this->db->query ( "SELECT * ,(select count(status) from event_details where event_details.event_id=event.id AND event_details.status=0 ) as pending,
				(select count(status) from event_details where event_details.event_id=event.id AND event_details.status=1 ) as accepted,
				(select count(status) from event_details where event_details.event_id=event.id AND (event_details.status=2 || event_details.status=3 ) ) as rejected

				FROM event WHERE client_id = " . $this->session->userdata ( 'id' ) . " and event_end < NOW()" );
		return $query->result ();
	}
	
	
	/* Edit event retrive data based on id */
	public function event_edit($id) {
		$query = $this->db->query ( "select * from event where id=$id " );
		return $query->result ();
	}
	
	
	/*Event summary page */
	public function event_summary($id){
		
		$query=$this->db->query("select *,(select count(status) from event_details where event_details.event_id=event.id AND event_details.status=0 ) as pending,
				(select count(status) from event_details where event_details.event_id=event.id AND event_details.status=1 ) as accepted,
				(select count(status) from event_details where event_details.event_id=event.id AND (event_details.status=2 || event_details.status=3 ) ) as rejected
				from event where id=$id");
		return $query->row();		
		
		
	}
	
	
	/*Hire me page show price package details based on talent id */
	public function pricing_detail($talents_id){
		
		$query=$this->db->query("select * from talents_pricing where talents_id=$talents_id");
		return $query->result_array();
	}
	
	
	/*Hire talents insert before check this talents have same event or not */
	public function hiretalent_check($event_id,$talent_id){
		
		
		
		$query = $this->db->query ( "select * from event_details where event_id=$event_id AND talents_id=$talent_id AND (status=0 || status=1)" );
		$result = $query->num_rows ();
		
		return $result;
	}
	
	
	/*List out talents list in event summary page who are all hired for particular event */
	public function talents_list($event_id){
		
	 	$query=$this->db->query("select *,(select name from talent_info where talent_info.talents_id=event_details.talents_id) as talent_name,(select talent from talent_category where id=(select act_type from talent_info where talent_info.talents_id=event_details.talents_id)) as cat_name from event_details where event_id=$event_id ");
	    return  $query->result_array();
	
	}
	
	/*Get details for send mail to hie\red talents from hire me final page */
	public function get_talentdetalils($talent_id,$client_id,$event_id){
		
		$query=$this->db->query("select email,(select name from talent_info where talents_id=$talent_id) as talent_name,(select firstname from client_info where client_id=$client_id) as client_name,(select event_title from event where id=$event_id) as event_name from user where id=$talent_id");
		$result=$query->row();
		return $result;
		
	}
	
	
}