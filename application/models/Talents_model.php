<?php

if (! defined ( 'BASEPATH' ))
	exit ( 'No direct script access allowed' );
class Talents_model extends CI_Model {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( 'date' );
	}
	public function check_email($email) {
		$this->db->where ( 'email', $email );
		
		$query = $this->db->get ( 'user' );
		
		$result = $query->num_rows ();
		
		return $result;
	}
	
	
	/* Verfiy the hash code in restuser table */
	
	public function verify_hash($hash){
			
		$this->db->where('hash', $hash);
		$query = $this->db->get('user');
		$result=$query->num_rows();
		if($result == 1){
	
			$row = $query->row();
			$data = array(
					'id'=>$row->id,
					'email'=>$row->email,
					'type'    =>$row->type,
					'validated' => true
			);
			$this->session->set_userdata($data);
			$time=(round(microtime(true) * 1000));
			$data=array('status'=>1,'last_login'=>$time);
			$this->db->where('hash',$hash);
			$this->db->update('user',$data);
		}
		return $result;
			
	}
	
	
	/*login for talent provider  */
	public  function login($email,$password){
			
		$this->db->where('email', $email);
		$this->db->where('password',md5($password));
		$this->db->where('status',1);
		$query = $this->db->get('user');
		$result=$query->num_rows();
		if($result == 1){
	
			$row = $query->row();
			$data = array(
					'id'=>$row->id,
					'updated'=>$row->updated,
					'type'    =>$row->type,
					'email'=>$row->email,
					'validated' => true
			);
			$this->session->set_userdata($data);
			return true;
	
		}
			
	}
	
	/*Types of category */
	
	public function talents_category(){
	
		$this->db->order_by('id');
		$query=$this->db->get('talent_maincategory');
	
		return $query->result_array();
	}
	
	/*Types of actyype search filter */
	
	public function talents_searchcategory(){
	
		$this->db->order_by('maincategory_id');
		$query=$this->db->get('talent_category');
	
		return $query->result_array();
	}
	/*Types of talents */
	
	public function talents_type($cat_id){
		
		$this->db->where('maincategory_id',$cat_id);
		$this->db->order_by('talent');
		$query=$this->db->get('talent_category');
		
		return $query->result_array();
	}
	
	/*Types of talents dashboard page */
	
	public function talents_type1(){
	
		
		$this->db->order_by('talent');
		$query=$this->db->get('talent_category');
	
		return $query->result_array();
	}
	
	/*Check talents id for insert or update function in dashboard section general info part */
	public function check_talents($id){
		
		$this->db->where ( 'talents_id', $id );
		
		$query = $this->db->get ( 'talent_info' );
		
		$result = $query->num_rows ();
		
		return $result;
		
		
	}
	
	/*Talents info details */
	
	public function talents_info($talents_id){
		
		$query=$this->db->query("select *,(select name from talent_maincategory where id=talent_info.category) as category_name from talent_info where talents_id=$talents_id");
		
		$result=$query->row();
		return $result;
		
	}
	
	/*Talents video details */
	
	public function videos($talents_id){
	
		$this->db->where('talents_id',$talents_id);
		$query=$this->db->get('talent_video');
	
		return $query->result_array();
	
	}
	
	/*Clients*/
	public function clients($id){
		
		$this->db->where('talents_id',$id);
		$query=$this->db->get('talents_clients');
		$this->db->order_by("id", "desc");
		return $query->result_array();
		
		
		
	}
	
	/*Client Edit*/
	
	public function client_edit($id){
		
		
		$this->db->where('id',$id);
		$query=$this->db->get('talents_clients');
		
		
		$result=$query->row();
		return $result;
		
	}
	
	/*Get price details based on user id*/
	
	public function price_package($talents_id){
		
		
		$this->db->where('talents_id',$talents_id);
		$query=$this->db->get('talents_pricing');
		
		return $query->result_array();
	}
	
	
	/*Talents list for home page */
	
public function talents_list(){
	
	$query=$this->db->query("select talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category FROM talent_info LEFT OUTER JOIN talent_maincategory ON talent_info.category=talent_maincategory.id");
	
	return $query->result_array();
	
	
   }
   
   /*Search page list out talents list based on serach key*/
   public function talents_list1($search_key,$category, $price, $exp){
   
   	$query=$this->db->query("select *,talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category,(select charge_type from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as charge_type,(select price from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as hr_price FROM talent_info LEFT JOIN talent_maincategory ON talent_info.category=talent_maincategory.id  WHERE talent_maincategory.name LIKE '%$search_key%'");
   
   	return $query->result_array();
   
   
   }

   /*Search page list out talents list based on serach key*/
   public function talents_search($search_key,$category, $price, $exp){
   	 
   	$query=$this->db->query("select *,talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category,(select charge_type from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as charge_type,(select price from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as hr_price FROM talent_info LEFT JOIN talent_maincategory ON talent_info.category=talent_maincategory.id  WHERE talent_maincategory.name LIKE '%$search_key%'");
   	 
   	return $query->result_array();
   	 
   	 
   }
   /*Search page list out talents list based on serach key*/
   public function talents_list_search($search_title){
   	 
   	$query=$this->db->query("select *,talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category,talent_category.talent as subcategory,(select charge_type from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as charge_type,(select price from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as hr_price FROM talent_info LEFT JOIN talent_maincategory ON talent_info.category=talent_maincategory.id LEFT JOIN talent_category ON talent_info.act_type=talent_category.id WHERE talent_maincategory.name LIKE '%$search_title%' or talent_info.name LIKE '%$search_title%' or talent_category.talent LIKE '%$search_title%'");
   	 
   	return $query->result_array();
   	 
   	 
   }
   /*Search page list out talents list based on filters key*/
   public function talents_list_filter($category,$minexp,$maxexp,$minprice,$maxprice){
   	 if($category !=""){
   	$query=$this->db->query("select *,talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category,(select charge_type from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as charge_type,(select price from talents_pricing where talents_id=talent_info.talents_id and talents_pricing.price BETWEEN '$minprice' AND '$maxprice' LIMIT 1) as hr_price FROM talent_info LEFT JOIN talent_maincategory ON talent_info.category=talent_maincategory.id WHERE talent_info.act_type='$category' and talent_info.experience BETWEEN '$minexp' AND '$maxexp'");
   	return $query->result_array();
   	 }
   	 else {
   	 	$query=$this->db->query("select *,talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category,(select charge_type from talents_pricing where talents_id=talent_info.talents_id LIMIT 1) as charge_type,(select price from talents_pricing where talents_id=talent_info.talents_id and talents_pricing.price BETWEEN '$minprice' AND '$maxprice' LIMIT 1) as hr_price FROM talent_info LEFT JOIN talent_maincategory ON talent_info.category=talent_maincategory.id WHERE talent_info.experience BETWEEN '$minexp' AND '$maxexp'");
   	 	return $query->result_array();
   	 }
   }
   /*Particular talents details */
   
  public function talents_detail($talents_id){
  	
  	$query=$this->db->query("select talent_info.id as id ,talent_info.talents_id as talents_id,talent_info.name as name,talent_maincategory.name as category,talent_info.service as service FROM talent_info LEFT JOIN talent_maincategory ON talent_info.category=talent_maincategory.id where talent_info.talents_id=$talents_id");
  	
  	return $query->row();
  	
  	
  }
  
  /*Talents video part */
  
  public function talents_video($talents_id){
  	
  	$this->db->where('talents_id',$talents_id);
  	$query=$this->db->get('talent_video');
  	return  $query->result_array();
  }
  
  /*Pending jobs in talent port */
  
  public function pending_jobs($talents_id){
  	
  	$query=$this->db->query("select event.id as event_id,event.event_title as event_name,event.event_start as event_start,event.event_end as event_end,event_details.total_hours as total_hours,event_details.talents_cost as talents_cost,event.venue as venue  from event_details
  			
  			INNER JOIN event on event_details.event_id=event.id
  			where event_details.talents_id=$talents_id AND event_details.status=0 
  			
  			
  			");
  	
  	$result=$query->result_array();
  	return $result;
  }
  
  /*Accepted jobs in talent port */
  
  public function accepted_jobs($talents_id){
  	 
  	$query=$this->db->query("select event.id as event_id,event.event_title as event_name,event.event_start as event_start,event.event_end as event_end,event_details.total_hours as total_hours,event_details.talents_cost as talents_cost,event.venue as venue  from event_details
  				
  			INNER JOIN event on event_details.event_id=event.id
  			where event_details.talents_id=$talents_id AND event_details.status=1
  				
  				
  			");
  	 
  	$result=$query->result_array();
  	return $result;
  }
  
  /*Declined jobs in talent port */
  
  public function declined_jobs($talents_id){
  	 
  	$query=$this->db->query("select event.id as event_id,event.event_title as event_name,event.event_start as event_start,event.event_end as event_end,event_details.total_hours as total_hours,event_details.talents_cost as talents_cost,event.venue as venue  from event_details
  				
  			INNER JOIN event on event_details.event_id=event.id
  			where event_details.talents_id=$talents_id AND (event_details.status=2 OR event_details.status=3)
  				
  				
  			");
  	 
  	$result=$query->result_array();
  	return $result;
  }
  
  
  /*Get accepted events */
  public function get_events($start,$end)
  {
  	$talents_id=$this->session->userdata('id');
  	
  	$query=$this->db->query("select event.id as event_id,event.event_title as event_name,event.event_start as event_start,event.event_end as event_end,event_details.total_hours as total_hours,event_details.talents_cost as talents_cost,event.venue as venue  from event_details
  	
  			INNER JOIN event on event_details.event_id=event.id
  			where event_details.talents_id=$talents_id AND (event_details.status=1) 
  	
  	
  			");
  	
  	$result=$query->result();
  	return $result;
  	
  }
  
  
	
}
