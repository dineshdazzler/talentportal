<?php 
class My404 extends CI_Controller 
{
    public function __construct() 
    {
        parent::__construct(); 
        
        $this->load->model ( 'talents_model' );
    } 

    public function index() 
    { 
       if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==1){
       	$data ['email'] = $this->session->userdata ( 'email' );
       	$data ['id'] = $this->session->userdata ( 'id' );
       	$data ['type'] = $this->session->userdata ( 'type' );
       	$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
       	
       	$this->load->view('talents_head',$data);
       	$this->load->view ( 'error', $data );
       	$this->load->view ( 'footer', $data );
       }else if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==2){
       	$data ['email'] = $this->session->userdata ( 'email' );
       	$data ['id'] = $this->session->userdata ( 'id' );
       	$data ['type'] = $this->session->userdata ( 'type' );
       	$this->load->view('client/client_head',$data);
       	$this->load->view ( 'error', $data );
       	$this->load->view ( 'footer', $data );
       	
       }else {
       	$data['username']="";
       	$data['id']		="";
       	$data['admin']="";
       	$this->load->view('head',$data);
       	$this->load->view('error');
       	$this->load->view('footer',$data);
       	
       }
    } 
} 
?> 