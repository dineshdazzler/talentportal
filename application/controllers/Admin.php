<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {
	
	var $data;
	//
	function __construct() {
		parent::__construct ();
		$this->load->helper ( array (
				'form',
				'url',
				'html',
				'date'
		) );
		$this->load->library ( array (
				'form_validation',
				'session'
		) );
		$this->load->model ( 'login_model' );
		$this->load->model ( 'talents_model' );
		$this->data = array(
				'admin' => 'dineshdazzler93@gmail.com'
				
		);

	}
	/**
	 * First controller

	 */
	public function index()
	{
		if(! $this->session->userdata ( 'validated' )){
				$this->load->view('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==2 && $this->session->userdata('updated')==1){
			$this->session->sess_destroy();
			$this->load->view('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && ! $this->session->userdata('type')==2 && ! $this->session->userdata('updated')==1){
			$data['name']=$this->session->userdata('name');
			$data['email']=$this->session->userdata('email');
			$data['id']=$this->session->userdata('id');
			$this->load->view('admin/admin_head',$data);
			$this->load->view('admin/dashboard',$data);
			$this->load->view('admin/admin_footer',$data);
		}

	}
	public function client()
	{
		
		
			$data['name']=$this->session->userdata('name');
			$data['email']=$this->session->userdata('email');
			$data['id']=$this->session->userdata('id');
			$data ['client_list'] = $this->login_model->client_list ();
			$this->load->view('admin/admin_head',$data);
			$this->load->view('admin/client_list',$data);
			$this->load->view('admin/admin_footer',$data);
		
	
	}
	
	public function talent()
	{
	
	
		$data['name']=$this->session->userdata('name');
		$data['email']=$this->session->userdata('email');
		$data['id']=$this->session->userdata('id');
		$data ['talent_list'] = $this->login_model->talent_list ();
		$this->load->view('admin/admin_head',$data);
		$this->load->view('admin/talent_list',$data);
		$this->load->view('admin/admin_footer',$data);
	
	
	}
	
	/* talent delete section */
	public function  delete_talent(){
		$t_id=$this->input->post('id');
	
		$this->db->where('id',$t_id);
		$this->db->delete('talent_info');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
	
	}
	/* client delete section */
	public function  delete_client(){
		$t_id=$this->input->post('id');
	
		$this->db->where('id',$t_id);
		$this->db->delete('client_info');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
	
	}
	
	/* Login check
	 *
	 */

	public function login_check(){

		$username=$this->input->post('username');
		$password=$this->input->post('password');

		$result=$this->login_model->login($username,$password);

		if(!$result){

			echo "fail";
		}else{

			echo "success";
		}
			

	}

	/*
	 * Logout function
	 */

	public function do_logout(){

		$this->session->sess_destroy();
		redirect('admin');
	}

	public function test(){
		$this->load->view('header');
		$this->load->view('test');
		$this->load->view('footer');
	}
	
	
	public function approve(){
		$id=$this->input->post('id');
		$approve = array('status' => 1);
		$this->db->where('talents_id', $id);
		$this->db->update('talent_info', $approve);
	}
	
	
	public function decline(){
		$id=$this->input->post('id');
		$status = 0;
		$approve = array('status' => 0);
		$this->db->where('talents_id', $id);
		$this->db->update('talent_info', $approve);
	}
	
	
	public function client_approve(){
		$id=$this->input->post('id');
		$approve = array('status' => 1);
		$this->db->where('client_id', $id);
		$this->db->update('client_info', $approve);
	}
	
	
	public function client_decline(){
		$id=$this->input->post('id');
		$status = 0;
		$approve = array('status' => 0);
		$this->db->where('client_id', $id);
		$this->db->update('client_info', $approve);
	}
	
	
	public function talent_edit()
	{
		
			$id=$this->uri->segment(3);
			$data ['talents_type'] = $this->talents_model->talents_type1 ();
			$data ['talents_info'] = $this->login_model->talents_info ($id);
			$data ['id'] = $id=$this->uri->segment(3);
			$this->load->view('admin/admin_head',$data);
			$this->load->view('admin/talent_edit',$data);
			$this->load->view('admin/admin_footer',$data);
		
	}
	
	
	public function client_edit()
	{
	
		$id=$this->uri->segment(3);
		$data ['client_type'] = $this->talents_model->talents_type1 ();
		$data ['client_info'] = $this->login_model->talents_info ($id);
		$data ['id'] = $id=$this->uri->segment(3);
		$this->load->view('admin/admin_head',$data);
		$this->load->view('admin/talent_edit',$data);
		$this->load->view('admin/admin_footer',$data);
	
	}
	
	
	public function talent_portedit()
	{
	
		$talents_id=$this->uri->segment(3);
		$data ['talents_type'] = $this->talents_model->talents_type1 ();
		$data ['talents_category'] = $this->talents_model->talents_category();
		$data ['talents_info'] = $this->talents_model->talents_info ($talents_id);
		$data ['talent_list1'] = $this->login_model->talent_list1($talents_id);
		$data ['email'] = $this->session->userdata ( 'email' );
		$data ['id'] = $this->session->userdata ( 'id' );
		$data['videos']=$this->talents_model->videos($talents_id);
		$data['clients']=$this->talents_model->clients($talents_id);
		$data['price']=$this->talents_model->price_package($talents_id);
		$data ['id'] = $id=$this->uri->segment(3);
		$data ['type'] = $this->session->userdata ( 'type' );
		$this->load->view('admin/admin_head',$data);
		$this->load->view('admin/talent_port',$data);
		$this->load->view('admin/admin_footer',$data);
	
	}
	
	
	public function loaddata()
	{
		$this->load->model ( 'talents_model' );
		$loadType=$_POST['loadType'];
		$loadId=$_POST['loadId'];
		$id=$_POST['id'];
		$result=$this->talents_model->talents_type($loadId);
		$HTML="";
	
		foreach($result as $row){
	
			if($id==$row['id'])
			{
				$HTML.="<option selected  value='".$row['id']."'>".$row['talent']."</option>";
	
			}else{
	
				$HTML.="<option selected  value='".$row['id']."'>".$row['talent']."</option>";
			}
		}
		$HTML.="<option  value='others'>Others</option>";
	
		echo $HTML;
	
	
	}
	
	
	/*Event management function*/
	public function event(){
		
		if(! $this->session->userdata ( 'validated' )){
			redirect('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==2 && $this->session->userdata('updated')==1){
			$this->session->sess_destroy();
			redirect('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && ! $this->session->userdata('type')==2 && ! $this->session->userdata('updated')==1){
			$data['name']=$this->session->userdata('name');
			$data['email']=$this->session->userdata('email');
			$data['id']=$this->session->userdata('id');
			$data['accepted_jobs']=$this->login_model->accepted_jobs();
			$data['declined_jobs']=$this->login_model->declined_jobs();
			$this->load->view('admin/admin_head',$data);
			$this->load->view('admin/event',$data);
			$this->load->view('admin/admin_footer',$data);
		}
		
	}
	
	/*Event detail view page */
	
	public function event_detail(){
		
		$detail_id=$this->uri->segment(3);

		if(! $this->session->userdata ( 'validated' )){
			redirect('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==2 && $this->session->userdata('updated')==1){
			$this->session->sess_destroy();
			redirect('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && ! $this->session->userdata('type')==2 && ! $this->session->userdata('updated')==1){
			$data['name']=$this->session->userdata('name');
			$data['email']=$this->session->userdata('email');
			$data['id']=$this->session->userdata('id');
			$data['event_detail']=$this->login_model->event_detail($detail_id);
			
			$this->load->view('admin/admin_head',$data);
			$this->load->view('admin/event_detail',$data);
			$this->load->view('admin/admin_footer',$data);
		}
		
	}
	
	/*List out the enquiry details */
	public function enquiry(){
		if(! $this->session->userdata ( 'validated' )){
			$this->load->view('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==2 && $this->session->userdata('updated')==1){
			$this->session->sess_destroy();
			redirect('admin/login');
		}
		else if($this->session->userdata ( 'validated' ) && ! $this->session->userdata('type')==2 && ! $this->session->userdata('updated')==1){
			$data['name']=$this->session->userdata('name');
			$data['email']=$this->session->userdata('email');
			$data['id']=$this->session->userdata('id');
			$data=$this->data;
			$data['company']=$this->login_model->company_enquiry();
			$data['talent']=$this->login_model->talent_enquiry();
			$this->load->view('admin/admin_head',$data);
			$this->load->view('admin/enquiry_details',$data);
			$this->load->view('admin/admin_footer',$data);
		}
	}
	
}