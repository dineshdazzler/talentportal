<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
header ( 'Access-Control-Allow-Origin: *' );
header('X-Frame-Options: GOFORIT');
class Talents extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( array (
				'form',
				'url',
				'html',
				'date' 
		) );
		$this->load->library ( array (
				'form_validation',
				'session' 
		) );
		$this->load->model ( 'talents_model' );
		$this->load->library ( 'email' );
	}
	
	/**
	 * Home page
	 */
	public function index() {
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'home', $data );
			$this->load->view ( 'footer', $data );
		} else if($this->session->userdata ( 'validated' ) && $this->session->userdata('type')==1){
			
			redirect ( 'talents/talents_profile' );
		}
		else{
			redirect('admin');
		}
	}
	
	/* Signup process for talents */
	public function signup() {
		$email = $this->input->post ( 'signup_email' );
		$password = md5 ( $this->input->post ( 'signup_password' ) );
		$type = $this->input->post ( 'type' );
		$reg_date = date ( 'Y-m-d H:i:s' );
		$email_exists = $this->talents_model->check_email ( $email );
		
		if ($email_exists == 0) {
			
			$data = array (
					'email' => $email,
					'password' => $password,
					'type' => $type,
					'reg_date' => $reg_date,
					'hash' => md5 ( $email ) 
			);
			$hash=md5($this->input->post ( 'signup_email' ));
			$this->mailsend($email,$hash);
			$this->db->insert ( 'user', $data );
			$user_id = $this->db->insert_id ();
			$result=$this->insert_talentsinfo($user_id);
			return $result;
			
			
		} else {
			
			echo "exists";
		}
	}
	/* Mail send function for email verification without using smtp */
	public function mailsend($email,$hash) {
		
		
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='localhost';
		$config['smtp_port']='25';
		$config['smtp_secure']='none';
		$config['smtp_timeout']='30';
		$config['smtp_auth']=FALSE;
		$config['smtp_user']='dinesh@inoble.in';
		$config['smtp_pass']='Welcome@123';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['crlf'] = '\r\n';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from ( 'dinesh@inoble.in', 'Talent Portal' );
		$this->email->to ($email);
		$this->email->subject ( 'Talent Portal email Verification for Talent Provider account' );
		$data['link']="http://inoble.in/talentportal/talents/verify/" . $hash ."";
		$data['mail']=$email;
		$data['type']="signup";
		$this->email->message($this->load->view('verify_emailtemplate',$data, TRUE));
		
		if(!$this->email->send()){
			echo "failure";
			echo $this->email->print_debugger();
			
		}
		//echo $this->email->print_debugger();
		
	}
	
	/*insert blank talents info first time after signup*/
	public function insert_talentsinfo($id){
		
		$data=array(
				
				'talents_id'=>$id
				
		);
		
		$this->db->insert('talent_info',$data);
		echo "success";
	}
	

	/* Verfication for user account and activate the user account */
	public function verify() {
		$hash = $this->uri->segment (3);
		
		$result = $this->talents_model->verify_hash ( $hash );
		if($result == 1){
		
			if(! $this->session->userdata('validated')){
				$data['username']="";
				$data['id']		="";
				$data['admin']="";
				$this->load->view('head',$data);
				$this->load->view('home');
				$this->load->view('footer',$data);
			}else{
				redirect('talents/talents_profile');
			}
		}
	}
	
	/* user login and get values from verify page */
	public function login() {
		$email = $this->input->post ( 'email' );
		$password = $this->input->post ( 'password' );
		$count = $this->talents_model->login ( $email, $password );
		
		if (! $count) {
			echo "failure";
		} else {
			$type = $this->session->userdata ( 'type' );
			echo $type;
			$this->db->where ( 'email', $email );
			$time = (round ( microtime ( true ) * 1000 ));
			$data = array (
					'last_login' => $time 
			);
			$this->db->update ( 'user', $data );
		}
	}
	/* Talents dashboard section */
	public function talents_profile() {
		$data ['talents_type'] = $this->talents_model->talents_type1 ();
		
		$data ['talents_category'] = $this->talents_model->talents_category();
		
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$data ['talents_list'] = $this->talents_model->talents_list ();
			$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
			$data['videos']=$this->talents_model->videos($this->session->userdata ( 'id' ));
			$data['price']=$this->talents_model->price_package($this->session->userdata ( 'id' ));
			$this->load->view ( 'talents_head', $data );
			$this->load->view ( 'talents/profile', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	/* Password update */
	public function  password_update(){
	
		$talent_id=$this->input->post('tal_id');
		$password =  md5 ( $this->input->post ( 'npass' ) );
		$data=array(
				'password'=>$password,
		);
	
		$this->db->where('id', $talent_id);
		$this->db->update('user', $data);
	
	
	}
	/* Talents genrel info insertion */
	public function generalinfo_insert() {
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$talents_id = $this->input->post ( 't_id' );
			$name = $this->input->post ( 'name' );
			$contact_number = $this->input->post ( 'contact_number' );
			$act_type = $this->input->post ( 'act_type' );
			$email = $this->input->post ( 'email' );
			
			$dob = $this->input->post ( 'dob' );
			$gender = $this->input->post ( 'gender' );
			$address = $this->input->post ( 'address' );
			$check_id = $this->talents_model->check_talents ( $talents_id );
			
			if ($check_id == 1) {
				$data = array (
						
						'name' => $name,
						'act_type' => $act_type,
						'phone_number' => $contact_number,
						'address' => $address,
						'email' => $email,
						'dob' => $dob,
						'gender' => $gender 
				)
				;
				$this->db->where ( 'talents_id', $talents_id );
				$this->db->update ( 'talent_info', $data );
				echo "success";
			} else {
				
				$data = array (
						
						'talents_id' => $talents_id,
						'name' => $name,
						'act_type' => $act_type,
						'phone_number' => $contact_number,
						'address' => $address,
						'email' => $email,
						'dob' => $dob,
						'gender' => $gender 
				)
				;
				
				$this->db->insert ( 'talent_info', $data );
				echo "success";
			}
		}
	}
	
	/* Bank info insert and updation */
	public function bankinfo_insert() {
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$talents_id = $this->input->post ( 't1_id' );
			$bank_name = $this->input->post ( 'bank_name' );
			$ac_type = $this->input->post ( 'ac_type' );
			$ac_number = $this->input->post ( 'ac_number' );
			$namein_bank = $this->input->post ( 'namein_bank' );
			$check_id = $this->talents_model->check_talents ( $talents_id );
			
			if ($check_id == 1) {
				$data = array (
						
						'bank_name' => $bank_name,
						'account_type' => $ac_type,
						'account_number' => $ac_number,
						'namein_bank' => $namein_bank 
				)
				;
				$this->db->where ( 'talents_id', $talents_id );
				$this->db->update ( 'talent_info', $data );
				echo "1";
			} else {
				
				$data = array (
						
						'talents_id' => $talents_id,
						'bank_name' => $bank_name,
						'account_type' => $ac_type,
						'account_number' => $ac_number,
						'namein_bank' => $namein_bank 
				)
				;
				
				$this->db->insert ( 'talent_info', $data );
				echo "2";
			}
		}
	}
	
	/* Performance info insert or update function */
	public function performanceinfo_insert() {
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$talents_id = $this->input->post ( 't_id' );
			$requirement = $this->input->post ( 'requirement' );
			
			
			$check_id = $this->talents_model->check_talents ( $talents_id );
			
			if ($check_id == 1) {
				$data = array (
						
						'logisitical' => $requirement
						
				)
				;
				$this->db->where ( 'talents_id', $talents_id );
				$this->db->update ( 'talent_info', $data );
				echo "1";
			} else {
				
				$data = array (
						
						'talents_id' => $talents_id,
						'logisitical' => $requirement
						
				)
				;
				
				$this->db->insert ( 'talent_info', $data );
				echo "2";
			}
		}
	}
	
	/* Talents Portfolio section */
	public function talents_dashboard() {
		$data ['talents_type'] = $this->talents_model->talents_type1 ();
		$data ['talents_category'] = $this->talents_model->talents_category();
		$data['uri']=$this->uri->segment(3);
	
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data['talents_video']=$this->talents_model->videos($this->session->userdata ( 'id' ));
			$data['clients']=$this->talents_model->clients($this->session->userdata ( 'id' ));
			$data['price']=$this->talents_model->price_package($this->session->userdata ( 'id' ));
			$data['pending_jobs']=$this->talents_model->pending_jobs($this->session->userdata ( 'id' ));
			$data['accepted_jobs']=$this->talents_model->accepted_jobs($this->session->userdata ( 'id' ));
			$data['declined_jobs']=$this->talents_model->declined_jobs($this->session->userdata ( 'id' ));
			$data ['type'] = $this->session->userdata ( 'type' );
			$this->load->view ( 'talents_head', $data );
			$this->load->view ( 'talents/dashboard', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	
	/*Discover page section*/
	public function discover() {
		$data ['talents_type'] = $this->talents_model->talents_type1 ();
		$data ['talents_category'] = $this->talents_model->talents_category();
		$data['uri']=$this->uri->segment(3);
	
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data['talents_video']=$this->talents_model->videos($this->session->userdata ( 'id' ));
			$data['clients']=$this->talents_model->clients($this->session->userdata ( 'id' ));
			$data['price']=$this->talents_model->price_package($this->session->userdata ( 'id' ));
			$data['pending_jobs']=$this->talents_model->pending_jobs($this->session->userdata ( 'id' ));
			$data['accepted_jobs']=$this->talents_model->accepted_jobs($this->session->userdata ( 'id' ));
			$data['declined_jobs']=$this->talents_model->declined_jobs($this->session->userdata ( 'id' ));
			$data ['type'] = $this->session->userdata ( 'type' );
			$this->load->view ( 'talents_head', $data );
			$this->load->view ( 'talents/discover', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	
	/*Support page section*/
	public function support() {
		$data ['talents_type'] = $this->talents_model->talents_type1 ();
		$data ['talents_category'] = $this->talents_model->talents_category();
		$data['uri']=$this->uri->segment(3);
	
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data['talents_video']=$this->talents_model->videos($this->session->userdata ( 'id' ));
			$data['clients']=$this->talents_model->clients($this->session->userdata ( 'id' ));
			$data['price']=$this->talents_model->price_package($this->session->userdata ( 'id' ));
			$data['pending_jobs']=$this->talents_model->pending_jobs($this->session->userdata ( 'id' ));
			$data['accepted_jobs']=$this->talents_model->accepted_jobs($this->session->userdata ( 'id' ));
			$data['declined_jobs']=$this->talents_model->declined_jobs($this->session->userdata ( 'id' ));
			$data ['type'] = $this->session->userdata ( 'type' );
			$this->load->view ( 'talents_head', $data );
			$this->load->view ( 'talents/support', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	
	/*Contact page section*/
	public function contact() {
		$data ['talents_type'] = $this->talents_model->talents_type1 ();
		$data ['talents_category'] = $this->talents_model->talents_category();
		$data['uri']=$this->uri->segment(3);
	
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data['talents_video']=$this->talents_model->videos($this->session->userdata ( 'id' ));
			$data['clients']=$this->talents_model->clients($this->session->userdata ( 'id' ));
			$data['price']=$this->talents_model->price_package($this->session->userdata ( 'id' ));
			$data['pending_jobs']=$this->talents_model->pending_jobs($this->session->userdata ( 'id' ));
			$data['accepted_jobs']=$this->talents_model->accepted_jobs($this->session->userdata ( 'id' ));
			$data['declined_jobs']=$this->talents_model->declined_jobs($this->session->userdata ( 'id' ));
			$data ['type'] = $this->session->userdata ( 'type' );
			$this->load->view ( 'talents_head', $data );
			$this->load->view ( 'talents/contact', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	public function loaddata()
	{
		
		$loadType=$_POST['loadType'];
		$loadId=$_POST['loadId'];
	 	$id=$_POST['id'];
		$result=$this->talents_model->talents_type($loadId);
		$HTML="";
	
	    foreach($result as $row){
	    	
	    	if($id==$row['id'])
	    	{
	    		$HTML.="<option selected  value='".$row['id']."'>".$row['talent']."</option>";
	    	
	    	}else{
	    	
	    	$HTML.="<option  value='".$row['id']."'>".$row['talent']."</option>";
	    	}
	    }
	    $HTML.="<option  value='others'>Others</option>";
		
		echo $HTML;
		
		
    }
		
    public function loaddata1()
    {
    
    	$loadType=$_POST['loadType'];
    	$loadId=$_POST['loadId'];
    	$id=$_POST['id'];
    	$result=$this->talents_model->talents_type($loadId);
    	$HTML="";
    
    	foreach($result as $row){
    
    		if($id==$row['id'])
    		{
    			$HTML.=$row['talent'];
    
    		}
    	}
    	
    
    	echo $HTML;
    
    
    }
		
	
	
	/* Talents portfolio and experience update */
	public function portfolio_update() {
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$talents_id = $this->input->post ( 't_id' );
			$category = $this->input->post ( 'category' );
			
			$exp = $this->input->post ( 'exp' );
			$editor1 = $this->input->post ( 'act_service' );
			$other=$this->input->post('other');
			$file=$_FILES['pdf']['name'];
			
			if($other != ''){
			
				$other_result=$this->act_typeadd($category,$other);
				$act_type=$other_result;
				
			}else{
				$act_type = $this->input->post ( 'act_type' );
			}
			
			$check_id = $this->talents_model->check_talents ( $talents_id );
			
			if ($check_id == 1) {
				$data = array (
						
						'act_type' => $act_type,
						'service' => $editor1,
						'experience' => $exp ,
						'category'=>$category
				)
				;
				$this->db->where ( 'talents_id', $talents_id );
				$this->db->update ( 'talent_info', $data );
				echo "1";
			} else {
				
				$data = array (
						
						'talents_id' => $talents_id,
						'act_type' => $act_type,
						'service' => $editor1,
						'experience' => $exp ,
						'category'=>$category
				)
				;
				
				$this->db->insert ( 'talent_info', $data );
				echo "2";
			}
			
			
			//documents uploads
			if (! empty ( $_FILES ['pdf'] ['name'] )) {
			$res_folder="talents".$talents_id."";
			if (!is_dir('talents_images/'.$res_folder.'/portfolio')) {
				mkdir('talents_images/' . $res_folder.'/portfolio', 0777, TRUE);
			
			}
			$path ='talents_images/'.$res_folder.'/portfolio/';
			$time=(round(microtime(true) * 1000));
				
			if ( 0 < $_FILES['pdf']['error'] ) {
				echo 'Error: ' . $_FILES['pdf']['error'] . '<br>';
			}
			else {
				move_uploaded_file($_FILES['pdf']['tmp_name'], $path."/" . $_FILES['pdf']['name']);
			}
			
			}
			
			
			
			
		}
	}
	
	/*add act type*/
	public function act_typeadd($category,$other){
		
	   $data=array(
	   	'talent'=>$other,
	   	'maincategory_id'=>$category
	   		
	   );
	   $this->db->insert('talent_category',$data);
	   $other_id = $this->db->insert_id ();
	   return $other_id;
	}
	
	/* you tube insertion */
	public function youtube_insert() {
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			
			$talents_id = $this->input->post ( 't_id' );
			$youtube_link = $this->input->post ( 'youtube_link' );
			$org_link=preg_replace("/\s*[a-zA-Z\/\/:\.]*youtube.com\/watch\?v=([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i","<iframe width=\"300\" height=\"215\" src=\"//www.youtube.com/embed/$1\" frameborder=\"0\" allowfullscreen></iframe>",$youtube_link);
			$data=array(
					
				'talents_id'=>$talents_id,
					'link'=>$org_link
			);
			
			$this->db->insert('talent_video',$data);
			
			$data['videos']=$this->talents_model->videos($talents_id);
			$this->load->view('talents/youtube_ajax_view',$data);
			
			
		}
	}
	
	/*you tube delete*/
	
	public function youtube_delete(){
		
		$id=$this->input->post('id');
		
		$this->db->where('id',$id);
		$this->db->delete('talent_video');
		echo 1;
	}
	
	/* Profile photo change add */
	public function ajax_add() {
		$validextensions = array (
				"jpeg",
				"jpg",
				"png" 
		); // Extensions which are allowed
		$ext = explode ( '.', basename ( $_FILES ['photo'] ['name'] ) ); // explode file name from dot(.)
		$file_extension = end ( $ext );
		
		$picname = "talent" . $_POST ['tid'] . "pic";
		$path = 'talents_profilepic/' . $picname;
		$files = glob ( 'talents_profilepic/*' ); // get all file names
		foreach ( $files as $file ) {
			if (is_file ( $file )) {
				$genres = explode ( '/', $file );
				$genres = explode ( '.', $genres [1] );
				if ($genres [0] == $picname) {
					unlink ( $file );
				}
				
				// unlink($file);
			} // delete file
		}
		
		if (! empty ( $_FILES ['photo'] ['name'] )) {
			if (($_FILES ["photo"] ["size"] < 100000000) && // Approx. 100kb files can be uploaded.
in_array ( $file_extension, $validextensions )) {
				if ($_FILES ['photo'] ['size'] > 1048576) {
					if (compress ( $_FILES ['photo'] ['tmp_name'], $path, 70 )) { // if file moved to uploads folder
						echo json_encode ( array (
								"status" => TRUE,
								"msg" => "Photo Uploaded Successfully" 
						) );
					} else { // if file was not moved.
						echo json_encode ( array (
								"status" => FALSE,
								"msg" => "Photo Not Uploaded, Please try again!" 
						) );
					}
				} else {
					if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], $path )) { // if file moved to uploads folder
						echo json_encode ( array (
								"status" => TRUE,
								"msg" => "Photo Uploaded Successfully" 
						) );
					} else {
						echo json_encode ( array (
								"status" => FALSE,
								"msg" => "Photo Not Uploaded, Please try again!" 
						) );
						// if file was not moved.
					}
				}
			} else { // if file size and file type was incorrect.
				echo json_encode ( array (
						"status" => FALSE,
						"msg" => "File size and file type was incorrect,Photo Not Uploaded, Please try again!" 
				) );
			}
		}
		
		// echo json_encode(array("status" => TRUE));
	}
	
	/*photo upload*/
	public function photo_upload(){
			
		$talents_id=$this->input->post('tid');
		$j = 0;
		$res_folder="talents".$talents_id."";
		if (!is_dir('talents_images/'.$res_folder.'/photos')) {
			mkdir('talents_images/' . $res_folder.'/photos', 0777, TRUE);
	
		}
		$path ='talents_images/'.$res_folder.'/photos/';
		$time=(round(microtime(true) * 1000));
			
		$target_path = $path; //Declaring Path for uploaded images
		for ($i = 0; $i < count($_FILES['photo']['name']); $i++) {//loop to get individual element from the array
			$target_path="";
			$validextensions = array("jpeg", "jpg", "png","PNG");  //Extensions which are allowed
			$ext = explode('.', basename($_FILES['photo']['name'][$i]));//explode file name from dot(.)
			$file_extension = end($ext); //store extensions in the variable
	
			$target_path = $path . $i.'t'.round(microtime(true) * 1000).'.'.end($ext);//set the target path with a new name of image
			$j = $j + 1;//increment the number of uploaded images according to the files in array
	
	
			if (($_FILES["photo"]["size"][$i] < 1000000000) //Approx. 100kb files can be uploaded.
					&& in_array($file_extension, $validextensions)) {
						if($_FILES['photo']['size'][$i] > 1048576){
							if (compress($_FILES['photo']['tmp_name'][$i], $target_path,70)) {//if file moved to uploads folder
							} else {//if file was not moved.
							}
	
						}
						else{
							if (move_uploaded_file($_FILES['photo']['tmp_name'][$i], $target_path)) {
								
								
								//if file moved to uploads folder
							} else {//if file was not moved.
								
							}
	
						}
							
					} else {
						
						// file size and file type was incorrect.
					}
		}
		redirect('talents/talents_profile');
	}
	
	/*Photo delete */
	public function photo_delete(){
		
		$photo=$this->input->post('id');
		
		
		$path = dirname($_SERVER["SCRIPT_FILENAME"])."/".$photo;
		unlink($path);
	
	}
	
	/*Photo delete */
	public function docs_delete(){
	
		$photo=$this->input->post('id');
	
	
		$path = dirname($_SERVER["SCRIPT_FILENAME"])."/".$photo;
		unlink($path);
		
		echo 1;
	
	}
	
	/*Insert the client details*/
	public function client_insert(){
		
		$talents_id=$this->input->post('id');
		$name=$this->input->post('name');
		
		$image=$_FILES['client_photo']['name'];
		$validextensions = array (
				"jpeg",
				"jpg",
				"png"
		); // Extensions which are allowed
		$ext = explode ( '.', basename ( $_FILES['client_photo'] ['name'] ) ); // explode file name from dot(.)
		$file_extension = end ( $ext );
		
		$picname =$image;
		$res_folder="talents".$talents_id."";
		if (!is_dir('talents_images/'.$res_folder.'/clients')) {
			mkdir('talents_images/' . $res_folder.'/clients', 0777, TRUE);
		
		}
		$path = 'talents_images/'.$res_folder.'/clients/'.$picname;
	
		
		if (! empty ( $_FILES ['client_photo'] ['name'] )) {
			if (($_FILES ["client_photo"] ["size"] < 100000000) && // Approx. 100kb files can be uploaded.
					in_array ( $file_extension, $validextensions )) {
						if ($_FILES ['client_photo'] ['size'] > 1048576) {
							if (compress ( $_FILES ['client_photo'] ['name'], $path, 70 )) { // if file moved to uploads folder
								echo json_encode ( array (
										"status" => TRUE,
										"msg" => "Photo Uploaded Successfully"
								) );
							} else { // if file was not moved.
								echo json_encode ( array (
										"status" => FALSE,
										"msg" => "Photo Not Uploaded, Please try again!"
								) );
							}
						} else {
							if (move_uploaded_file ( $_FILES ['client_photo'] ['tmp_name'], $path )) { // if file moved to uploads folder
								echo json_encode ( array (
										"status" => TRUE,
										"msg" => "Photo Uploaded Successfully"
								) );
							} else {
								echo json_encode ( array (
										"status" => FALSE,
										"msg" => "Photo Not Uploaded, Please try again!"
								) );
								// if file was not moved.
							}
						}
					} else { // if file size and file type was incorrect.
						echo json_encode ( array (
								"status" => FALSE,
								"msg" => "File size and file type was incorrect,Photo Not Uploaded, Please try again!"
						) );
					}
		}
		
		
		
		$data=array(
				
				'talents_id'=>$talents_id,
				'client_name'=>$name,
				'logo'=>$image		
				
		);
		$this->db->insert('talents_clients',$data);
		
		$data['clients']=$this->talents_model->clients($this->session->userdata ( 'id' ));
	    $this->load->view('talents/client_append',$data);
		
	}
	
	/*Client Edit*/
	public function client_edit(){
		$id=$this->input->post('id');
		$data ['id'] = $this->session->userdata ( 'id' );
	    $data['client']=$this->talents_model->client_edit($id);
		$this->load->view('talents/ajax_edit_client',$data);
		
	}
	

	/*Update the client details*/
	public function client_update(){
	
		$id=$this->input->post('id');
		$talents_id=$this->session->userdata ( 'id' );
		$name=$this->input->post('edit_name');
	
		$image=$_FILES['edit_client_photo']['name'];
		if (! empty ( $_FILES ['edit_client_photo'] ['name'] )) {
		
		$validextensions = array (
				"jpeg",
				"jpg",
				"png"
		); // Extensions which are allowed
		$ext = explode ( '.', basename ( $_FILES ['edit_client_photo'] ['name'] ) ); // explode file name from dot(.)
		$file_extension = end ( $ext );
	
		$picname =$image;
		$res_folder="talents".$talents_id."";
		if (!is_dir('talents_images/'.$res_folder.'/clients')) {
			mkdir('talents_images/' . $res_folder.'/clients', 0777, TRUE);
	
		}
		$path = 'talents_images/'.$res_folder.'/clients/'.$picname;
	
	
		if (! empty ( $_FILES ['edit_client_photo'] ['name'] )) {
			if (($_FILES ["edit_client_photo"] ["size"] < 100000000) && // Approx. 100kb files can be uploaded.
					in_array ( $file_extension, $validextensions )) {
						if ($_FILES ['edit_client_photo'] ['size'] > 1048576) {
							if (compress ( $_FILES ['edit_client_photo'] ['name'], $path, 70 )) { // if file moved to uploads folder
								echo json_encode ( array (
										"status" => TRUE,
										"msg" => "Photo Uploaded Successfully"
								) );
							} else { // if file was not moved.
								echo json_encode ( array (
										"status" => FALSE,
										"msg" => "Photo Not Uploaded, Please try again!"
								) );
							}
						} else {
							if (move_uploaded_file ( $_FILES ['client_photo'] ['tmp_name'], $path )) { // if file moved to uploads folder
								echo json_encode ( array (
										"status" => TRUE,
										"msg" => "Photo Uploaded Successfully"
								) );
							} else {
								echo json_encode ( array (
										"status" => FALSE,
										"msg" => "Photo Not Uploaded, Please try again!"
								) );
								// if file was not moved.
							}
						}
					} else { // if file size and file type was incorrect.
						echo json_encode ( array (
								"status" => FALSE,
								"msg" => "File size and file type was incorrect,Photo Not Uploaded, Please try again!"
						) );
					}
		}
	
		
	
		$data=array(
	
				
				'client_name'=>$name,
				'logo'=>$image
	
		);
		
		}else{
			$data=array(
			
				
					'client_name'=>$name
					
			
			);
			
		}
		$this->db->where('id',$id);
		$this->db->update('talents_clients',$data);
	
		$data['clients']=$this->talents_model->clients($this->session->userdata ( 'id' ));
		$this->load->view('talents/client_append',$data);
	
	}
	
	
	/*Client delete */
	
	public function client_delete(){
		
		$id=$this->input->post('id');
		$talents_id=$this->input->post('t_id');
		
		$this->db->where('id',$id);
		$this->db->delete('talents_clients');
		echo 1;
		
		
	}
	
	/*Insert  the price page from portfolio page */
	
	public function price_insert(){
		
		$tid=$this->input->post('tid');
		$title=$this->input->post('title');
		$description=$this->input->post('description');
		$cancel_policy=$this->input->post('cancel_policy');
		$charge_type=$this->input->post('charge_type');
		$min=$this->input->post('min');
		$max=$this->input->post('max');
		$price=$this->input->post('price');
		$talent_receive=$this->input->post('talent_receive');
		//$talent_price_id=$this->input->post('price_id');
		
		$data=array(
			    'talents_id'=>$tid,
				'title'=>$title,
				'description'=>$description,
				'cancel_policy'=>$cancel_policy,
				'charge_type'=>$charge_type,
				'min'=>$min,
				'max'=>$max,
				'price'=>$price,
				'talent_receiveable'=>$talent_receive
				
				
				
		);
		
		$this->db->insert('talents_pricing',$data);
		
		echo 1;
		
	}
	
	/*Update  the price page from portfolio page */
	
	public function price_update(){
	
		$tid=$this->input->post('tid');
		$title=$this->input->post('title');
		$description=$this->input->post('description');
		$cancel_policy=$this->input->post('cancel_policy');
		$charge_type=$this->input->post('charge_type');
		$min=$this->input->post('min');
		$max=$this->input->post('max');
		$price=$this->input->post('price');
		$talent_receive=$this->input->post('talent_receive');
		$talent_price_id=$this->input->post('price_id');
	
		$data=array(
				'talents_id'=>$tid,
				'title'=>$title,
				'description'=>$description,
				'cancel_policy'=>$cancel_policy,
				'charge_type'=>$charge_type,
				'min'=>$min,
				'max'=>$max,
				'price'=>$price,
				'talent_receiveable'=>$talent_receive
	
	
	
		);
	
		$this->db->where('id',$talent_price_id);
		$this->db->update('talents_pricing',$data);
	
		echo 1;
	
	}
	
	/*Delete price package */
	public function delete_price(){
		
		$id=$this->input->post('id');
		$this->db->where('id',$id);
		$this->db->delete('talents_pricing');
		echo 1;
		
	}
	
	
	/*Upload price document*/
	
	public function price_docsupload(){
		
		$talents_id=$this->input->post('tid');
		//documents uploads
		if (! empty ( $_FILES ['price_pdf'] ['name'] )) {
			$res_folder="talents".$talents_id."";
			if (!is_dir('talents_images/'.$res_folder.'/price')) {
				mkdir('talents_images/' . $res_folder.'/price', 0777, TRUE);
					
			}
			$path ='talents_images/'.$res_folder.'/price/';
			$time=(round(microtime(true) * 1000));
		
			if ( 0 < $_FILES['price_pdf']['error'] ) {
				echo 'Error: ' . $_FILES['price_pdf']['error'] . '<br>';
			}
			else {
				move_uploaded_file($_FILES['price_pdf']['tmp_name'], $path."/" . $_FILES['price_pdf']['name']);
			}
				
		}
		redirect('talents/talents_portfolio/pricing');
	}
	
	/*Jobs page */
	public function jobs(){
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$data ['talents_info'] = $this->talents_model->talents_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$data['pending_jobs']=$this->talents_model->pending_jobs($this->session->userdata ( 'id' ));
			$data['accepted_jobs']=$this->talents_model->accepted_jobs($this->session->userdata ( 'id' ));
			$data['declined_jobs']=$this->talents_model->declined_jobs($this->session->userdata ( 'id' ));
			$this->load->view ( 'talents_head', $data );
			$this->load->view ( 'talents/jobs', $data );
			$this->load->view ( 'footer', $data );
			
		}
		
	}
	
	/*Accept the job from talents side */
	
	public function job_accept(){
		
		$talents_id=$this->input->post('talents_id');
		$event_id=$this->input->post('event_id');
			
		$this->db->where('talents_id',$talents_id);
		$this->db->where('event_id',$event_id);
		$this->db->where('status',0);
		$data = array (
				'status' => 1
		);
		$this->db->update ( 'event_details', $data );
		echo 1;
		
		
		
	}
	
	/*Job decline from talennts end*/
	public function job_decline(){
	
		$talents_id=$this->input->post('talents_id');
		$event_id=$this->input->post('event_id');
			
		$this->db->where('talents_id',$talents_id);
		$this->db->where('event_id',$event_id);
		$data = array (
				'status' => 3
		);
		$this->db->update ( 'event_details', $data );
		echo 1;
	
	
	
	}
	
	
	/*logout funtion */
	public function logout() {
		$this->session->sess_destroy ();
		redirect ( 'home' );
	}
	
	
	function compress($source, $destination, $quality) {
	
		$info = getimagesize($source);
	
		if ($info['mime'] == 'image/jpeg')
			$image = imagecreatefromjpeg($source);
	
			elseif ($info['mime'] == 'image/gif')
			$image = imagecreatefromgif($source);
	
			elseif ($info['mime'] == 'image/png')
			$image = imagecreatefrompng($source);
			
			elseif ($info['mime'] == 'application/pdf')
			$image = imagecreatefrompng($source);
			
			elseif ($info['mime'] == 'application/docx')
			$image = imagecreatefrompng($source);
			
			elseif ($info['mime'] == 'application/doc')
			$image = imagecreatefrompng($source);
			
			
	
			imagejpeg($image, $destination, $quality);
	
			return $destination;
	}
	
	
	/*Load the events only accepted events to calendar*/
	public function get_events()
	{
		// Our Start and End Dates
		$start = $this->input->get("start");
		$end = $this->input->get("end");
	
		$startdt = new DateTime('now'); // setup a local datetime
		$startdt->setTimestamp($start); // Set the date based on timestamp
		$start_format = $startdt->format('Y-m-d h:i:s');
	
		$enddt = new DateTime('now'); // setup a local datetime
		$enddt->setTimestamp($end); // Set the date based on timestamp
		$end_format = $enddt->format('Y-m-d h:i:s');
	
		$events = $this->talents_model->get_events($start_format, $end_format);
	
		$data_events = array();
	
		foreach($events as $r) {
	
			$data_events[] = array(
					"id" => $r->event_id,
					"title" => $r->event_name,
					"start"=>$r->event_start,
					"onstart"=>$r->event_start,
					"location"=>$r->venue
					
			);
		}
	
		echo json_encode(array("events" => $data_events));
		exit();
	}
	
}