<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
class Home extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( array (
				'form',
				'url',
				'html',
				'date'
		) );
		$this->load->library ( array (
				'form_validation',
				'session'
		) );
		$this->load->model ( 'clients_model' );
		$this->load->model ( 'talents_model' );
		$this->load->library ( 'email' );
	}

	/**
	 * Home page
	 */
	public function index() {
		$this->load->model ( 'talents_model' );

		$data ['talents_list'] = $this->talents_model->talents_list ();
		if (! $this->session->userdata ( 'validated' )) {
				
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'home', $data );
			$this->load->view ( 'footer',$data );
				
		} else {
			if ($this->session->userdata ( 'type' ) == 1) {
				redirect ( 'talents/talents_profile' );
			} else if ($this->session->userdata ( 'type' ) == 2) {

				$data ['email'] = $this->session->userdata ( 'email' );
				$data ['id'] = $this->session->userdata ( 'id' );
				$data ['type'] = $this->session->userdata ( 'type' );
				$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
				$data ['user'] = $this->clients_model->user_info ( $this->session->userdata ( 'id' ) );

				$this->load->view ( 'head', $data );
				$this->load->view ( 'home', $data );
				$this->load->view ( 'footer',$data );

			}else if($this->session->userdata('type')==2 && $this->session->userdata('updated')==0){

				redirect ( 'clients/client_info_dashboard' );
			}
		}
	}

	/*function for discover page */
	public function discover(){

		$this->load->model ( 'talents_model' );

		$data ['talents_list'] = $this->talents_model->talents_list ();
		if (! $this->session->userdata ( 'validated' )) {

			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'client/discover', $data );
			$this->load->view ( 'footer',$data );

		} else {
			if ($this->session->userdata ( 'type' ) == 1) {
				redirect ( 'talents/talents_profile' );
			} else if ($this->session->userdata ( 'type' ) == 2) {

				$data ['email'] = $this->session->userdata ( 'email' );
				$data ['id'] = $this->session->userdata ( 'id' );
				$data ['type'] = $this->session->userdata ( 'type' );
				$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
				$data ['user'] = $this->clients_model->user_info ( $this->session->userdata ( 'id' ) );

				$this->load->view ( 'head', $data );
				$this->load->view ( 'client/discover', $data );
				$this->load->view ( 'footer',$data );

			}
		}
	}

	/* Search function */
	public function search() {
		$search_key=$this->uri->segment(3);
		$category_key="";
		
		$search_title = urldecode($search_key);
		
		$data ['talents_list'] = $this->talents_model->talents_list_search($search_title);
		$data ['talents_category'] = $this->talents_model->talents_searchcategory();

		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'search', $data );
			$this->load->view ( 'footer', $data );

		} else {

			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$type = $this->session->userdata ( 'type' );
			if ($type == 2) {
				$this->load->view ( 'head', $data );
				$this->load->view ( 'search', $data );
				$this->load->view ( 'footer', $data );
			} else {

				redirct ( 'talents' );
			}
		}
	}
	/* Contact Page for talent */
	public function contactus_talent() {
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'contact_talent', $data );
			$this->load->view ( 'footer', $data );
		} else {

			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$type = $this->session->userdata ( 'type' );
			if ($type == 2) {
				$this->load->view ( 'head', $data );
				$this->load->view ( 'contact_talent', $data );
				$this->load->view ( 'footer', $data );
			} else {

				redirct ( 'talents' );
			}
		}
	}
	/* Contact Page for company */
	public function contactus_company() {
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'contact_company', $data );
			$this->load->view ( 'footer', $data );
		} else {

			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$type = $this->session->userdata ( 'type' );
			if ($type == 2) {
				$this->load->view ( 'head', $data );
				$this->load->view ( 'contact_company', $data );
				$this->load->view ( 'footer', $data );
			} else {

				redirct ( 'talents' );
			}
		}
	}
	/* Search function */
	public function search_filter() {
		$search_key=$this->uri->segment(3);$search_key=$this->uri->segment(3);
		$category = $this->input->post ( 'category' );
		$price = $this->input->post ( 'price' );
		$exp = $this->input->post ( 'exp' );
		$experience=explode(",",$exp);
		$price=explode(",",$exp);
		$minexp=$experience[0];
		$maxexp=$experience[1];
		$minprice=$price[0];
		$maxprice=$price[1];
		$data ['talents_list'] = $this->talents_model->talents_list_filter($category,$minexp,$maxexp,$minprice,$maxprice);
		$data ['talents_category'] = $this->talents_model->talents_searchcategory();
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'ajax_search', $data );
				
		} else {

			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$type = $this->session->userdata ( 'type' );
			if ($type == 2) {
				$this->load->view ( 'ajax_search', $data );

			} else {

				redirct ( 'talents' );
			}
		}

	}

	/* Talents profile page in front end */
	public function talents_profile() {
		$talents_id = $this->uri->segment ( 3 );
		$data ['talents_detail'] = $this->talents_model->talents_detail ( $talents_id );
		$data ['talents_video'] = $this->talents_model->talents_video ( $talents_id );
		$data['price']=$this->talents_model->price_package($talents_id);
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'talent_user_detail', $data );
			$this->load->view ( 'footer', $data );
		} else {
				
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$type = $this->session->userdata ( 'type' );
				

			if ($type == 2) {
				$this->load->view ( 'head', $data );
				$this->load->view ( 'talent_user_detail', $data );
				$this->load->view ( 'footer', $data );
			} else {

				redirct ( 'talents' );
			}
		}
	}

	/* Contact form Mail send Company */
	public function contact_mail() {
		$email = $this->input->post('email');
		$message = $this->input->post('message');
		$name = $this->input->post('name');
		$subject = $this->input->post('subject');
		$enquiry = $this->input->post('message');
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='localhost';
		$config['smtp_port']='25';
		$config['smtp_secure']='none';
		$config['smtp_timeout']='30';
		$config['smtp_auth']=FALSE;
		$config['smtp_user']='dinesh@inoble.in';
		$config['smtp_pass']='Welcome@123';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['crlf'] = '\r\n';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from ( 'dinesh@inoble.in', 'Talent Portal' );
		$this->email->to ('dinesh@inoble.in');
		$this->email->subject ( 'Enquiry Mail' );
		
		$data['mail']=$email;
		$data['subject']=$subject;
		$data['name']=$name;
		$data['message']=$message;
		$this->email->message($this->load->view('mail_template',$data, TRUE));
		$data=array(

				'name'=>$name,
				'email'=>$email,
				'subject'=>$subject,
				'message'=>$message
		);
		$this->db->insert('enquiry',$data);
		
		if(!$this->email->send()){
			echo "failure";
			echo $this->email->print_debugger();
		
		}
		if ($this->session->userdata ( 'validated' ) && $this->session->userdata ( 'type' )==1) {
			
			redirct('talents/contact');
		}else{
			redirect('clients/contact');
		}
		

	}

	/* Contact form Mail send talent */
	public function send_mail_talent() {
		$email = $this->input->post('email');
		$secondmail = $this->input->post('secondemail');
		$personname = $this->input->post('personname');
		$actname = $this->input->post('actname');
		$contactnum = $this->input->post('contact_number');
		$othernum = $this->input->post('other_number');
		$this->load->library ( 'email' );
		$this->email->from ( $email, 'Talent Portal' );
		$this->email->to ('inoblearunpandi@gmail.com');
		$this->email->subject ('Talent Portal');
		$this->email->message ('Name:'.$name. 'Contact Email Address:'.$email.'second mail Address:'.$secondmail.'Contact Person Name:'.$personname.'contact Phone Number:'.$contactnum );
		if (! $this->email->send ()) {
			echo "failure";
		}
		echo $this->email->print_debugger ();
		$data=array(
				'act_type'=>$actname,
				'name'=>$personname,
				'email'=>$email,
				'phone'=>$contactnum,
				'email1'=>$secondmail,
				'phone1'=>$othernum

		);
		$this->db->insert('talent_enquiry',$data);

	}
	
	/*Resetpassword send mail to client */
	public function reset_password(){
		
		$email=$this->input->post('signup_email');
		$hash=md5($email);
		$result=$this->talents_model->check_email($email);
		if($result==1){
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='localhost';
		$config['smtp_port']='25';
		$config['smtp_secure']='none';
		$config['smtp_timeout']='30';
		$config['smtp_auth']=FALSE;
		$config['smtp_user']='dinesh@inoble.in';
		$config['smtp_pass']='Welcome@123';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['crlf'] = '\r\n';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from ( 'dinesh@inoble.in', 'Talent Portal' );
		$this->email->to ($email);
		$this->email->subject ( 'Event Muse Reset Password ' );
		$data['link']="http://inoble.in/talentportal/home/reset/" . $hash ."";
		$data['mail']=$email;
		$data['type']="reset";
		$this->email->message($this->load->view('verify_emailtemplate',$data, TRUE));
		
		if(!$this->email->send()){
			echo "failure";
			echo $this->email->print_debugger();
		
		}else{
			
			echo "success";
		}
		}else{
			
			echo 2;
		}
		//echo $this->email->print_debugger();
	}
	
	/*view reset password*/
	public function reset(){
		$data['hash']=$this->uri->segment(3);
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = "";
			$data ['type'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'reset', $data );
			$this->load->view ( 'footer', $data );
		} else {
		
			redirect('home');
		
	}
}

/*update password*/
public function password_update(){
	
	
	$hash=$this->input->post('hash');
	
	
	$password =  md5 ( $this->input->post('password') );
	$data=array(
			'password'=>$password,
	);
	
	$this->db->where('hash', $hash);
	$this->db->update('user', $data);
	echo 1;
}


}


