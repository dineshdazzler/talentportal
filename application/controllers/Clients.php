<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
header ( 'Access-Control-Allow-Origin: *' );
class Clients extends CI_Controller {
	function __construct() {
		parent::__construct ();
		$this->load->helper ( array (
				'form',
				'url',
				'html',
				'date'
		) );
		$this->load->library ( array (
				'form_validation',
				'session'
		) );
		$this->load->model ( 'clients_model' );
		$this->load->model ( 'talents_model' );
		$this->load->library ( 'email' );
	}

	/**
	 * Home page
	 */
	public function index() {
		if (! $this->session->userdata ( 'validated' )) {
			$data ['username'] = "";
			$data ['id'] = ""; 
			$data ['type'] = "";
			$data ['updated'] = "";
			$this->load->view ( 'head', $data );
			$this->load->view ( 'home', $data );
			$this->load->view ( 'footer', $data );
		} else {
			if($this->session->userdata('type')==2 && $this->session->userdata('updated')==1){
		
			redirect ( 'clients/clients_dashboard' );
			}
			else if($this->session->userdata('type')==2 && $this->session->userdata('updated')==0){
				
				redirect ( 'clients/client_info_dashboard' );
				
			}else{
				redirect('admin');
			}
			
		}
		}
		
	/* Signup process for Clients */
	public function signup() {
		$email = $this->input->post ( 'signup_email' );
		$password = md5 ( $this->input->post ( 'signup_password' ) );
		$type = $this->input->post ( 'type' );
		$reg_date = date ( 'Y-m-d H:i:s' );
		$email_exists = $this->clients_model->check_email ( $email );

		if ($email_exists == 0) {
				
			$data = array (
					'email' => $email,
					'password' => $password,
					'type' => $type,
					'reg_date' => $reg_date,
					'hash' => md5 ( $email )
			);
				
			$this->db->insert ( 'user', $data );
				
			$hash=md5($this->input->post('signup_email' ));
			$this->mailsend($email,$hash);
			echo 'success';
			
		} else {
				
			echo "exists";
		}
	}

	/* Mail send function for email verification without using smtp */
	public function mailsend($email,$hash) {
		
		$this->load->library('email');
		$config['protocol']='smtp';
		$config['smtp_host']='localhost';
		$config['smtp_port']='25';
		$config['smtp_secure']='none';
		$config['smtp_timeout']='30';
		$config['smtp_auth']=FALSE;
		$config['smtp_user']='dinesh@inoble.in';
		$config['smtp_pass']='Welcome@123';
		$config['charset']='utf-8';
		$config['newline']="\r\n";
		$config['crlf'] = '\r\n';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html';
		
		$this->email->initialize($config);
		$this->email->set_newline("\r\n");
		$this->email->from ( 'dinesh@inoble.in', 'Talent Portal' );
		$this->email->to ($email);
		$this->email->subject ( 'Event Muse email Verification for Client account' );
		$data['link']="http://inoble.in/talentportal/clients/verify/" . $hash ."";
		$data['mail']=$email;
		$data['type']="signup";
		$this->email->message($this->load->view('verify_emailtemplate',$data, TRUE));
		
		if(!$this->email->send()){
			echo "failure";
			echo $this->email->print_debugger();
				
		}
		//echo $this->email->print_debugger();
		
	}

	/* Verfication for user account and activate the user account */
	public function verify() {
		$hash = $this->uri->segment ( 3 );

		$result = $this->clients_model->verify_hash ( $hash );
		if($result == 1){
		
			if(! $this->session->userdata('validated')){
				$data['username']="";
				$data['id']		="";
				$data['admin']="";
				$this->load->view('head',$data);
				$this->load->view('home');
				$this->load->view('footer',$data);
			}else{
			if($this->session->userdata('type')==2 && $this->session->userdata('updated')==1){
		
			redirect ( 'clients/clients_dashboard' );
			}
			else if($this->session->userdata('type')==2){
				
				redirect ( 'clients/client_info_dashboard' );
				
			}
		}
	}
	}
	/* user login and get values from verify page */
	public function login() {
		$email = $this->input->post ( 'email' );
		$password = $this->input->post ( 'password' );
		$count = $this->clients_model->login ( $email, $password );

		if (! $count) {
			echo "failure";
		} else {
			$type = $this->session->userdata ( 'type' );
			echo $type;
			$this->db->where ( 'email', $email );
			$time = (round ( microtime ( true ) * 1000 ));
			$data = array (
					'last_login' => $time
			);
			$this->db->update ( 'user', $data );
		}
	}

	/* Clients dashboard/upcomming event section */
	public function clients_dashboard() {
		$data ['talents_type'] = $this->clients_model->talents_type ();

		if (! $this->session->userdata ( 'validated' )&&$this->session->userdata('type')!=2 && $this->session->userdata('updated')!=1 ) {
			redirect ( 'home' );
		} else {
				
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$data['result']=$this->clients_model->upcomming_event();
			$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );	
			$this->load->view ( 'head', $data );
			$this->load->view ( 'client/client_dashboard', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	/* Clients past event section */
	public function past_event() {
		$data ['talents_type'] = $this->clients_model->talents_type ();
	
		if (! $this->session->userdata ( 'validated' )&&$this->session->userdata('type')!=2 && $this->session->userdata('updated')!=1 ) {
			redirect ( 'home' );
		} else {
			$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$data['result']=$this->clients_model->past_event();
			$this->load->view ( 'client/client_head', $data );
			$this->load->view ( 'client/past_event', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	
	/* event create section */
	public function event_create() {
		$data ['talents_type'] = $this->clients_model->talents_type ();
		$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
		if (! $this->session->userdata ( 'validated' )&&$this->session->userdata('type')!=2 && $this->session->userdata('updated')!=1 ) {
			redirect ( 'home' );
		} else {
	
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$this->load->view ( 'head', $data );
			$this->load->view ( 'client/event_create', $data );
			$this->load->view ( 'footer', $data );
		}
	}
	/* event edit page section */
	public function event_edit()
	{
		$data ['talents_type'] = $this->clients_model->talents_type ();
		$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
			
		if (! $this->session->userdata ( 'validated' )&&$this->session->userdata('type')!=2 && $this->session->userdata('updated')!=1 ) {
			redirect ( 'home' );
		} else {
		
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
		$id=$this->uri->segment(3);
		$data['result']=$this->clients_model->event_edit($id);
		$this->load->view('head',$data);
		$this->load->view('client/event_edit',$data);
		$this->load->view('footer',$data);
		}
	}
	/* event delete section */
	public function  event_cancel(){
		$e_id=$this->input->post('id');
	
		$this->db->where('id',$e_id);
		$this->db->delete('event');
		$value=$this->db->affected_rows();
		if($value >0 ){
			echo 1;
		}else{
			echo 0;
		}
	
	}
	/* Clients info start dashboard section */
	public function client_info_dashboard() {
		$data ['talents_type'] = $this->clients_model->talents_type ();
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'home' );
		} else if($this->session->userdata('type')==2 && $this->session->userdata('updated')==0){
			
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
			$data ['user'] = $this->clients_model->user_info ( $this->session->userdata ( 'id' ) );	
			$this->load->view ( 'info_head', $data );
			$this->load->view ( 'client/client_info_dashboard', $data );
			$this->load->view ( 'footer', $data );
		}else{
			
			redirect('clients');
		}
	}
	
	/* Clients info setting section */
	public function settings() {
		
		$data ['talents_type'] = $this->clients_model->talents_type ();
		$data ['settings'] = $this->clients_model->setting ();
		
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'home' );
		} else {
	
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
			$this->load->view ( 'head', $data );
			$this->load->view ( 'client/setting', $data );
			$this->load->view ( 'footer', $data );
		}
	}

	/*Clients genrel info insertion */
	public function  generalinfo_insert(){

		$client_id=$this->input->post('t_id');
		$fname=$this->input->post('fname');
		$lname=$this->input->post('lname');
		$contact_number=$this->input->post('contact_number');
		$organization=$this->input->post('organization');
		$country=$this->input->post('country');
		$email=$this->input->post('email');
		$update=$this->input->post('update');
			$data=array(
						
					'client_id'=>$client_id,
					'firstname'=>$fname,
					'lastname'=>$lname,
					'organization'=>$organization,
					'phone'=>$contact_number,
					'email'=>$email,
					'country'=>$country			
			);
				
			$this->db->insert('client_info',$data);
			echo "success";
			
			
		}
		/*Clients genrel info update */
		public function  generalinfo_update(){
		
			$client_id=$this->input->post('t_id');
			$fname=$this->input->post('fname');
			$lname=$this->input->post('lname');
			$contact_number=$this->input->post('contact_number');
			$organization=$this->input->post('organization');
			$country=$this->input->post('country');
			$email=$this->input->post('email');
			$update=$this->input->post('update');
			$data=array(
		
					'client_id'=>$client_id,
					'firstname'=>$fname,
					'lastname'=>$lname,
					'organization'=>$organization,
					'phone'=>$contact_number,
					'email'=>$email,
					'country'=>$country
			);
		
			$this->db->where('client_id', $client_id);
			$this->db->update('client_info', $data);
				
				
		}
		/*Password change function  */
		public function  password_update(){
		
			$client_id=$this->input->post('client_id');
			$password =  md5 ( $this->input->post ( 'newpass' ) );
			$data=array(
					'password'=>$password,
			);
		
			$this->db->where('id', $client_id);
			$this->db->update('user', $data);
		
		
		}
		/*Events Insert function  */
		
		public function  event_insert(){
		
			$client_id=$this->input->post('c_id');
			$eventtitle=$this->input->post('eventtitle');
			$eventtype=$this->input->post('eventtype');
			$starttime=$this->input->post('eventstart');
			$endtime=$this->input->post('eventend');
			$budget=$this->input->post('budget');
			$venue=$this->input->post('venue');
			
			$pax=$this->input->post('pax');
			$description=$this->input->post('description');
			
			
			
			

			$data=array(
		
					'client_id'=>$client_id,
					'event_title'=>$eventtitle,
					'event_type'=>$eventtype,
					'event_start'=>$starttime,
					'event_end'=>$endtime,
					'budget'=>$budget,
					'pax'=>$pax,
					'venue'=>$venue,
					
					'description'=>$description
					
			);
		
			$this->db->insert('event',$data);
			$event_id=$this->db->insert_id();
			
			echo "success";
				
				
		}
		
		
		public function  event_update(){
		
			$client_id=$this->input->post('c_id');
			$e_id=$this->input->post('e_id');
			$eventtitle=$this->input->post('eventtitle');
			$eventtype=$this->input->post('eventtype');
			$starttime=$this->input->post('eventstart');
			$endtime=$this->input->post('eventend');
			$budget=$this->input->post('budget');
			$venue=$this->input->post('venue');
			
			$pax=$this->input->post('pax');
			$description=$this->input->post('description');
				
			$data=array(
		
					'client_id'=>$client_id,
					'event_title'=>$eventtitle,
					'event_type'=>$eventtype,
					'event_start'=>$starttime,
					'event_end'=>$endtime,
					'budget'=>$budget,
					'pax'=>$pax,
					'venue'=>$venue,
					
					'description'=>$description
						
			);
		
			$this->db->where('id', $e_id);
			$this->db->update('event', $data);
			$event_id=$e_id;
			
			
			echo "success";
			
			
				
		
		}
		public function  user_update(){
			$client_id=$this->input->post('t_id');
			$update=$this->input->post('update');
			$data=array(
			
					'updated'=>$update,
					
			);
			$this->db->where('id',$client_id);
			$this->db->update('user',$data);
		}

	 /*Event summary details in clients section */
	public function event_summary(){

		$event_id=$this->uri->segment(3);
		$data['event_details']=$this->clients_model->event_summary($event_id);
		$data['talents_detail']=$this->clients_model->talents_list($event_id);
		if (! $this->session->userdata ( 'validated' )) {
			redirect ( 'talents' );
		} else {
			$data ['clients_info'] = $this->clients_model->clients_info ( $this->session->userdata ( 'id' ) );
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$this->load->view ( 'head', $data );
			$this->load->view ( 'client/event_summary', $data );
			$this->load->view ( 'footer', $data );
		}
		
	}
	
	
	
	/*Hire talents for evvents */
	public function hire_me(){
		$talents_id = $this->uri->segment ( 3 );
		$data ['talents_detail'] = $this->talents_model->talents_detail ( $talents_id );
		$data ['talents_video'] = $this->talents_model->talents_video ( $talents_id );
		$data['result']=$this->clients_model->upcomming_event();
		$data['pricing']=$this->clients_model->pricing_detail($talents_id);
		if (! $this->session->userdata ( 'validated' )) {
		redirect('home');
		} else {
				
			$data ['email'] = $this->session->userdata ( 'email' );
			$data ['id'] = $this->session->userdata ( 'id' );
			$data ['type'] = $this->session->userdata ( 'type' );
			$type = $this->session->userdata ( 'type' );
			if ($type == 2) {
				$this->load->view ( 'head', $data );
				$this->load->view ( 'hireme', $data );
				$this->load->view ( 'footer', $data );
			} else {
		
				redirct ( 'talents' );
			}
		}
		
		
		}
		
		/*Hired talent insert into database and also validation for dublicate hiring */
		
		public function hiretalent_insert(){
			
			$event_id=$this->input->post('event_id');
			$talent_id=$this->input->post('talent_id');
			$duration=$this->input->post('duration');
			$total_price=$this->input->post('total_price');
			$client_id=$this->input->post('client_id');
			$hr_price=$this->input->post('hr_price');
			$package_name=$this->input->post('package_name');
			$notes=$this->input->post('notes');
			$talents_check=$this->clients_model->hiretalent_check($event_id,$talent_id);
			 
			if($talents_check == 0 || $talents_check == null){
				$data=array(
						
						'event_id'=>$event_id,
						'talents_id'=>$talent_id,
						'talents_cost'=>$total_price,
						'status'=>0,
						'total_hours'=>$duration,
						'notes'=>$notes,
						'clients_id'=>$client_id,
						'hr_price'=>$hr_price,
						'package_name'=>$package_name
						
				);
				
				$this->db->insert('event_details',$data);
				$result=$this->clients_model->get_talentdetalils($talent_id,$client_id,$event_id);
				
					$data['talent_name']=$result->talent_name;
					$data['talent_email']=$result->email;
					$data['client_name']=$result->client_name;
					$data['event']=$result->event_name;
					
					$this->load->library('email');
					$config['protocol']='smtp';
					$config['smtp_host']='localhost';
					$config['smtp_port']='25';
					$config['smtp_secure']='none';
					$config['smtp_timeout']='30';
					$config['smtp_auth']=FALSE;
					$config['smtp_user']='dinesh@inoble.in';
					$config['smtp_pass']='Welcome@123';
					$config['charset']='utf-8';
					$config['newline']="\r\n";
					$config['crlf'] = '\r\n';
					$config['wordwrap'] = TRUE;
					$config['mailtype'] = 'html';
					
					$this->email->initialize($config);
					$this->email->set_newline("\r\n");
					$this->email->from ( 'dinesh@inoble.in', 'Talent Portal' );
					$this->email->to ($result->email);
					$this->email->subject ( 'Job request from event Muse ' );
					$data['link']="http://127.0.0.1/talentportal/talents/talents_dashboard";
					
				
					$this->email->message($this->load->view('hire_emailtemplate',$data, TRUE));
					
					if(!$this->email->send()){
						echo "failure";
						echo $this->email->print_debugger();
					
					}
				
				echo 1;
				
			}else{
				echo 2;
			}
		}
	
		/* Profile photo change add */
		public function ajax_add() {
			$validextensions = array (
					"jpeg",
					"jpg",
					"png"
			); // Extensions which are allowed
			$ext = explode ( '.', basename ( $_FILES ['photo'] ['name'] ) ); // explode file name from dot(.)
			$file_extension = end ( $ext );
		
			$picname = "clients" . $_POST ['tid'] . "pic";
			$path = 'clients_profilepic/' . $picname;
			$files = glob ( 'clients_profilepic/*' ); // get all file names
			foreach ( $files as $file ) {
				if (is_file ( $file )) {
					$genres = explode ( '/', $file );
					$genres = explode ( '.', $genres [1] );
					if ($genres [0] == $picname) {
						unlink ( $file );
					}
		
					// unlink($file);
				} // delete file
			}
		
			if (! empty ( $_FILES ['photo'] ['name'] )) {
				if (($_FILES ["photo"] ["size"] < 100000000) && // Approx. 100kb files can be uploaded.
						in_array ( $file_extension, $validextensions )) {
							if ($_FILES ['photo'] ['size'] > 1048576) {
								if (compress ( $_FILES ['photo'] ['tmp_name'], $path, 70 )) { // if file moved to uploads folder
									echo json_encode ( array (
											"status" => TRUE,
											"msg" => "Photo Uploaded Successfully"
									) );
								} else { // if file was not moved.
									echo json_encode ( array (
											"status" => FALSE,
											"msg" => "Photo Not Uploaded, Please try again!"
									) );
								}
							} else {
								if (move_uploaded_file ( $_FILES ['photo'] ['tmp_name'], $path )) { // if file moved to uploads folder
									echo json_encode ( array (
											"status" => TRUE,
											"msg" => "Photo Uploaded Successfully"
									) );
								} else {
									echo json_encode ( array (
											"status" => FALSE,
											"msg" => "Photo Not Uploaded, Please try again!"
									) );
									// if file was not moved.
								}
							}
						} else { // if file size and file type was incorrect.
							echo json_encode ( array (
									"status" => FALSE,
									"msg" => "File size and file type was incorrect,Photo Not Uploaded, Please try again!"
							) );
						}
			}
		
			// echo json_encode(array("status" => TRUE));
		}

	/*Quote page */
		public function quote_me(){
			$talents_id = $this->uri->segment ( 3 );
			$data ['talents_detail'] = $this->talents_model->talents_detail ( $talents_id );
			$data ['talents_video'] = $this->talents_model->talents_video ( $talents_id );
			$data['result']=$this->clients_model->upcomming_event();
			$data['pricing']=$this->clients_model->pricing_detail($talents_id);
			if (! $this->session->userdata ( 'validated' )) {
				redirect('home');
			} else {
		
				$data ['email'] = $this->session->userdata ( 'email' );
				$data ['id'] = $this->session->userdata ( 'id' );
				$data ['type'] = $this->session->userdata ( 'type' );
				$type = $this->session->userdata ( 'type' );
				if ($type == 2) {
					$this->load->view ( 'head', $data );
					$this->load->view ( 'quoteme', $data );
					$this->load->view ( 'footer', $data );
				} else {
		
					redirct ( 'talents' );
				}
			}
		
		
		}
		
		/*Quoted talent  */
		
		public function quote_insert(){
				
			$event_id=$this->input->post('event_id');
			$talent_id=$this->input->post('talent_id');
			$duration=$this->input->post('duration');
			$total_price=$this->input->post('total_price');
			$client_id=$this->input->post('client_id');
			$hr_price=$this->input->post('hr_price');
			$package_name=$this->input->post('package_name');
			$notes=$this->input->post('notes');
			
				echo 1;
		
			
		}
		
		
		/*Cancel the talents from event summary page*/
		public function talents_cancel(){
			
			$talents_id=$this->input->post('talents_id');
			$event_id=$this->input->post('event_id');
			
			$this->db->where('talents_id',$talents_id);
			$this->db->where('event_id',$event_id);
			$data = array (
					'status' => 2
			);
			$this->db->update ( 'event_details', $data );
			echo 1;
		}
	
		
		/*Support page section*/
		public function support() {
			
			
				$data ['email'] = $this->session->userdata ( 'email' );
				$data ['id'] = $this->session->userdata ( 'id' );
				
				$data ['type'] = $this->session->userdata ( 'type' );
				$this->load->view ( 'head', $data );
				$this->load->view ( 'talents/support', $data );
				$this->load->view ( 'footer', $data );
			
		}
		
		/*Contact page section*/
		public function contact() {
			
				$data ['email'] = $this->session->userdata ( 'email' );
				$data ['id'] = $this->session->userdata ( 'id' );
				
				$data ['type'] = $this->session->userdata ( 'type' );
				$this->load->view ( 'head', $data );
				$this->load->view ( 'talents/contact', $data );
				$this->load->view ( 'footer', $data );
			
		}

	/* Client Logout */
	public function logout() {
		$this->session->sess_destroy ();
		redirect ( 'home' );
	}
}