<style>
.hover06 figure img {
	-webkit-transform: rotate(15deg) scale(1.4);
	transform: rotate(15deg) scale(1.4);
	-webkit-transition: .3s ease-in-out;
	transition: .3s ease-in-out;
}

.hover06 figure:hover img {
	-webkit-transform: rotate(0) scale(1);
	transform: rotate(0) scale(1);
}
.tp-caption1{
    background: linear-gradient(180deg, rgba(255, 255, 255, 0.7) 0px, rgba(255, 255, 255, 0.7) 95%);
    background-color: rgba(255,255,255,.6);
    backdrop-filter: blur(5px);
    padding:50px;
    margin-top:-100px;
}
.tp-caption2{
    background: linear-gradient(180deg,rgba(0, 219, 194, 0.47) 0px, rgba(0, 219, 194, 0.47) 95%);
    background-color: rgba(0, 219, 113, 0.29);
    backdrop-filter: blur(5px);
    padding:50px;
    margin-top:-100px;
}
@media only screen and (max-device-width: 480px) {
		.tp-caption1{
   margin: -75px -70px 0px !important;
   font-weight: 100 !important;
    font-size: 12px !important;
}
	.tp-caption1 h4{
    font-size: 12px !important;
    letter-spacing: 0.1825em !important;
}
	.tp-caption1 h1{
    font-size: 16px !important;
    letter-spacing: 0.1825em !important;
}
.tp-caption2{
   margin: -75px -130px 0px !important;
   font-weight: 100 !important;
    font-size: 12px !important;
}
.tp-caption p{
   transition: none;
    line-height: 38px; 
     border-width: 0px; 
    margin: 10px -110px 8px !important;
    padding: 0px; 
     letter-spacing: 0px;
    font-weight: 100 !important;
    font-size: 9px !important;
    color: white;
}
	}
	@media only screen 
and (min-device-width : 768px) 
and (max-device-width : 1024px)  {	.tp-caption1{
   margin: -75px -70px 0px !important;
   font-weight: 100 !important;
    font-size: 12px !important;
}
.tp-caption2{
   margin: -75px 0px 0px !important;
   font-weight: 100 !important;
    font-size: 12px !important;
}
.tp-caption p{
   transition: none;
    line-height: 38px; 
     border-width: 0px; 
    margin: 50px -70px 8px !important;
    padding: 0px; 
     letter-spacing: 0px;
    font-weight: 600 !important;
    font-size: 16px !important;
    color: white;
} }
</style>
<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>theme_assets/css/jquery.datetimepicker.css" />
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}
</style>

<script type="text/javascript">
function add_profilepic(id)
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit Event Picture'); // Set title to Bootstrap modal title
}


function parseDate(s) {
	  var b = s.split(/\D/);
	  return new Date(b[2], --b[0], b[1]);
	}
/*    General info */
$(document).ready(function(){
	
	$("#error_edit").hide();
	
	
	$("form#form_eventcreate").submit(function(event){
		 
		  //disable the default form submission
		  event.preventDefault();

		   //grab all form data  
		   var formData = new FormData($(this)[0]);
           var starttime=$("#eventstart").val();
           var endtime=$("#eventend").val();

           var today = new Date();
           today.setHours(0,0,0,0);


           if (new Date(starttime) > today ) {
        	   
        	 
			if(new Date(endtime) > new Date(starttime)){

				 $.ajax({
					    url: '<?php echo base_url();?>clients/event_insert',
					    type: 'POST',
					    data: formData,
					    async: false,
					    cache: false,
					    contentType: false,
					    processData: false,
					    success: function (data) {
						 
						window.location.href="<?php echo base_url();?>clients/clients_dashboard";	 
						
					},
					error: function(jqXHR, textStatus) {
						alert( "Request failed: " + jqXHR );
					}
				});

			}else{
        alert("Please select end date larger than start date");
			}

		

           }else{

alert("Please select startdate and end date larger than today!");
               }

		


	});


});





	</script>
<style>
.title {
	font-family: Brandon Grostesque;
	font-size: 39px;
}

.hover06 figure img {
	-webkit-transform: rotate(15deg) scale(1.4);
	transform: rotate(15deg) scale(1.4);
	-webkit-transition: .3s ease-in-out;
	transition: .3s ease-in-out;
}

.hover06 figure:hover img {
	-webkit-transform: rotate(0) scale(1);
	transform: rotate(0) scale(1);
}
</style>
<link href="<?php echo base_url();?>css/style1.css" rel="stylesheet"
	type="text/css" />
<div class="clearfix"></div>
<div class="mj_shop_slider">
	<div id="rev_slider_4_1_wrapper"
		class="rev_slider_wrapper fullwidthbanner-container"
		data-alias="classicslider1"
		style="margin-top: 10px auto; background-color: transparent; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
		<div id="rev_slider_4_1" class="rev_slider fullwidthabanner"
			style="display: none;" data-version="5.0.7">
			<ul>
				<!-- SLIDE  -->
				<li data-index="rs-1" data-transition="zoomout"
					data-slotamount="default" data-easein="Power4.easeInOut"
					data-easeout="Power4.easeInOut" data-masterspeed="2000"
					data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500"
					data-fsslotamount="7" data-saveperformance="off" data-title="Intro"
					data-description="">
					<!-- MAIN IMAGE --> <img
					src="<?php echo base_url();?>theme_assets/images/banner/Banner.JPG"
					alt="" data-bgposition="center center" data-bgfit="cover"
					data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
					data-no-retina>  <!-- LAYER NR. 1 -->
					<div
						class="tp-caption tp-caption1 NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
						id="slide-16-layer-1.3" data-x="['left','left','left','center']"
						data-hoffset="['300','300','150','80']"
						data-y="['top','top','top','top']"
						data-voffset="['250','150','190','110']"
						data-fontsize="['30','30','20','25']"
						data-lineheight="['100','100','80','50']" data-width="none"
						data-height="none" data-whitespace="nowrap"
						data-transform_idle="o:1;"
						
						style="z-index: 5; white-space: nowrap;">
						
					<h4 style="color:black;font-family: Arial, Helvetica, sans-serif;letter-spacing: 0.1825em;font-size:25px;margin-bottom:20px">Become An</h4>
					<h1 style="color:black;font-family: Arial, Helvetica, sans-serif;font-weight:bolder;font-size:55px;letter-spacing: 0.1825em;">EVENTS EXPERT</h1>
					<h4 style="color:black;font-family: Arial, Helvetica, sans-serif;letter-spacing: 0.1825em;font-size:25px;margin-top:20px">In a Few Clicks!</h4>
						
					</div>
					
					
						
				</li>
<li data-index="rs-2" data-transition="zoomout"
					data-slotamount="default" data-easein="Power4.easeInOut"
					data-easeout="Power4.easeInOut" data-masterspeed="2000"
					data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500"
					data-fsslotamount="7" data-saveperformance="off" data-title="Intro"
					data-description="">
					<!-- MAIN IMAGE --> <img
					src="<?php echo base_url();?>theme_assets/images/banner/banner3.jpg"
					alt="" data-bgposition="center center" data-bgfit="cover"
					data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
					data-no-retina> <!-- LAYER NR. 1 -->
					<div
						class="tp-caption tp-caption2 NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
						id="slide-16-layer-1.3" data-x="['left','left','left','center']"
						data-hoffset="['400','300','150','80']"
						data-y="['top','top','top','top']"
						data-voffset="['200','150','190','110']"
						data-fontsize="['20','20','20','25']"
						data-lineheight="['100','100','80','50']" data-width="none"
						data-height="none" data-whitespace="nowrap"
						data-transform_idle="o:1;"
						style="z-index: 5; white-space: nowrap;">
						<h1 style="font-family: Arial, Helvetica, sans-serif;">What People Are Saying</h1><br>
						<h1 style="font-family: Arial, Helvetica, sans-serif;">Jennifer Lim, 2017</h1>	
					</div>
					<div
						class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
						id="slide-16-layer-1.3" data-x="['left','left','left','center']"
						data-hoffset="['250','300','150','80']"
						data-y="['top','top','top','top']"
						data-voffset="['240','150','190','110']"
						data-fontsize="['20','20','20','25']"
						data-lineheight="['30','30','50','50']" data-width="none"
						data-height="none" data-whitespace="nowrap"
						data-transform_idle="o:1;"
						style="z-index: 5; white-space: nowrap;">
						<p style="font-family: Arial, Helvetica, sans-serif;">Events Muse saved me so much time in planning for my daughter's 21st birthday!<br>
						I was even complimented by my guests on how everything was so well organized!</p>
					</div>
					
				</li>

<li data-index="rs-3" data-transition="zoomout"
					data-slotamount="default" data-easein="Power4.easeInOut"
					data-easeout="Power4.easeInOut" data-masterspeed="2000"
					data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500"
					data-fsslotamount="7" data-saveperformance="off" data-title="Intro"
					data-description="">
					<!-- MAIN IMAGE --> <img
					src="<?php echo base_url();?>theme_assets/images/banner/banner3.jpg"
					alt="" data-bgposition="center center" data-bgfit="cover"
					data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
					data-no-retina> <!-- LAYER NR. 1 -->
					<div
						class="tp-caption tp-caption2 NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
						id="slide-16-layer-1.3" data-x="['left','left','left','center']"
						data-hoffset="['400','300','150','80']"
						data-y="['top','top','top','top']"
						data-voffset="['200','150','190','110']"
						data-fontsize="['20','20','20','25']"
						data-lineheight="['100','100','80','50']" data-width="none"
						data-height="none" data-whitespace="nowrap"
						data-transform_idle="o:1;"
						style="z-index: 5; white-space: nowrap;">
						<h1 style="font-family: Arial, Helvetica, sans-serif;">What People Are Saying</h1><br>
						<h1 style="font-family: Arial, Helvetica, sans-serif;">Jennifer Lim, 2017</h1>	
					</div>
					<div
						class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-0"
						id="slide-16-layer-1.3" data-x="['left','left','left','center']"
						data-hoffset="['250','300','150','80']"
						data-y="['top','top','top','top']"
						data-voffset="['240','150','190','110']"
						data-fontsize="['20','20','20','25']"
						data-lineheight="['30','30','50','50']" data-width="none"
						data-height="none" data-whitespace="nowrap"
						data-transform_idle="o:1;"
						style="z-index: 5; white-space: nowrap;">
						<p style="font-family: Arial, Helvetica, sans-serif;">Events Muse saved me so much time in planning for my daughter's 21st birthday!<br>
						I was even complimented by my guests on how everything was so well organized!</p>
					</div>


					
				</li>
			</ul>


		</div>
	</div>
	<!-- END REVOLUTION SLIDER -->
</div>
<div class="clearfix"></div>

<div id="sucess_div"
	style="position: fixed; z-index: 10004; top: 30px; right: 0px;">
	<h4 style="white-space: nowrap;" class="sucess_content"></h4>

</div>


<section id="plan_event">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Plan Your Event!</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="" style="background-color: white !important; text-align: left"> 
<div class="container" >
	<div class="row mj_toppadder20"
		>
		
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<form id="form_eventcreate" enctype="multipart/form-data">
				<input type="hidden" name="c_id" id="c_id" value="<?php echo $id;?>">

				<fieldset>

					<div class="row">
						<div class="col-sm-6">

							<div class="form-group vali_eventtitle">
								<label for="particular">Event Name *</label> <input type="text"
									name="eventtitle" placeholder="Event name" id="eventtitle"
									value="" class="form-control" required> <span class="text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">

							<div class="form-group vali_eventtitle">
								<div class="form-group vali_eventtype">
									<label for="particular">Event Type *</label> <input type="text"
									name="eventtype" placeholder="Event type" id="eventtype"
									value="" class="form-control" required> <span class="text-danger"></span>


								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group vali_eventstart">
								<label for="particular">Event Start Time *</label> <input
									type="text" name="eventstart" title="Start Time"
									id="eventstart" value="" class="form-control some_class"> <span
									class="text-danger"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group vali_eventend">
								<label for="particular">Event End Time *</label> <input
									type="text" name="eventend" title="End Time" id="eventend"
									value="" class="form-control some_class"> <span
									class="text-danger"></span>
							</div>
						</div>

					</div>
					<div class="row">

						<div class="col-sm-6">
							<div class="form-group vali_pax">
								<label for="particular">Number of Pax *</label> <input
									type="text" name="pax" title="Number" id="pax" value=""
									class="form-control"> <span class="text-danger"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group vali_budget">
								<label for="Loantype">Budget(S$) *</label><br>
								<div>
									<input type="text" name="budget" title="Budget" id="budget"
										value="" class="form-control">
								</div>

								<span class="text-danger"></span>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group vali_description">
								<label for="particular">Description </label>
								<textarea rows="3" cols="" id="description" name="description"
									class="form-control"></textarea>
								<span class="text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group vali_venue">
								<label for="particular">Location *</label> <input type="text"
									name="venue" title="Venue" id="venue" value=""
									class="form-control"> <span class="text-danger"></span>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
						
							<div class="form-group pull-right">
<?php if($id!=""){?>
								<input type="submit" class="btn btn-success form-control"
									value="Plan Now !" >
									<?php }else{?>
									<input type="submit" class="btn btn-success"
									value="Plan Now !" disabled="disabled">
									
									<span style="color:red">Please Login First !!!</span><?php }?>
							</div>
						</div>

					</div>
					



					<div class="form-group">
						<br>



					</div>
				</fieldset>
			</form>


		</div>
</div>

	</div>
</div>
</section>

<section id="service_provider">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Your Perfect Choice</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="background-color: white; text-align: left">
	<div class="container" >
	<div class="row mj_toppadder20" >
			<div class="mj_articleslider mj_bottompadder50">
				<div class="col-md-1"></div>
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="row">
				<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/emcee.jpg"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Emcees</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Emcees">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
				<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Entertainers.JPG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Entertainers</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/entertainer">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Photographers.JPG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Photographers</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Photographers">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					
					</div>
					<br>
					
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Event Prof.PNG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Event Professionals</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Event Professionals">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Venue.PNG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Venues</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Venues">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Catering.PNG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Catering</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Catering">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					
					
					
					<div class="col-sm-12 mj_topadder20"><br>
					<a href="<?php echo base_url();?>home/discover" class="btn btn-success pull-right" id="">Click Here more</a>
					</div>
					
					
					
					
				</div>
				<div class="col-md-1"></div>
				
				
				
			</div>
		</div>
		
	</div>
	</div>
	</section>
<section id="featured_talents">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Featured Talents & Venues</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	</section>
<div class="mj_bottompadder30" style="background: white">
	<div class="container">
		
		<div class="row hover06">
			
		</div>
	</div>
</div>

<script src="<?php echo base_url();?>js/index1.js"
	type="text/javascript"></script>

<script src="<?php echo base_url();?>theme_assets/js/jquery.js"></script>
<script
	src="<?php echo base_url();?>theme_assets/js/jquery.datetimepicker.full.js"></script>
<script>



$('.some_class').datetimepicker();




</script>


		