<script>
$(document).ready(function(){
	$("#update_pass").click(function(){

		var password	=$('#password').val();
		var cpassword	=$('#password1').val();
		var hash        =$('#hash').val();

        if(password == '' || cpassword==''){
        	$('.error').addClass('alert alert-danger').html("Please enter all fields.");
        }else if(password != cpassword){
        	$('.error').addClass('alert alert-danger').html("Password does not match");
        }else{
        	var dataString='password='+password+'&hash='+hash;
        	
        	$.ajax({
        	type: "post",
        	url:"<?php echo base_url(); ?>/home/password_update",
        	data:dataString  ,
        	success: function(data){
        		if(data==1){
        			$('.error').addClass('alert alert-success').html("Successfully password changed");
        			$('#final_reset').fadeOut('fast');
        			window.location.href = '<?php echo base_url();?>home';
        			
        		}else{
        			$('.error').addClass('alert alert-danger').html("Password not changed");
        		}

        	},
            error: function(jqXHR, textStatus) {
                alert( "Request failed: " + jqXHR );
            }
        	});
        }



	});
});
</script>
<section id="reset_section">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Reset Your Password</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="" style="background-color: white !important; text-align: left"> 
<div class="container" >
	<div class="row mj_toppadder20" style="margin-bottom:235px !important">
		
		<div class="col-md-3"></div>
		<div class="col-md-6">
		<div class="error"></div>
				<input type="hidden" id="hash" value="<?php echo $hash;?>">
					<div class="row">
						<div class="col-sm-12">

							<div class="form-group vali_eventtitle">
								<label for="particular">Enter Password *</label> <input type="password"
									name="" placeholder="Enter Your password" id="password"
									value="" class="form-control" required> <span class="text-danger"></span>
							</div>
						</div>
						<div class="col-sm-12">

							<div class="form-group vali_password1">
								<div class="form-group vali_eventtype">
									<label for="particular">Re Enter Password *</label> <input type="password"
									name="" placeholder="Confirm Password" id="password1"
									value="" class="form-control" required> <span class="text-danger"></span>


								</div>
							</div>
						</div>
						
						</div>
						<div class="row">
						<div class="col-sm-12">

							<button class="btn btn-success" id="update_pass" value="">Reset Password</button>
						</div>
						</div>

					</div>
					
				



					<div class="form-group">
						<br>



					</div>
				</fieldset>
			</form>


		</div>
</div>

	</div>
</div>
</section>