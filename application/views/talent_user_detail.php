<style>
@media ( max-width :500px) {
	iframe {
		width: 100% !important;
		height: auto;
	}
}
.mj_mainheading h2:after {
	border-bottom: 2px solid #f26522;
	width: 200px;
	top: 90%;
	left: 0%;
}

.hover06 figure img {
	-webkit-transform: rotate(15deg) scale(1.4);
	transform: rotate(15deg) scale(1.4);
	-webkit-transition: .3s ease-in-out;
	transition: .3s ease-in-out;
}

.hover06 figure:hover img {
	-webkit-transform: rotate(0) scale(1);
	transform: rotate(0) scale(1);
}

#gallery_list li {
	display: none;
}

#video_list li {
	display: none;
}

.mj_pagetitle2 .mj_pagetitleimg img {
	width: 100%;
	max-height: 140px !important;
	min-height: 270px !important;
}

.mj_social_media_section .mj_mainbtn {
	padding: 15px 45px;
	width: 200px;
}

.mj_joblogo img {
	width: 240px !important;
	height: 260px !important;
}

.mj_social_media_section ul li {
	float: left;
	list-style: none;
	padding-right: 20px;
	text-align: center;
	margin-bottom: 15px;
}
.font_7 {
    font: normal normal normal 20px/1.4em georgia,palatino,'book antiqua','palatino linotype',serif;
    color: #454545;
}
#mylist li{ 
display:none ;
}
</style>

<script>
$(document).ready(function () {
    size_li = $("#mylist li").size();
   
    x=4;

    if(size_li==0){
      $("#loadmore").hide();
    }
        
    $('#mylist li:lt('+x+')').show();
    $('#loadmore').click(function () {
        x= (x+4 <= size_li) ? x+4 : size_li;
        $('#mylist li:lt('+x+')').show();
       
    });
 
});
</script>
<script type="text/javascript">
$(document).ready(function () {
	
    size_li_1 = $("#video_list li").size();
    
    y=0;
    var size_1=size_li_1 - y;
   
    if(size_1 > 0){
    	size_1=size_1;
    }else{
    	size_1 =0;
    }
    $('#video_list li:lt('+y+')').show();
   $('#loadMore_video').val('Load more');
   
    $('#loadMore_video').click(function () {
        y= (y+5 <= size_li_1) ? y+5 : size_li_1;
        $('#video_list li:lt('+y+')').show();
        var size2=size_li_1-y;
        $('#loadMore_video').val('Load more');
       
    });

  
});
</script>
<script type="text/javascript">
$(document).ready(function () {
	
    size_li_1 = $("#gallery_list li").size();
    
    y=6;
    var size_1=size_li_1 - y;
   
    if(size_1 > 0){
    	size_1=size_1;
    }else{
    	size_1 =0;
    }
    $('#gallery_list li:lt('+y+')').show();
   $('#loadMore_gallery').val('Load more');
   
    $('#loadMore_gallery').click(function () {
        y= (y+5 <= size_li_1) ? y+5 : size_li_1;
        $('#gallery_list li:lt('+y+')').show();
        var size2=size_li_1-y;
        $('#loadMore_gallery').val('Load more');
       
    });

  
});
</script>

<style>
.mj_social_media_section .mj_mainbtn {
	padding: 15px 10px;
}
</style>
<link href="<?php echo base_url();?>css/style1.css" rel="stylesheet"
	type="text/css" />
<div class="mj_toppadder80"></div>
<div class="mj_toppadder80" style="background-color: white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="mj_pageheading ">
							<h1 style="font-size: 51px; text-align: left"><?php echo $talents_detail->name;?> <span
									style="float: right"><img
									src="<?php echo base_url();?>theme_assets/images/3-out-of-5-stars.png"
									class="img-responsive" width="180" height="30"></span>
							</h1>

							<h4 style="text-align: left"> I'm <?php echo $talents_detail->category;?><span
									style="float: right"><?php if($id ==""){?><a
									data-toggle="modal" data-target="#login_modal" href="#"
									class="btn btn-info"
									style="background-color: rgba(232, 120, 72, 1); border: none;"
									data-text="Hire Me"><span>Hire Me</span></a><?php }else{?><a
									href="<?php echo base_url();?>clients/hire_me/<?php echo $talents_detail->talents_id;?>"
									class="btn btn-info"
									style="background-color: rgba(232, 120, 72, 1); border: none;"
									data-text="Hire Me"><span>Hire Me</span></a><?php }?></span>
							</h4>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

						<div class="mj_joblogo">
							
							<?php
			
			$prinimage = '<img id="myimage"
		src="' . base_url () . '/theme_assets/images/default_user.png"
		  alt=""   class="img-responsive">
    	';
			if ($talents_detail->talents_id!= '') {
				
				$files = glob ( 'talents_profilepic/*' ); // get all file names
				foreach ( $files as $file ) {
					if (is_file ( $file )) {
						$picname = "talent" .$talents_detail->talents_id . "pic";
						
						$photos_from_direc = explode ( '/', $file );
						$photoname = explode ( '.', $photos_from_direc [1] );
						if ($photoname [0] == $picname) {
							$prinimage = '<img id="myimage"
		src=" ' . base_url () . 'talents_profilepic/' . $picname . '"
		class="img-responsive" alt="Profile " >
    	';
						}
						
						// unlink($file);
					} // delete file
				}
			}
			
			echo $prinimage;
			?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="" style="background-color: white">
	<div class="container">
		<div class="row">
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
			<h2 style="text-align:left;color:#132460"><u>Biography</u></h2>
			
			<p class="font_7" style="text-align:left"><span style="font-style:italic;"><?php echo $talents_detail->service;?></span></p>
			</div>
		</div>
	</div>
</div>
<div class="" style="background-color: white">
	<div class="container">
		<div class="row mj_toppadder20">
		<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
			<h2 style="text-align:left;color:#132460"><u>My Portfolio</u></h2>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				
				<div id="">
				<ul class=" list-unstyled " id="mylist">
	
													<?php
													$talents_id =  $talents_detail->talents_id;
													$images = glob ( "talents_images/talents" . $talents_id . "/photos/*.*" );
													$sn = 0;
													foreach ( $images as $image ) {
														$sn ++;
														?>

                    <li class="col-md-6 mix mix-all holiday"
						id="<?php echo $sn;?>_div" data-value="1">
						<div class="row">
							<div class="mj_gallary_img">
								<img src="<?php echo base_url();?><?php echo $image;?>"
									alt="gallery" class="img-responsive"
									style="height: 150px !important;">
								<div class="mj_overlay">
									<div class="mj_gallary_info">
										<h5 class="animated fadeInDown">
											<a class="fancybox" data-fancybox-group="gallery"
												href="<?php echo base_url();?><?php echo $image;?>" title="">View</a>
										</h5>
										<button class="btn btn-danger photo_delete"
											id="<?php echo $image;?>_div<?php echo $sn;?>" class="delete">Delete</button>
									</div>
								</div>
							</div>
						</div>
					</li>
         
            <?php }?>
             </ul>
          
             </div>&nbsp;
              <center>              
            <button class="btn btn-info "  id="loadmore">Load More</button></center>
			</div>
 
			<div
				class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mj_toppadder10 mj_bottompadder20">
		
		<?php
		
		$sn = 1;
		$first="";
		foreach ( $talents_video as $row ) {
			
			if ($sn == 2)
				break;
			
			$first = $row ['link'];
			
			$sn ++;
		}
		if($first!=''){
		$first_explode = explode ( 'embed/', $first );
		$second_explode = explode ( '"', $first_explode [1] );
		}
		?>
									
									<?php
									
									$sn = 1;
									$final_list = array ();
									foreach ( $talents_video as $row ) {
										
										if ($sn == 2)
											continue;
										
										$list = $row ['link'];
										$list_explode = explode ( 'embed/', $list );
										$second_list = explode ( '"', $list_explode [1] );
										$final_list [] = $second_list [0];
										$sn ++;
									}
									
									?>
										
									<?php
									
									$playlist = implode ( ',', $final_list );
									
									?>
								

									<iframe width="450" height="300"
					src="http://www.youtube.com/embed/<?php echo $second_explode[0];  ?>?playlist=<?php echo $playlist;?>"
					frameborder="0" allowfullscreen></iframe>


			</div>
			</div>
		</div>
	</div>
</div>

<div class="" style="background-color: white">
	<div class="container">
		<div class="row ">
			<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
		
				<h2 style="text-align:left;color:#132460"><u>My Rates</u></h2>
				
			<?php foreach($price as $row){?>
			   <h4 style="text-align:left;"> <b><?php echo $row['title'];?>:</b> S$ <?php echo $row['price'];?> <?php echo $row['charge_type'];?>  -- <?php echo $row['description'];?></h4><br>
			    
			    <?php }?>
			</div>
			
		</div>
		<span
									style="float: right">Service Not Listed?<br>
									<?php if($id ==""){?><a
									data-toggle="modal" data-target="#login_modal" href="#"
									class="btn btn-info"
										style="background-color: rgba(0, 108, 109, 1); border: none;"
									data-text="Get A Quote"><span>Get A Quote</span></a><?php }else{?><a
									 href="<?php echo base_url();?>clients/quote_me/<?php echo $talents_detail->talents_id;?>"
									class="btn btn-info"
										style="background-color: rgba(0, 108, 109, 1); border: none;"
									data-text="Get A Quote"><span>Get A Quote</span></a><?php }?></span>
		</div>
		</div>
<div class="mj_bottompadder40" style="background-color: white">
	<div class="container">
		<div class="row ">
			<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
		
				<h2 style="text-align:left;color:#132460"><u>My Reviews</u></h2>
				
		
			</div>
			
		</div>
		
		</div>
		</div>


<script src="<?php echo base_url();?>js/index1.js"
	type="text/javascript"></script>