 <div class="mj_error_wrapper mj_toppadder80 mj_bottompadder40">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
                    <div class="mj_mainheading ">
                        <h1>p<span>age</span> n<span>ot</span> f<span>ound</span></h1>
                        <p>It seems we can't find what you're looking for.
                            <br>Perhaps searching can help.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-12 col-xs-12">
                    <div class="mj_error_search mj_toppadder50">
                        
                        <p class="mj_toppadder50 mj_bottompadder70">Go Back to the <a href="<?php echo base_url();?>">home page</a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>