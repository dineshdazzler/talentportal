<script>   //no need to specify the language
 $(function(){
  $("#send").click(function(e){  // passing down the event 
	  var actname=$('#actname').val();
		 var personname	=$('#personname').val();
		 var email	=$('#email').val();
			var secondemail=$('#secondemail').val();
			var contact_number=$('#contact_number').val();
			  var other_number	=$('#other_number').val();
			var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			if( actname == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Act Name");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( personname == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Contact Person Name");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( email == '' || !emailFilter.test(email)){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Primary Valid Email");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( secondemail == '' || !emailFilter.test(secondemail)){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Secondary Valid Email");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( contact_number == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Contact Number");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( other_number == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Secondary Contact Number");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			
			else{
    $.ajax({
       url:'<?php echo base_url();?>home/send_mail_talent',
       type: 'POST',
       data: $("#company_contact").serialize(),
       success: function(){
    	   $('.sucess_content').addClass('alert alert-success').html("Message Successfully Sent");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             });
	 		 setTimeout(function(){
	 	 			window.location.href="<?php echo base_url();?>home/contactus_talent" ;},1000);
       },
       error: function(){
           alert("Fail")
       }
   });
			}
   e.preventDefault(); // could also use: return false;
 });
});
</script>
<div  id="sucess_div"
	style="position: fixed; z-index: 10004; top: 30px; right: 0px;">
	<h4 style="white-space: nowrap;" class="sucess_content"></h4>
	
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="">
				<div
					class="">
					<div class="mj_mainheading mj_toppadder80 mj_bottompadder50">
						<h3>
							C<span>ontact</span> F<span>orm</span>
						</h3>
						<p></p>
					</div>
				</div>
				<div
					class="">
					<div class="row">
						<form id="company_contact" class="mj_bottompadder80">
							<div class="form-group col-lg-6">
							<label>Act Name:</label>
								<input type="text" name="actname" id="actname" class="form-control" placeholder="Act name">
							</div>
							<div class="form-group col-lg-6">
							<label>Contact Person:</label>
								<input type="text" name="personname" id="personname" class="form-control" placeholder="Contact Person Name">
							</div>
							<div class="form-group col-lg-6">
							<label>Contact Email:</label>
								<input type="email" id="email" name="email" class="form-control"
									placeholder="Contact Email Address">
							</div>
							<div class="form-group col-lg-6">
							<label>Second Email:</label>
								<input type="email" id="secondemail" name="secondemail" class="form-control"
									placeholder="Second Email Address">
							</div>
							<div class="form-group col-lg-6">
							<label>Contact Number:</label>
								<input type="text" name="contact_number" id="contact_number" class="form-control" placeholder="Contact Phone Number">
							</div>
							<div class="form-group col-lg-6">
							<label>Other Number:</label>
								<input type="text" name="other_number" id="other_number" class="form-control" placeholder="Other Number">
							</div>
							<center><button type="button" id="send" class="btn btn-success">send a message</button></center>
							
						</form>
					</div>
				</div>
			</div>
		</div>

		
	<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="mj_mainheading mj_toppadder80 mj_bottompadder50">
				<h3>
					c<span>ontact</span> i<span>nformation</span>
				</h3>
				
			</div>
			     <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                   <h4>Singapore Office:</h4>
				<p style="font-size: 15px;">31 Woodlands Close,</p>
				<p style="font-size: 15px;">Woodlands Horizon Bizhub,</p>
				<p style="font-size: 15px;">#06-01 Singapore 737855.</p>
			<h5>TEL/FAX:</h5>	
				<p style="font-size: 15px;">Company Mobile : (65) 98455562</p>
                                </div>
                                
                                <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                    <h4>Malaysia Office:</h4>
				<p style="font-size: 15px;">27 A & B, Jalan Sutera Tanjung 8/3,</p>
				<p style="font-size: 15px;">Taman Suetra Utama 81300 Skudai,</p>
				<p style="font-size: 15px;">Johor Bahru Malaysia.</p>
			<h5>TEL/FAX:</h5>	
				<p style="font-size: 15px;">(607) 558 1812</p>
			<h5>Email:</h5>	
				<p style="font-size: 15px;"><a href="admin@heartneucircus.com" target="_top">admin@heartneucircus.com</a></p>
				
                                </div>
			</div>

	</div>
	
</div>


       