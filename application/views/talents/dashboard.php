
<link href='<?php echo base_url();?>theme_assets/calendar/fullcalendar.min.css' rel='stylesheet' />
<link href='<?php echo base_url();?>theme_assets/calendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<script src='<?php echo base_url();?>theme_assets/calendar/moment.min.js'></script>
<script src='<?php echo base_url();?>theme_assets/calendar/fullcalendar.min.js'></script>
<link rel="stylesheet" href="<?php echo base_url();?>theme_assets/css/scroll.css" />
<script src='<?php echo base_url();?>theme_assets/js/jquery.scrollto.js'></script>
<link rel="stylesheet" href="<?php echo base_url();?>theme_assets/css/testmonial.css"> <!-- Resource style -->
<script src="<?php echo base_url();?>theme_assets/js/modernizr.js"></script> <!-- Modernizr -->
<link rel="stylesheet" href="<?php echo base_url();?>theme_assets/css/reset.css">
<script>

	$(document).ready(function() {
		
		$('#calendar').fullCalendar({
			defaultDate: new Date(),
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			eventSources:[
		         {
		             events: function(start, end, timezone, callback) {
		                 $.ajax({
		                 url: '<?php echo base_url(); ?>talents/get_events/',
		                 dataType: 'json',
		                 data: {
			                 
		                 // our hypothetical feed requires UNIX timestamps
		                 start: start.unix(),
		                 end: end.unix()
		                 },
		                 success: function(msg) {
			                console.log(msg);
			                console.log("ds");
		                     var events = msg.events;
		                     callback(events);
		                 },
		                 error: function (xhr, ajaxOptions, thrownError) {
		                     console.log(xhr.status);
		                     console.log(xhr.responseText);
		                     console.log(thrownError);
		                 }
		                 });
		             }
		         },
		     ],
		     eventMouseover: function(calEvent, jsEvent) {
		    	    var tooltip = '<div class="tooltipevent" style="width:100px;height:100px;color:white;background:#25b982;border-radius:10px;position:absolute;z-index:10001;text-align:center">' + calEvent.title +'<br>'+calEvent.onstart+'<br>'+calEvent.location+'</div>';
		    	    var $tooltip = $(tooltip).appendTo('body');

		    	    $(this).mouseover(function(e) {
		    	        $(this).css('z-index', 10000);
		    	        $tooltip.fadeIn('500');
		    	        $tooltip.fadeTo('10', 1.9);
		    	    }).mousemove(function(e) {
		    	        $tooltip.css('top', e.pageY + 10);
		    	        $tooltip.css('left', e.pageX + 20);
		    	    });
		    	},

		    	eventMouseout: function(calEvent, jsEvent) {
		    	    $(this).css('z-index', 8);
		    	    $('.tooltipevent').remove();
		    	},
		     eventClick: function(calEvent, jsEvent, view) 
		     {          
		    	 var content = '<h3>'+calEvent.title+'</h3>' + 
	                '<p><b>Start:</b> '+calEvent.start+'<br />' ;

	            tooltip.set({
	                'content.text': content
	            })
		     },
		});
		
	});

</script>

<style>

	
	#calendar {
		max-width: 900px;
		margin: 10px;
		
	}

</style>
<style>
#mylist li{ 
display:none ;
}
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}
</style>
<style type="text/css">
@media ( max-width :500px) {
	iframe {
		width: 100% !important;
		height: auto;
	}
}

@media ( max-width :991px) {
	.mj_pagetitle2 .mj_pagetitle_inner {
		position: absolute;
		bottom: -20%;
		left: 0;
		width: 60%;
	}
	.mj_feature_product_img img {
		width: 100% !important;
	}
	iframe {
		width: 100% !important;
	}
	.service {
		width: 200px !important;
	}
}

.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive,
	.thumbnail a>img, .thumbnail>img {
	display: block;
	height: 250px ;
}

.mj_gallary_img {
	float: left;
	width: auto;
	position: relative;
	margin: 10px;
}
.title{
font-family:Brandon Grostesque;
font-size:50px;
}
.content{
font-family:Georgia !important;
font-size:18px ;
}
</style>


<script type="text/javascript">
function add_profilepic()
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

   
        url = "<?php echo site_url('talents/ajax_add')?>";
   

    // ajax adding data to database
<?php $picname="talents".$talents_info->talents_id."pic";?>
    var formData = new FormData($('#form')[0]);
  
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        { 


           
            if(data.status) //if success close modal and reload ajax table
            {location.reload(true);
                alert(data.msg);
                $('#modal_form').modal('hide');
                
               
            }
            else{
            	 alert(data.msg);
                }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //  alert(textStatus+""+errorThrown+""+jqXHR);
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

$(document).ready(function(){
	$("#other_div").hide();

	$("#act_type").change(function(){

		var val=$("#act_type").val();

		 if(val == "others"){
             $("#other_div").show();
		 }else{
			 $("#other_div").hide();
		 }
	   
	});
	/* Signup process */
    $('#check4').click(function() {
        
      if ($(this).is(':checked')) {
          $('#save_price').removeAttr('disabled');
      } else {
         
           $('#save_price').attr('disabled', 'disabled');
      }
  });


    $("#package_price").keyup(function(){

       
        var price      = $(this).val();
        
        var percentage =15;
        var calcPerc = (15 / 100 ) * price;
        var total =price - calcPerc;
        $('#total_price').val(total);

    });
	
	$( 'textarea#price_description' ).ckeditor();
	$( 'textarea#price_description1' ).ckeditor();
	$( 'textarea#price_description2' ).ckeditor();
	$( 'textarea#price_description3' ).ckeditor();
	$( 'textarea#price_description4' ).ckeditor();
	$( 'textarea#price_description5' ).ckeditor();
	$( 'textarea#price_description6' ).ckeditor();
	$( 'textarea#price_description7' ).ckeditor();
	$( 'textarea#price_description8' ).ckeditor();
/*Portfolio page  function*/


	$("form#portfolio").submit(function(event){
     
		  //disable the default form submission
		  event.preventDefault();

		  //grab all form data  
		  var formData = new FormData($(this)[0]);
		 
		 $.ajax({
		    url: '<?php echo base_url();?>talents/portfolio_update',
		    type: 'POST',
		    data: formData,
		    async: false,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function (data) {
              
           		if(data==1){
           		 $('.sucess_content').addClass('alert alert-success').html("sucessfully Updated");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           			
           			
           		}else {
           		 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           			
           			
           		}

           	},
               error: function(jqXHR, textStatus) {
                   alert( "Request failed: " + jqXHR );
               }
           	});

               

       


   });


/*You tube video insertion*/

$("#youtube_add").click(function(){
	
       var t_id=$("#t_id").val();
       var youtube_link=$("#youtube_link").val();
       
     

       if(youtube_link == '' ){
    	   $('.sucess_content').addClass('alert alert-danger').html("Enter youtube link!!");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             }); 
       	if(youtube_link==""){
       	 $('.sucess_content').addClass('alert alert-danger').html("Enter youtube link!!");
		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#sucess_div").slideUp(500);
	             }); 
			}
       

       }else{

           	var dataString='youtube_link='+youtube_link+'&t_id='+t_id;
   			
           	$.ajax({
           	type: "POST",
           	url:"<?php echo base_url(); ?>talents/youtube_insert",
           	data:dataString,
           	success: function(data){
           		$("#youtube_link").val("");
           		$("#video_exists").hide();
           		$('#video_section').show();
           		$('#video_section').html(data);
           		

           	},
               error: function(jqXHR, textStatus) {
                   alert( "Request failed: " + jqXHR );
               }
           	});

               

       } 


   });

/*You tube link delete */

	$('#menu1').on("click", ".link_delete", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		var id=$(this).attr('id');
      
		var dataString='id='+id;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/youtube_delete",
			data:dataString  ,
			success: function(data){
		   $("#"+id).hide();
            if(data == 1){
            	$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
				  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });
				
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

/*Photo delete delete */
$('.photo_delete').click( function(){
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
        var arr=id.split('_div');
        var org_photo=arr[0];
       
		var dataString='id='+org_photo;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/photo_delete",
			data:dataString  ,
			success: function(data){
		  $("#"+arr[1]+"_div").hide();
            if(data == 1){
				
				$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
				  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

//Clients save 
$("form#client_form").submit(function(event){
 
  //disable the default form submission
  event.preventDefault();

  //grab all form data  
  var formData = new FormData($(this)[0]);
 
  $.ajax({
    url: '<?php echo base_url();?>talents/client_insert',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
    	$("#client_name").val("");
    	$("#client_photo").val("");
    	$("#cleint_exists").hide();
   		$("#cleint_new").html(data);
   		
   		
    }
    ,
	error: function(jqXHR, textStatus) {
		alert( "Request failed: " + jqXHR );
	}
  });
 
  return false;
});


//Client update
//Clients save 
$("form#edit_client_form").submit(function(event){
 
  //disable the default form submission
  event.preventDefault();

  //grab all form data  
  var formData = new FormData($(this)[0]);
 
  $.ajax({
    url: '<?php echo base_url();?>talents/client_update',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
    	
    	$("#cleint_exists").hide();
   		$("#cleint_new").hide();
   		$('#cleint_update').html(data);
   		
   		$('.sucess_content').addClass('alert alert-success').html("Updated successfully");
		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#sucess_div").slideUp(500);
	             });
   		
    }
    ,
	error: function(jqXHR, textStatus) {
		alert( "Request failed: " + jqXHR );
	}

  // $("#client_edit").modal();
  });
 
  return false;
});


//Client delete
$('#menu3').on("click", ".client_delete", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
		 var tid=$("#t_id").val();
       
		var dataString='id='+id+'&tid='+tid;
		$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/client_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){

            	$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
		 		
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

//Client Edit
$('#menu3').on("click", ".edit_client", function (e) {

	var id = $(this).attr('id');

	var dataString='id='+id;
	$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>talents/client_edit/",
		data:dataString  ,
		timeout: 5000,
		success: function(data){
			$("#client_edit").modal();
			$("#rev").html(data);

		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});

});


/*Show package panel*/


$("#package_panel").hide();
$('#menu4').on("click", "#add_package", function (e) {
	var $this = $(this);

    // Current click count is previous click count +1
    var clickCount = ($this.data("click-count") || 0) + 1;

    // Save current click count
    $this.data("click-count", clickCount);


 
	$("#package_panel").show();
	$("#add_package").prop("disabled", true);
	 $('#title').click();
 
	
});


/*price package */
$('#menu4').on("click", "#save_price", function (e) {

	var title=$("#price_title").val();
	var tid=$("#tid").val();
	var description=$("#price_description").val();
	var cancel_policy = $("input[name='policy']:checked").val();
	var charge_type=$("#charge_type").val();
	var min=$("#min").val();
	var max=$("#max").val();
	var price=$("#package_price").val();
	var talent_receive=$("#total_price").val();
   
	if(title == '' || price =='' || price == 0){

        if(title ==''){
        	$(".vali_title").addClass("has-error");
        }

        if(price =='' || price ==0){
        	$(".vali_price").addClass("has-error");
        	alert("Price can not be empty or zero");
        }
         
	}else{

      var dataString='tid='+tid+'&title='+title+'&description='+description+'&cancel_policy='+cancel_policy+'&charge_type='+charge_type+'&min='+min+'&max='+max+'&price='+price+'&talent_receive='+talent_receive;
		
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>talents/price_insert",
			data:dataString  ,
			
			success: function(data){
				$('.sucess_content').addClass('alert alert-success').html("Successfully done");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
		 		setTimeout(function(){
		 			window.location.href = '<?php echo base_url();?>talents/talents_portfolio/pricing';},3000);
				
				
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});



	}

	

});

/* Delete price package */
$('#menu4').on("click", ".delete_price", function (e) {

	
	var id = $(this).attr('id');

	var dataString='id='+id;
	$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>talents/delete_price/",
		data:dataString  ,
		timeout: 5000,
		success: function(data){
			$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             }); 
	 		setTimeout(function(){
	 			window.location.href = '<?php echo base_url();?>talents/talents_portfolio/pricing';},3000);
			
			

		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});
	

});

/* Update price package */
$('#menu4').on("click", ".update_price", function (e) {

	
	var id = $(this).attr('id');
	var price_id=$("#price_id"+id).val();
	var title=$("#price_title"+id).val();
	var tid=$("#tid").val();
	var description=$("#price_description"+id).val();
	var cancel_policy = $("input[name='policy"+id+"']:checked").val();
	var charge_type=$("#charge_type"+id).val();
	var min=$("#min"+id).val();
	var max=$("#max"+id).val();
	var price=$("#package_price"+id).val();
	var talent_receive=$("#total_price"+id).val();
   alert(cancel_policy);
	if(title == '' || price =='' || price == 0){

        if(title ==''){
        	$(".vali_title").addClass("has-error");
        }

        if(price =='' || price ==0){
        	$(".vali_price").addClass("has-error");
        	alert("Price can not be empty or zero");
        }
         
	}else{

      var dataString='price_id='+price_id+'&tid='+tid+'&title='+title+'&description='+description+'&cancel_policy='+cancel_policy+'&charge_type='+charge_type+'&min='+min+'&max='+max+'&price='+price+'&talent_receive='+talent_receive;
		
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>talents/price_update",
			data:dataString  ,
			
			success: function(data){
				 $('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			window.location.href = '<?php echo base_url();?>talents/talents_portfolio/pricing';},3000);
				
				
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});



	}
	
	

});
//pdf remove from portfolio
$('#home').on("click", ".docs_remove", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
		 
       
		var dataString='id='+id;
		//$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/docs_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){

            	 $('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           	
			
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

//pdf remove from pricing
$('#menu4').on("click", ".price_remove", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
		 
       
		var dataString='id='+id;
		//$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/docs_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){
            	 $('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			window.location.href="<?php echo base_url();?>talents/talents_portfolio/pricing";},3000);
            	
				
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});



});


</script>
<script>
$(document).ready( function () {
    $('.jobs').dataTable();
   


    $(".job_accept").click(function(){


        var val=$(this).attr('id');
        var val_split=val.split('_');
        var event_id=val_split[0];
        var talents_id=val_split[1];
       
        var r = confirm("Are you sure to accept the job?");
		  if (r == true){
		

			var dataString='talents_id='+talents_id+'&event_id='+event_id;
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>talents/job_accept/",
				data:dataString  ,
				success: function(data){
                 if(data ==1){
					alert("Sucessfully Accepted job admin person will contact you sonn !!");
					location.reload(true);
                 }else{
                 alert("Try Again later !!!");
                 }
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
		}


        });

    $(".job_decline").click(function(){


        var val=$(this).attr('id');
        var val_split=val.split('_');
        var event_id=val_split[0];
        var talents_id=val_split[1];

        var r = confirm("Are you sure to decline this the job?");
		  if (r == true){
		

			var dataString='talents_id='+talents_id+'&event_id='+event_id;
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>talents/job_decline/",
				data:dataString  ,
				success: function(data){
                 if(data ==1){
					alert("Job declined");
					location.reload(true);
                 }else{
                 alert("Try Again later !!!");
                 }
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
		}


        });


    
} );



</script>
<script>
$(document).ready(function () {
    size_li = $("#mylist li").size();
    if(size_li==0){
        $("#loadmore").hide();
      }
    x=4;
   
        
    $('#mylist li:lt('+x+')').show();
    $('#loadmore').click(function () {
        x= (x+4 <= size_li) ? x+4 : size_li;
        $('#mylist li:lt('+x+')').show();
       
    });
 
});
</script>
<section id="profile_summary">
<div class=" mj_toppadder80 mj_bottompadder20"
	style="background-color: rgba(142, 230, 203, 1);">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
				<div class="mj_addsection ">
					<h3 class="title"
						>Your
						Dashboard</h3>
					<h4>Profile Summary, Jobs, Schedule & Reviews</h4>

				</div>
			</div>
		</div>
	</div>
</div>
<div id="sucess_div"
	style="position: fixed; z-index: 10004; top: 30px; right: 0px;">
	<h4 style="white-space: nowrap;" class="sucess_content"></h4>

</div>
<div id="sidebar">
            <div id="nav-anchor"></div>
            <nav>
                <ul type="circle" >
                    <li ><a href="#profile_summary">Profile Summary</a></li>
                    <li ><a href="#jobs_div">Jobs</a></li>
                    <li ><a href="#schedule_div">Schedule</a></li>
                    <li><a href="#reviews_div">Your Reviews</a></li>
                    
                </ul>
            </nav>
        </div>
        
<div class="mj_bottompadder80 mj_toppadder20 content" >
	<div class="container">

		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
				<h2 style="text-align: center" id="act_type"><?php echo $talents_info->category_name;?></h2>
				<div class="col-sm-6">
					<h4>
						<b>Name :</b><?php echo $talents_info->name;?> <?php if($talents_info->gender=="male"){ echo "(M)";}elseif ($talents_info->gender=="female"){ echo "(F)";}?></h4>
				</div>
				<div class="col-sm-6">
					<h4>
						<b>Experience :</b><?php echo $talents_info->experience;?> </h4>
				</div>
				<div class="col-sm-2">
					<h4>
						<b>Contact:</b></h4></div>
						
						<div class="col-sm-8">
					<h4>
						<?php echo $talents_info->phone_number;?> ,
		<?php echo $talents_info->email; ?>
			</h4>
				</div>
				<div class="col-sm-6 ">
					<h4>
						<b>Address :</b><?php echo $talents_info->address;?> </h4>
				</div>
				<div class="col-sm-6">
					<h4>
						<b>Birthday :</b><?php echo $talents_info->dob;?> </h4>
				</div>

				<div class="col-sm-12 "
					style="background-color: #f3f5f6; padding: 10px; border-radius: 10px">
					<h4>
						<?php echo $talents_info->service;?> </h4>
				</div>

			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
			<?php
			
			$prinimage = '<img id="myimage"
		src="' . base_url () . '/theme_assets/images/default_user.png"
		class="img-circle" alt="Cinque Terre" width="200" height="200">
    	';
			if ($talents_info->id != '') {
				
				$files = glob ( 'talents_profilepic/*' ); // get all file names
				foreach ( $files as $file ) {
					if (is_file ( $file )) {
						$picname = "talent" . $talents_info->talents_id . "pic";
						
						$photos_from_direc = explode ( '/', $file );
						$photoname = explode ( '.', $photos_from_direc [1] );
						if ($photoname [0] == $picname) {
							$prinimage = '<img id="myimage"
		src=" ' . base_url () . 'talents_profilepic/' . $picname . '"
		class="img-circle" alt="Profile " width="200" height="200">
    	';
						}
						
						// unlink($file);
					} // delete file
				}
			}
			
			echo $prinimage;
			?>
			</div>
			<div class="col-md-3"> </div>
		</div>

		<div class="row mj_toppadder20">
			<div class="col-md-1"></div>
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<h2>My Portfolio</h2>
				<div id="">
				<ul class=" list-unstyled " id="mylist">
	
													<?php
													$talents_id = $id;
													$images = glob ( "talents_images/talents" . $talents_id . "/photos/*.*" );
													$sn = 0;
													foreach ( $images as $image ) {
														$sn ++;
														?>

                    <li class="col-md-6 mix mix-all holiday"
						id="<?php echo $sn;?>_div" data-value="1">
						<div class="row">
							<div class="mj_gallary_img">
								<img src="<?php echo base_url();?><?php echo $image;?>"
									alt="gallery" class="img-responsive"
									style="height: 150px !important;">
								<div class="mj_overlay">
									<div class="mj_gallary_info">
										<h5 class="animated fadeInDown">
											<a class="fancybox" data-fancybox-group="gallery"
												href="<?php echo base_url();?><?php echo $image;?>" title="">View</a>
										</h5>
										<button class="btn btn-danger photo_delete"
											id="<?php echo $image;?>_div<?php echo $sn;?>" class="delete">Delete</button>
									</div>
								</div>
							</div>
						</div>
					</li>
         
            <?php }?>
             </ul>
            <center> <button class="btn btn-info"  id="loadmore">Load More</button></center>
             </div>
			</div>

			<div
				class="col-lg-5 col-md-5 col-sm-12 col-xs-12 mj_toppadder70 mj_bottompadder20">
		
		<?php
		
		$sn = 1;
		$first="";
		foreach ( $talents_video as $row ) {
			
			if ($sn == 2)
				break;
			
			$first = $row ['link'];
			
			$sn ++;
		}
		if($first !=''){
		$first_explode = explode ( 'embed/', $first );
		$second_explode = explode ( '"', $first_explode [1] );
		}
		?>
									
									<?php
									
									$sn = 1;
									$final_list = array ();
									foreach ( $talents_video as $row ) {
										
										if ($sn == 2)
											continue;
										
										$list = $row ['link'];
										$list_explode = explode ( 'embed/', $list );
										$second_list = explode ( '"', $list_explode [1] );
										$final_list [] = $second_list [0];
										$sn ++;
									}
									
									?>
										
									<?php
									
									$playlist = implode ( ',', $final_list );
									
									?>
								

									<iframe width="450" height="320"
					src="http://www.youtube.com/embed/<?php echo $second_explode[0];  ?>?playlist=<?php echo $playlist;?>"
					frameborder="0" allowfullscreen></iframe>


			</div>
		</div>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<h2>My Rates</h2>
				
				<?php foreach($price as $row){?>
			   <h4> <b><?php echo $row['title'];?>:</b> S$ <?php echo $row['price'];?> <?php echo $row['charge_type'];?>  -- <?php echo $row['description'];?></h4><br>
			    
			    <?php }?>
			</div>
		</div>
	</div>
</div>
</section>
<section id="jobs_div">
<div class="  " 
	style="background-color: rgba(142, 230, 203, 1);">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
				<div class="mj_addsection ">
					<h3
						class="title">Your
						Jobs</h3>


				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
<div class="row content">
	<div class="col-md-1"></div>
	<div class="col-lg-11 col-md-11 col-sm-12 col-xs-12">
	       <div class="">
            <div class="row">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                    <div class="mj_tabs mj_bottompadder50">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs mj_joblist" role="tablist">
                            <li role="presentation" class="active"><a href="#recentjobs" aria-controls="recentjobs" role="tab" data-toggle="tab">Pending Job Request's</a>
                            </li>
                            <li role="presentation"><a href="#featuredjobs" aria-controls="featuredjobs" role="tab" data-toggle="tab">Acceepted Jobs</a>
                            </li>
                             <li role="presentation"><a href="#declinedjobs" aria-controls="declinedjobs" role="tab" data-toggle="tab">Declined Jobs</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="recentjobs">
                                <div class="mj_tabcontent mj_toppadder30">
                                    <table class="table table-striped jobs">
                                        <thead>
													<tr>
														<th>S.No</th>
														<th>Event Details</th>

														<th>Total Hr's</th>
														<th>Price(S$)</th>
														<th>Location</th>
														<th>action</th>

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($pending_jobs as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><b>Event:</b><?php echo $row['event_name'];?></br>
														<b>Start:</b><?php echo $row['event_start'];?></br>
														<b>End:</b><?php echo $row['event_end'];?>
														
														
														
														</td>
														<td><?php echo $row['total_hours'];?></td>
														<td>S$<?php echo $row['talents_cost'];?></td>
														<td><?php echo $row['venue'];?></td>
														<td><button class="btn btn-success btn-sm job_accept" id="<?php echo $row['event_id'];?>_<?php echo $id;?>">Accept</button>
															<button class="btn btn-danger btn-sm job_decline" id="<?php echo $row['event_id'];?>_<?php echo $id;?>">Decline</button></td>


													</tr>

<?php }?>

												</tbody>
                                       
                                    </table>
                                    
                                </div>

                            </div>
                            <div role="tabpanel" class="tab-pane" id="featuredjobs">
                                <div class="mj_tabcontent mj_toppadder30">
                                    <table class="table table-striped jobs">
                                        <thead>
													<tr>
														<th>S.No</th>
														<th>Event Details</th>

														<th>Total Hr's</th>
														<th>Price(S$)</th>
														<th>Location</th>
														<th>action</th>

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($accepted_jobs as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><b>Event:</b><?php echo $row['event_name'];?></br>
														<b>Start:</b><?php echo $row['event_start'];?></br>
														<b>End:</b><?php echo $row['event_end'];?>
														
														
														
														</td>
														<td><?php echo $row['total_hours'];?></td>
														<td>S$<?php echo $row['talents_cost'];?></td>
														<td><?php echo $row['venue'];?></td>
														<td>
															<button class="btn btn-danger btn-sm job_decline" id="<?php echo $row['event_id'];?>_<?php echo $id;?>">Decline</button></td>


													</tr>

<?php }?>

												</tbody>
                                      
                                    </table>
                                    
                                </div>
                            </div>
                              <div role="tabpanel" class="tab-pane" id="declinedjobs">
                                <div class="mj_tabcontent mj_toppadder30">
                                    <table class="table table-striped jobs">
                                        <thead>
													<tr>
														<th>S.No</th>
														<th>Event Details</th>

														<th>Total Hr's</th>
														<th>Price(S$)</th>
														<th>Location</th>
														

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($declined_jobs as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><b>Event:</b><?php echo $row['event_name'];?></br>
														<b>Start:</b><?php echo $row['event_start'];?></br>
														<b>End:</b><?php echo $row['event_end'];?>
														
														
														
														</td>
														<td><?php echo $row['total_hours'];?></td>
														<td>S$<?php echo $row['talents_cost'];?></td>
														<td><?php echo $row['venue'];?></td>
														


													</tr>

<?php }?>

												</tbody>
                                    </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
               
         
            </div>
        </div>
	</div>
</div>
</div>
</section>
<section id="schedule_div">
<div class=" " 
	style="background-color: rgba(142, 230, 203, 1);">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
				<div class="mj_addsection ">
					<h3
						class="title">Your Schedule</h3>


				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
<div class="row mj_toppadder20">
	<div class="col-md-2"></div>
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" >
	<div id='calendar'></div>
	</div>
	</div>
	</div>
</section>
<section id="reviews_div">
<div class="" 
	style="background-color: rgba(142, 230, 203, 1);">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
				<div class="mj_addsection ">
					<h3
						class="title">Your Reviews
</h3>


				</div>
			</div>
		</div>
	</div>
</div>
<div class="container">
<div class="row mj_toppadder20 content">
	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
	<div class="cd-testimonials-wrapper cd-container">
	<ul class="cd-testimonials">
		<li>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
			<div class="cd-author">
				<img src="img/avatar-1.jpg" alt="Author image">
				<ul class="cd-author-info">
					<li>MyName</li>
					<li>CEO, AmberCreative</li>
				</ul>
			</div>
		</li>

		<li>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ea, perferendis error repudiandae numquam dolor fuga temporibus. Unde omnis, consequuntur.</p>
			<div class="cd-author">
				<img src="img/avatar-2.jpg" alt="Author image">
				<ul class="cd-author-info">
					<li>MyName</li>
					<li>Designer, CodyHouse</li>
				</ul>
			</div>
		</li>

		<li>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quam totam nulla est, illo molestiae maxime officiis, quae ad, ipsum vitae deserunt molestias eius alias.</p>
			<div class="cd-author">
				<img src="img/avatar-3.jpg" alt="Author image">
				<ul class="cd-author-info">
					<li>MyName</li>
					<li>CEO, CompanyName</li>
				</ul>
			</div>
		</li>
		
	</ul> <!-- cd-testimonials -->

	<a href="#0" class="cd-see-all">See all</a>
</div> <!-- cd-testimonials-wrapper -->

<div class="cd-testimonials-all">
	<div class="cd-testimonials-all-wrapper">
		<ul>
			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit totam saepe iste maiores neque animi molestias nihil illum nisi temporibus.</p>
				
				<div class="cd-author">
					<img src="img/avatar-1.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore nostrum nisi, doloremque error hic nam nemo doloribus porro impedit perferendis. Tempora, distinctio hic suscipit. At ullam eaque atque recusandae modi fugiat voluptatem laborum laboriosam rerum, consequatur reprehenderit omnis, enim pariatur nam, quidem, quas vel reiciendis aspernatur consequuntur. Commodi quasi enim, nisi alias fugit architecto, doloremque, eligendi quam autem exercitationem consectetur.</p>
				
				<div class="cd-author">
					<img src="img/avatar-2.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem quibusdam eveniet, molestiae laborum voluptatibus minima hic quasi accusamus ut facere, eius expedita, voluptatem? Repellat incidunt veniam quaerat, qui laboriosam dicta. Quidem ducimus laudantium dolorum enim qui at ipsum, a error.</p>
				
				<div class="cd-author">
					<img src="img/avatar-3.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero voluptates officiis tempore quae officia! Beatae quia deleniti cum corporis eos perferendis libero reiciendis nemo iusto accusamus, debitis tempora voluptas praesentium repudiandae laboriosam excepturi laborum, nisi optio repellat explicabo, incidunt ex numquam. Ullam perferendis officiis harum doloribus quae corrupti minima quia, aliquam nostrum expedita pariatur maxime repellat, voluptas sunt unde, inventore.</p>
				
				<div class="cd-author">
					<img src="img/avatar-4.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odit totam saepe iste maiores neque animi molestias nihil illum nisi temporibus.</p>
				
				<div class="cd-author">
					<img src="img/avatar-5.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis quia quas, quis illo adipisci voluptate ex harum iste commodi nulla dolor. Eius ratione quod ab!</p>
				
				<div class="cd-author">
					<img src="img/avatar-6.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur, dignissimos iure rem fugiat consequuntur officiis.</p>
				
				<div class="cd-author">
					<img src="img/avatar-1.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At temporibus tempora necessitatibus reiciendis provident deserunt maxime sit id. Dicta aut voluptatibus placeat quibusdam vel, dolore.</p>
				
				<div class="cd-author">
					<img src="img/avatar-2.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Corporis iusto sapiente, excepturi velit, beatae possimus est tenetur cumque fugit tempore dolore fugiat! Recusandae, vel suscipit? Perspiciatis non similique sint suscipit officia illo, accusamus dolorum, voluptate vitae quia ea amet optio magni voluptatem nemo, natus nihil.</p>
				
				<div class="cd-author">
					<img src="img/avatar-3.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolor quasi officiis pariatur, fugit minus omnis animi ut assumenda quod commodi, ad a alias maxime unde suscipit magnam, voluptas laboriosam ipsam quibusdam quidem, dolorem deleniti id.</p>
				
				<div class="cd-author">
					<img src="img/avatar-4.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At temporibus tempora necessitatibus reiciendis provident deserunt maxime sit id. Dicta aut voluptatibus placeat quibusdam vel, dolore.</p>
				
				<div class="cd-author">
					<img src="img/avatar-5.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>

			<li class="cd-testimonials-item">
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque tempore ipsam, eos suscipit nostrum molestias reprehenderit, rerum amet cum similique a, ipsum soluta delectus explicabo nihil repellat incidunt! Minima magni possimus mollitia deserunt facere, tempore earum modi, ea ipsa dicta temporibus suscipit quidem ut quibusdam vero voluptatibus nostrum excepturi explicabo nulla harum, molestiae alias. Ab, quidem rem fugit delectus quod.</p>
				
				<div class="cd-author">
					<img src="img/avatar-6.jpg" alt="Author image">
					<ul class="cd-author-info">
						<li>MyName</li>
						<li>CEO, CompanyName</li>
					</ul>
				</div> <!-- cd-author -->
			</li>
		</ul>
	</div>	<!-- cd-testimonials-all-wrapper -->

	<a href="#0" class="close-btn">Close</a>
</div> <!-- cd-testimonials-all -->
	
	</div>
	</div>
	</div>
</section>
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Person Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="<?php echo $id;?>" name="tid" />
					<div class="form-body">
						<div id="photo_upload">
							<div class="mbot error-message-highlight hidden"
								id="progress_report_status"
								style="font-size: 12px; text-transform: none;"></div>
							<label for="profile_photo_upload">Upload Picture</label>
							<div class="">

								<!-- Conditional comments are dropped from IE post IE 10 -->
								<input id="profile_photo_upload" name="photo" type="file"
									class="file-up">

							</div>

							<div
								style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
								JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
								in height and width
							</div>
							<div class="upload_status"></div>

							<div id="progress_report" style="">
								<div id="progress_report_name"></div>

								<div id="progress_report_bar_container"
									style="height: 5px; margin-top: 10px">
									<div id="progress_report_bar"
										style="background-color: #cb202d; width: 0; height: 100%;"></div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()"
					class="btn btn-primary">Upload</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->

<div class="modal fade" id="gallery_photo" tabindex="-1" role="dialog"
	aria-labelledby="gallery_photo">
	<div class="modal-dialog" role="document">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->

			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div id="gallery_form">
					Photo Upload

					<form method="POST"
						action="<?php echo base_url();?>talents/photo_upload"
						enctype="multipart/form-data">
						<input type="hidden" value="<?php echo $id;?>" name="tid" />

						<div id="filediv">
							<input name="photo[]" id="file" type="file" />
						</div>
						<br /> <input type="button" id="add_more"
							class="upload btn btn-info" value="Add More Files" /> <input
							type="submit" value="Upload File" id="upload"
							class="upload btn btn-success" />
					</form>

				</div>
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>


<div class="modal fade" id="client_edit" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

				</button>
				<h4>Edit Client</h4>
			</div>


			<form id="edit_client_form" enctype="multipart/form-data">
				<div class="reviews-form-box" id="rev"></div>
			</form>



		</div>
	</div>
</div>


<div class="modal fade" id="price_file" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

				</button>
				<h4>Upload file</h4>
			</div>


			<form id="price_upload" method="POST"
				action="<?php echo base_url();?>talents/price_docsupload"
				enctype="multipart/form-data">
				<br> <br>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-6">
						<div class="form-group ">
							<input type="hidden" value="<?php echo $id;?>" name="tid" /> <input
								type="file" name="price_pdf" class="form-control" accept=".pdf">

						</div>
					</div>
					<div class="col-sm-4">

						<div class="form-group ">
							<input type="submit" class="btn btn-success" name="submit"
								value="Save">
						</div>
					</div>
				</div>






			</form>



		</div>
	</div>
</div>


<script type="text/javascript">
var abc = 0; //Declaring and defining global increement variable

$(document).ready(function() {

//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
    $('#add_more').click(function() {
        $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                $("<input/>", {name: 'photo[]', type: 'file', id: 'file'}),        
                $("<br/><br/>")
                ));
    });

//following function will executes on change event of file input to select different file	
$('body').on('change', '#file', function(){
            if (this.files && this.files[0]) {
                 abc += 1; //increementing global variable by 1
				
				var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/ width='150' height='150'></div>");
               
			    var reader = new FileReader();
                reader.onload = imageIsLoaded;
                
                reader.readAsDataURL(this.files[0]);
               
			    $(this).hide();
                $("#abcd"+ abc).append($("<img/>", {id: 'img', src: '<?php echo base_url();?>theme_assets/images/x.png', alt: 'delete',width:'30',height:'40'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

//To preview image     
    function imageIsLoaded(e) {
       
       
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
      
        var name = $(":photo").val();
        alert(name);
        if (!name)
        {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });
});


</script>
<style>
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

.upload {
	background-color: #ff0000;
	border: 1px solid #ff0000;
	color: #fff;
	border-radius: 5px;
	padding: 10px;
	text-shadow: 1px 1px 0px green;
	box-shadow: 2px 2px 15px rgba(0, 0, 0, .75);
}

.upload:hover {
	cursor: pointer;
	background: #c20b0b;
	border: 1px solid #c20b0b;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, .75);
}

#file {
	color: green;
	padding: 5px;
	border: 1px dashed #123456;
	background-color: #f9ffe5;
}

#upload {
	margin-left: 45px;
}

#noerror {
	color: green;
	text-align: left;
}

#error {
	color: red;
	text-align: left;
}

#img {
	width: 40px;
	border: none;
	height: 50px;
	margin-left: -20px;
	margin-bottom: 91px;
}

.abcd {
	text-align: center;
}

.abcd img {
	height: 100px;
	width: 100px;
	padding: 5px;
	border: 1px solid rgb(232, 222, 189);
}

#formget {
	float: right;
}

.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive,
	.thumbnail a>img, .thumbnail>img {
	display: block;
	max-width: 180px;
	height: 250px ;
}

.mj_gallary_img {
	float: left;
	width: 180px;
	position: relative;
	margin: 10px;
}
</style>
<script src="<?php echo base_url();?>theme_assets/js/masonry.pkgd.min.js"></script>
<script src="<?php echo base_url();?>theme_assets/js/jquery.flexslider-min.js"></script>
<script src="<?php echo base_url();?>theme_assets/js/main.js"></script> 