

<body>
   
    <!--Loader End -->
  
  <div class="mj_shop_slider" >
	<div id="rev_slider_4_1_wrapper"
		class="rev_slider_wrapper fullwidthbanner-container"
		data-alias="classicslider1"
		style="margin-top: 30px auto; height:300px !important;background-color: transparent; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
		<div id="rev_slider_4_1" class="rev_slider fullwidthabanner"
			style="display: none;" data-version="5.0.7">
			<ul>
				<!-- SLIDE  --> 
				<li data-index="rs-1" data-transition="zoomout"
					data-slotamount="default" data-easein="Power4.easeInOut"
					data-easeout="Power4.easeInOut" data-masterspeed="2000"
					data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500"
					data-fsslotamount="7" data-saveperformance="off" data-title="Intro"
					data-description="">
					<!-- MAIN IMAGE --> <img
					src="https://www.effro.com/assets/images/client_banners/favtalent.jpg"
					alt="" data-bgposition="center center" data-bgfit="cover"
					data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg"
					data-no-retina> <!-- LAYER NR. 1 -->
					<div
						class="tp-caption NotGeneric-Title   tp-resizeme rs-parallaxlevel-20"
						id="slide-16-layer-1.3" data-x="['left','left','left','center']"
						data-hoffset="['500','300','150','80']"
						data-y="['top','top','top','top']"
						data-voffset="['190','150','190','110']"
						data-fontsize="['30','30','20','25']"
						data-lineheight="['100','100','80','50']" data-width="none"
						data-height="none" data-whitespace="nowrap"
						data-transform_idle="o:1;"
						data-transform_in="x:[100%];z:0;rX:45deg;rY:0deg;rZ:90deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;"
						data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;"
						data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
						data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
						data-start="1000" data-splitin="chars" data-splitout="none"
						data-responsive_offset="on" data-elementdelay="0.05"
						style="z-index: 5; white-space: nowrap;">
						<strong>Its Show time!</strong>
						
					</div>
					
<div class="col-sm-3"></div>

					<div class="mj_top_searchbox col-sm-9 mj_toppadder40">
					
						 <div class="mj_social_media_section" style="">
                       <ul>
                            <li><a href="post_job.html" class="mj_mainbtn mj_btnblue" data-text="Learn About!"><span>Learn About!</span></a>
                            </li>
                            <li><a href="contact.html" class="mj_mainbtn mj_btnyellow" data-text="Join as Talent"><span>Join as Talent</span></a>
                            </li>
                        </ul>
                       
                    </div>
                   
					</div>
				</li>


			</ul>


		</div>
	</div>
	<!-- END REVOLUTION SLIDER -->
</div>
<div  id="sucess_div"
	style="position: fixed; z-index: 10004; top: 30px; right: 0px;">
	<h4 style="white-space: nowrap;" class="sucess_content"></h4>
	
</div>
  <div class="mj_lightgraytbg mj_bottompadder30 mj_toppadder40">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0"
				style="float: right !important">

				<div class="mj_mainheading mj_side_heading mj_bottompadder50 ">
					<h2 style="color: #b62937 !important">
						l<span>ets start here !</span>
					</h2>
					<p>Choose from popular services</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="mj_articleslider mj_bottompadder50">
				<div class="col-md-3">
					<div class="mj_feature_product">
						<div class="mj_feature_product_img">

							<img src="<?php echo base_url();?>theme_assets/images/Magic.jpg"
								style="height: 350px; width: 300px"
								class="img-responsive" alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important">Magicians</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search">view all</a>
								</div>
							</div>
						</div>

					</div>
				</div>
				<div class="col-md-3">
					<div class="mj_feature_product">
						<div class="mj_feature_product_img">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important">Emcee</h5>
							</div>
							<img src="<?php echo base_url();?>theme_assets/images/emcee.jpg"
								style="height: 350px; width: 300px"
								class="img-responsive" alt="">
							<div class="mj_feature_product_overlay">
								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search">view all</a>
								</div>

							</div>
						</div>

					</div>
				</div>
				<div class="col-md-3">
					<div class="mj_feature_product">
						<div class="mj_feature_product_img">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important">Photo
									graphers</h5>
							</div>
							<img
								src="<?php echo base_url();?>theme_assets/images/photgraper.jpg"
								style="height: 350px; width: 300px"
								class="img-responsive" alt="">
							<div class="mj_feature_product_overlay">
								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search">view all</a>
								</div>

							</div>
						</div>

					</div>
				</div>
				<div class="col-md-3">
					<div class="mj_feature_product">
						<div class="mj_feature_product_img">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important">Event
									promoters</h5>
							</div>
							<img
								src="<?php echo base_url();?>theme_assets/images/event_promo.jpg"
								style="height: 350px; width: 300px"
								class="img-responsive" alt="">
							<div class="mj_feature_product_overlay">
								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search">view all</a>
								</div>

							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>
  

</body>

