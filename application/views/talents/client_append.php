

<?php
$talents_id = $this->session->userdata ( 'id' );

foreach ( $clients as $row ) {
	?>
<tr>
	<td><?php echo $row['client_name'];?></td>
	<td><img
		src="<?php echo base_url();?>talents_images/talents<?php echo $talents_id;?>/clients/<?php echo $row['logo']; ?>"
		width="150" height="150"></td>
	<td>
		<button type="button" class="btn btn-info edit_client"
			id="<?php echo $row['id'];?>">
			<span class="glyphicon glyphicon-edit"></span> Edit
		</button>
		<button type="button" class="btn btn-danger client_delete"
			id="<?php echo $row['id'];?>">
			<span class="glyphicon glyphicon-trash"></span> Delete
		</button>
	</td>
<tr>
<?php }?>

