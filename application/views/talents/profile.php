<link rel="stylesheet"
	href="<?php echo base_url();?>theme_assets/css/scroll.css" />
<script src='<?php echo base_url();?>theme_assets/js/jquery.scrollto.js'></script>
<style>
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

.upload {
	background-color: #ff0000;
	border: 1px solid #ff0000;
	color: #fff;
	border-radius: 5px;
	padding: 10px;
	text-shadow: 1px 1px 0px green;
	box-shadow: 2px 2px 15px rgba(0, 0, 0, .75);
}

.upload:hover {
	cursor: pointer;
	background: #c20b0b;
	border: 1px solid #c20b0b;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, .75);
}

#file {
	color: green;
	padding: 5px;
	border: 1px dashed #123456;
	background-color: #f9ffe5;
}

#upload {
	margin-left: 45px;
}

#noerror {
	color: green;
	text-align: left;
}

#error {
	color: red;
	text-align: left;
}

.abcd {
	text-align: center;
}

.abcd img {
	height: 100px;
	width: 100px;
	padding: 5px;
	border: 1px solid rgb(232, 222, 189);
}

#formget {
	float: right;
}

.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive,
	.thumbnail a>img, .thumbnail>img {
	display: block;
	height: 250px !important;
}

.mj_gallary_img {
	float: left;
	width: auto;
	position: relative;
	margin: 10px;
}
</style>
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	width: 100%;
	min-height: 200px;
}

.form-inline .form-group {
	margin-right: 10px;
}

.well-primary {
	color: rgb(255, 255, 255);
	background-color: rgb(66, 139, 202);
	border-color: rgb(53, 126, 189);
}

.glyphicon {
	margin-right: 5px;
}

.card .header {
	box-shadow: 0 10px 30px -12px rgba(0, 0, 0, 0.42), 0 4px 25px 0px
		rgba(0, 0, 0, 0.12), 0 8px 10px -5px rgba(0, 0, 0, 0.2);
	margin: -20px 15px 0;
	border-radius: 6px;
	padding: 15px;
	background-color: #fea002;
}

.headerDivider {
	border-left: 1px solid #38546d;
	border-right: 1px solid #16222c;
	height: 80px;
	position: absolute;
	right: 249px;
	top: 10px;
}
</style>
<style>
.parallax {
	/* The image used */
	background-image: url("../theme_assets/images/bg.jpg");
	/* Set a specific height */
	min-height: 500px;
	/* Create the parallax scrolling effect */
	background-attachment: fixed;
	background-position: center;
	background-repeat: no-repeat;
	background-size: cover;
}

.middle {
	transition: .5s ease;
	opacity: 0;
	position: absolute;
	top: 50%;
	left: 50%;
	transform: translate(-50%, -50%);
	-ms-transform: translate(-50%, -50%)
}

.text {
	background-color: #4CAF50;
	color: white;
	font-size: 12px;
	padding: 14px 10px;
	border-radius: 10px;
}

.containers:hover .image {
	opacity: 0.3;
}

.containers:hover .middle {
	opacity: 1;
}

.row.vdivide [class*='col-']:not (:last-child ):after {
	background: #e0e0e0;
	width: 1px;
	content: "";
	display: block;
	position: absolute;
	top: 0;
	bottom: 0;
	right: 0;
	min-height: 70px;
}
</style>
<style type="text/css">
@media ( max-width :991px) {
	.mj_pagetitle2 .mj_pagetitle_inner {
		position: absolute;
		bottom: -20%;
		left: 0;
		width: 60%;
	}
	.mj_feature_product_img img {
		width: 100% !important;
	}
}

@media ( max-width : 500px) {
	.h_title {
		display: none;
	}
}
</style>
<script type="text/javascript">

function add_profilepic()
{
	
	  $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
      $('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

   
        url = "<?php echo site_url('talents/ajax_add')?>";
   
       
    // ajax adding data to database
<?php $picname="talents".$talents_info->talents_id."pic";?>
    var formData = new FormData($('#form')[0]);
    
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        { 


           
            if(data.status) //if success close modal and reload ajax table
            {location.reload(true);
                alert(data.msg);
                $('#modal_form').modal('hide');
                
               
            }
            else{
            	 alert(data.msg);
                }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //  alert(textStatus+""+errorThrown+""+jqXHR);
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


/*    General info */
 $(document).ready(function(){
	 
	 $("#error_edit").hide();
	 for (instance in CKEDITOR.instances) {
	        CKEDITOR.instances[instance].updateElement();
	    }
		 
    $("#general_info").click(function(){
        
        var t_id=$("#t_id").val();
        var name=$("#name").val();
        var contact_number=$("#contact_number").val();
       
        var act_type=$("#act_type").val();
        var email=$("#email").val();
        
        var dob=$("#dob").val();
        var address=$("#address").val();
        var gender=$("input[name='gender']:checked").val();
		var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

        if(name == '' || contact_number == '' || email =='' ||  dob == '' || gender =="undefined"){

        	if(name==""){
				$(".vali_name").addClass("has-error");
				$('.sucess_content').addClass('alert alert-danger').html("Please Enter name");
	   	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	  		            $("#sucess_div").slideUp(500);
	  		             });
			}
        	if(contact_number==""){
				$(".vali_num").addClass("has-error");
				$('.sucess_content').addClass('alert alert-danger').html("Please Enter contact number");
	   	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	  		            $("#sucess_div").slideUp(500);
	  		             });
			}
        	
        	if(email==""){
				$(".vali_email").addClass("has-error");
				$('.sucess_content').addClass('alert alert-danger').html("Please Enter Valid Email Address");
   	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
  		            $("#sucess_div").slideUp(500);
  		             });
			}
        	
        	if(dob==""){
				$(".vali_dob").addClass("has-error");
			}
        	if(gender =="undefined"){
            	alert("dsasaf");
				$(".vali_gender").addClass("has-error");
			}

        	
        }else if(!emailFilter.test(email)){
        	
		$('.email_error').html("Please enter valid email address");
		$('.sucess_content').addClass('alert alert-danger').html("Please Enter Valid Email Address");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#sucess_div").slideUp(500);
	             });
        	
            }else{

            	var dataString='name='+name+'&contact_number='+contact_number+'&act_type='+act_type+'&email='+email+'&dob='+dob+'&gender='+gender+'&t_id='+t_id+'&address='+address;
    			
           	$.ajax({
            	type: "POST",
            	url:"<?php echo base_url(); ?>talents/generalinfo_insert",
            	
            	data:dataString,
            	success: function(data){
                	
            		if(data=="success"){
            			 $('.sucess_content').addClass('alert alert-success').html("Informations successfully saved !!");
            	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
           		            $("#sucess_div").slideUp(500);
           		             }); 
            			
            		}else {
            			
            			 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
           	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
          		            $("#sucess_div").slideUp(500);
          		             });
            		}

            	},
                error: function(jqXHR, textStatus) {
                    alert( "Request failed: " + jqXHR );
                }
            	});

                

        } 


    });


    /*Bank details insert or update function*/
 $("#bank_submit").click(function(){
        
        var t1_id=$("#t1_id").val();
        var bank_name=$("#bank_name").val();
        var ac_type=$("#ac_type").val();
        var ac_number=$("#ac_number").val();
        var namein_bank=$("#namein_bank").val();
        alert(ac_type);

        if(bank_name == '' || ac_type == '' || ac_number == '' || namein_bank =='' ){

        	if(bank_name==""){
				$(".vali_bname").addClass("has-error");
			}
        	if(ac_type==""){
				$(".vali_actype").addClass("has-error");
			}
        	if(ac_number==""){
				$(".vali_acnumber").addClass("has-error");
			}
        	if(namein_bank==""){
				$(".vali_namein").addClass("has-error");
			}

        }else{

            	var dataString='bank_name='+bank_name+'&ac_type='+ac_type+'&ac_number='+ac_number+'&namein_bank='+namein_bank+'&t1_id='+t1_id;
    			
            	$.ajax({
            	type: "POST",
            	url:"<?php echo base_url(); ?>talents/bankinfo_insert",
            	
            	data:dataString,
            	success: function(data){
            	$('#menu1').load('dashboard.php');
            		if(data=="success"){
            			 $('.sucess_content').addClass('alert alert-success').html("Bank details successfully saved !!");
           	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
          		            $("#sucess_div").slideUp(500);
          		             }); 
            			
            			
            		}else {
            			 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
              	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
             		            $("#sucess_div").slideUp(500);
             		             });
            		}

            	},
                error: function(jqXHR, textStatus) {
                    alert( "Request failed: " + jqXHR );
                }
            	});

                

        } 


    });

    /*Performance info*/

 
 $("#performance_info").click(function(){
        
        var t_id=$("#t_id").val();
        var requirement=$("#requirement").val();
        
    

        if(requirement == '' ){

        	if(requirement==""){
				$(".vali_requirement").addClass("has-error");
				$('.sucess_content').addClass('alert alert-danger').html("Please Enter requirements");
	   	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	  		            $("#sucess_div").slideUp(500);
	  		             });
			}
        	

        }else{

            	var dataString='requirement='+requirement+'&t_id='+t_id;
    			
            	$.ajax({
            	type: "POST",
            	url:"<?php echo base_url(); ?>talents/performanceinfo_insert",
            	
            	data:dataString,
            	 
            	success: function(data){
            		
            		if(data==1){
            			 $('.sucess_content').addClass('alert alert-success').html("Performance Information successfully saved !!");
              	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
             		            $("#sucess_div").slideUp(500);
             		             }); 
            			
            		}else {
            			 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
              	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
             		            $("#sucess_div").slideUp(500);
             		             });
            			
            		}

            	},
                error: function(jqXHR, textStatus) {
                    alert( "Request failed: " + jqXHR );
                }
            	});

                

        } 


    });
    
 
	
});





</script>

<!-- Start From porfolio page  -->
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}

.title {
	font-family: Brandon Grostesque;
	font-size: 50px;
}

.content {
	font-family: Georgia;
	font-size: 18px;
}
</style>
<style type="text/css">
@media ( max-width :991px) {
	.mj_pagetitle2 .mj_pagetitle_inner {
		position: absolute;
		bottom: -20%;
		left: 0;
		width: 60%;
	}
	.mj_feature_product_img img {
		width: 100% !important;
	}
	iframe {
		width: 100% !important;
	}
}
</style>
<script type="text/javascript">
$(document).ready(function () {
	
	

	var countrydropdown = document.getElementById("category").value;

	 if(countrydropdown!="")
	 { 
		 loadData('act_type',countrydropdown,<?php echo $talents_info->act_type;?>);
		 

		 }
	});

</script>
<script type="text/javascript">



function selectact(category_id){
	
  	if(category_id!=""){
		loadData('act_type',category_id,<?php echo  $talents_info->act_type;?>);
			
	}else{
		$("#act_type").html("<option value=''>Select act first</option>");
			
	}
}




function loadData(loadType,loadId,$id){
	    
	var dataString = 'loadType='+ loadType +'&loadId='+ loadId+'&id='+$id;
	
		$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>talents/loaddata",
		data: dataString,
	
		success: function(result){
			
	      	$("#"+loadType).html("<option value=''>Select "+loadType+" first </option>");  
			$("#"+loadType).append(result);  
		}
	});
}

</script>

<script type="text/javascript">
function add_profilepic()
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

   
        url = "<?php echo site_url('talents/ajax_add')?>";
       
        $('#general_info').click();

    // ajax adding data to database
<?php $picname="talents".$talents_info->talents_id."pic";?>
    var formData = new FormData($('#form')[0]);
  
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        { 


           
            if(data.status) //if success close modal and reload ajax table
            {location.reload(true);
                alert(data.msg);
                $('#modal_form').modal('hide');
                
               
            }
            else{
            	 alert(data.msg);
                }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //  alert(textStatus+""+errorThrown+""+jqXHR);
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}

$(document).ready(function(){
	$("#other_div").hide();

	$("#act_type").change(function(){

		var val=$("#act_type").val();

		 if(val == "others"){
             $("#other_div").show();
		 }else{
			 $("#other_div").hide();
		 }
	   
	});
	/* Signup process */
    $('#check4').click(function() {
        
      if ($(this).is(':checked')) {
          $('#save_price').removeAttr('disabled');
      } else {
         
           $('#save_price').attr('disabled', 'disabled');
      }
  });


    $("#package_price").keyup(function(){

       
        var price      = $(this).val();
        
        var percentage =15;
        var calcPerc = (15 / 100 ) * price;
        var total =price - calcPerc;
        $('#total_price').val(total);

    });
	
	
/*Portfolio page  function*/


	$("form#portfolio").submit(function(event){
     
		  //disable the default form submission
		  event.preventDefault();
		  for (instance in CKEDITOR.instances) {
			    CKEDITOR.instances[instance].updateElement();
			}
		  //grab all form data  
		  var formData = new FormData($(this)[0]);
		 
		 $.ajax({
		    url: '<?php echo base_url();?>talents/portfolio_update',
		    type: 'POST',
		    data: formData,
		    async: false,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function (data) {
              
           		if(data==1){
           		 $('.sucess_content').addClass('alert alert-success').html("sucessfully Updated");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           			
           			
           		}else {
           		 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           			
           			
           		}

           	},
               error: function(jqXHR, textStatus) {
                   alert( "Request failed: " + jqXHR );
               }
           	});

               

       


   });


/*You tube video insertion*/

$("#youtube_add").click(function(){
	
       var t_id=$("#t_id").val();
       var youtube_link=$("#youtube_link").val();
       
     

       if(youtube_link == '' ){
    	   $('.sucess_content').addClass('alert alert-danger').html("Enter youtube link!!");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             }); 
       	if(youtube_link==""){
       	 $('.sucess_content').addClass('alert alert-danger').html("Enter youtube link!!");
		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#sucess_div").slideUp(500);
	             }); 
			}
       

       }else{

           	var dataString='youtube_link='+youtube_link+'&t_id='+t_id;
   			
           	$.ajax({
           	type: "POST",
           	url:"<?php echo base_url(); ?>talents/youtube_insert",
           	data:dataString,
           	success: function(data){
           		$("#youtube_link").val("");
           		$("#video_exists").hide();
           		$('#video_section').show();
           		$('#video_section').html(data);
           		

           	},
               error: function(jqXHR, textStatus) {
                   alert( "Request failed: " + jqXHR );
               }
           	});

               

       } 


   });

/*You tube link delete */

	$('#portfolio_section').on("click", ".link_delete", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		var id=$(this).attr('id');
      
		var dataString='id='+id;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/youtube_delete",
			data:dataString  ,
			success: function(data){
		   $("#"+id).hide();
            if(data == 1){
            	$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
				  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });
				
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

/*Photo delete delete */
$('.photo_delete').click( function(){
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
        var arr=id.split('_div');
        var org_photo=arr[0];
       
		var dataString='id='+org_photo;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/photo_delete",
			data:dataString  ,
			success: function(data){
		  $("#"+arr[1]+"_div").hide();
            if(data == 1){
				
				$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
				  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

//Clients save 
$("form#client_form").submit(function(event){
 
  //disable the default form submission
  event.preventDefault();

  //grab all form data  
  var formData = new FormData($(this)[0]);
 
  $.ajax({
    url: '<?php echo base_url();?>talents/client_insert',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
    	$("#client_name").val("");
    	$("#client_photo").val("");
    	$("#cleint_exists").hide();
   		$("#cleint_new").html(data);
   		
   		
    }
    ,
	error: function(jqXHR, textStatus) {
		alert( "Request failed: " + jqXHR );
	}
  });
 
  return false;
});


//Client update
//Clients save 
$("form#edit_client_form").submit(function(event){
 
  //disable the default form submission
  event.preventDefault();

  //grab all form data  
  var formData = new FormData($(this)[0]);
 
  $.ajax({
    url: '<?php echo base_url();?>talents/client_update',
    type: 'POST',
    data: formData,
    async: false,
    cache: false,
    contentType: false,
    processData: false,
    success: function (data) {
    	
    	$("#cleint_exists").hide();
   		$("#cleint_new").hide();
   		$('#cleint_update').html(data);
   		
   		$('.sucess_content').addClass('alert alert-success').html("Updated successfully");
		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#sucess_div").slideUp(500);
	             });
   		
    }
    ,
	error: function(jqXHR, textStatus) {
		alert( "Request failed: " + jqXHR );
	}

  // $("#client_edit").modal();
  });
 
  return false;
});


//Client delete
$('#menu3').on("click", ".client_delete", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
		 var tid=$("#t_id").val();
       
		var dataString='id='+id+'&tid='+tid;
		$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/client_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){

            	$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
		 		
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

//Client Edit
$('#menu3').on("click", ".edit_client", function (e) {

	var id = $(this).attr('id');

	var dataString='id='+id;
	$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>talents/client_edit/",
		data:dataString  ,
		timeout: 5000,
		success: function(data){
			$("#client_edit").modal();
			$("#rev").html(data);

		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});

});


/*Show package panel*/


$("#package_panel").hide();
$('#price_section').on("click", "#add_package", function (e) {
	var $this = $(this);

    // Current click count is previous click count +1
    var clickCount = ($this.data("click-count") || 0) + 1;

    // Save current click count
    $this.data("click-count", clickCount);


 
	$("#package_panel").show();
	$("#add_package").prop("disabled", true);
	 $('#title').click();
 
	
});


/*price package */
$('#price_section').on("click", "#save_price", function (e) {

	var title=$("#price_title").val();
	var tid=$("#tid").val();
	var description=$("#price_description").val();
	var cancel_policy = $("input[name='policy']:checked").val();
	var charge_type=$("#charge_type").val();
	var min=$("#min").val();
	var max=$("#max").val();
	var price=$("#package_price").val();
	var talent_receive=$("#total_price").val();
   
	if(title == '' || price =='' || price == 0){

        if(title ==''){
        	$(".vali_title").addClass("has-error");
        }

        if(price =='' || price ==0){
        	$(".vali_price").addClass("has-error");
        	alert("Price can not be empty or zero");
        }
         
	}else{

      var dataString='tid='+tid+'&title='+title+'&description='+description+'&cancel_policy='+cancel_policy+'&charge_type='+charge_type+'&min='+min+'&max='+max+'&price='+price+'&talent_receive='+talent_receive;
		
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>talents/price_insert",
			data:dataString  ,
			
			success: function(data){
				$('.sucess_content').addClass('alert alert-success').html("Successfully Saved");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
		 		location.reload(true);
				
				
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});



	}

	

});

/* Delete price package */
$('#price_section').on("click", ".delete_price", function (e) {

	
	var id = $(this).attr('id');

	var dataString='id='+id;
	$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>talents/delete_price/",
		data:dataString  ,
		timeout: 5000,
		success: function(data){
			$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             }); 
	 		location.reload(true);
			
			

		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});
	

});

/* Update price package */
$('#price_section').on("click", ".update_price", function (e) {

	
	var id = $(this).attr('id');
	var price_id=$("#price_id"+id).val();
	var title=$("#price_title"+id).val();
	var tid=$("#tid").val();
	var description=$("#price_description"+id).val();
	var cancel_policy = $("input[name='policy"+id+"']:checked").val();
	var charge_type=$("#charge_type"+id).val();
	var min=$("#min"+id).val();
	var max=$("#max"+id).val();
	var price=$("#package_price"+id).val();
	var talent_receive=$("#total_price"+id).val();
  
	if(title == '' || price =='' || price == 0){

        if(title ==''){
        	$(".vali_title").addClass("has-error");
        }

        if(price =='' || price ==0){
        	$(".vali_price").addClass("has-error");
        	alert("Price can not be empty or zero");
        }
         
	}else{

      var dataString='price_id='+price_id+'&tid='+tid+'&title='+title+'&description='+description+'&cancel_policy='+cancel_policy+'&charge_type='+charge_type+'&min='+min+'&max='+max+'&price='+price+'&talent_receive='+talent_receive;
		
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>talents/price_update",
			data:dataString  ,
			
			success: function(data){
				 $('.sucess_content').addClass('alert alert-success').html("Sucessfully updated");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		location.reload(true);
				
				
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});



	}
	
	

});
//pdf remove from portfolio
$('#portfolio_section').on("click", ".docs_remove", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
		 
       
		var dataString='id='+id;
		//$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/docs_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){

            	 $('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           	
			
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});

//pdf remove from pricing
$('#menu4').on("click", ".price_remove", function (e) {
	
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
		 
       
		var dataString='id='+id;
		//$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/docs_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){
            	 $('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			window.location.href="<?php echo base_url();?>talents/talents_portfolio/pricing";},3000);
            	
				
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});



});


</script>


<!-- END -->
<div id="sidebar">
	<div id="nav-anchor"></div>
	<nav>
		<ul type="circle">
			<li><a href="#about_div">More about you </a></li>
			<li><a href="#portfolio_div">Your portfolio</a></li>
			<li><a href="#rates_div">Your rates</a></li>


		</ul>
	</nav>
</div>
<section id="about_div">
	<div class="mj_bottompadder20"
		style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>More About You!</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="content" style="background-color: white">
		<div class="container">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="">

					<div class="mj_workit_info">
						<form>
							<div class="row">

								<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 mj_topadder20">

									<input type="hidden" name="tid" id="t_id"
										value="<?php echo $id;?>">
									<div class="col-sm-6">
										<div class="form-group vali_name">
											<label for="particular">Artist name *</label> <input
												type="text" name="name" placeholder="Name" id="name"
												value="<?php echo $talents_info->name;?>"
												class="form-control"> <span class="text-danger"></span>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group vali_num">
											<label for="particular">Contact Number *</label> <input
												type="number" name="phone" placeholder="Contact Number"
												pattern="[0-9]*" title="Phone number" id="contact_number"
												value="<?php echo $talents_info->phone_number;?>"
												class="form-control"> <span class="text-danger"></span>
										</div>
									</div>

									<div class="col-sm-6">
										<div class="form-group vali_dob">
											<label for="particular">Birthday</label>
											<div class="mj_datepicker">
												<input type="date" class="form-control" name="dob"
													value="<?php echo $talents_info->dob;?>" id="dob">
											</div>
											<span class="text-danger"></span>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group vali_email">
											<label for="particular">Contact email *</label> <input
												type="email" name="email" placeholder="Enter your mail id"
												id="email" value="<?php echo $talents_info->email;?>"
												class="form-control"> <span class="email_error"
												style="color: red;"></span>
										</div>
									</div>



									<div class="col-sm-6">
										<div class="form-group vali_dob">
											<label for="particular">Gender</label> <br> <input
												type="radio" class="gender " name="gender" id="gender"
												value="male"
												<?php if($talents_info->gender=="male" || $talents_info->gender==""){ echo "checked";}?>>
											Male <input type="radio" class="gender" name="gender"
												value="female"
												<?php if($talents_info->gender=="female" ){ echo "checked";}?>>
											Female <span class="text-danger"></span>
										</div>
									</div>
									<div class="col-sm-6">
										<div class="form-group vali_address">
											<label for="particular">Address</label>

											<textarea class="form-control" name="address" id="address"
												rows="4" cols="4"><?php echo $talents_info->address;?></textarea>

											<span class="text-danger"></span>
										</div>
									</div>





								</div>

								<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center">
									<div
										class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ih-item circle effect17">
										<div class="mj_mainheading">
											<div class="row">
												<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
													<a href="javascript:void(0)" onclick="add_profilepic()">
														<div class="mj_joblogo img">
									<?php
									
									$prinimage = '<img id="myimage"
		src="' . base_url () . '/theme_assets/images/default_user.png"
		class="img-circle" alt="Cinque Terre" width="200" height="200">
    	';
									if ($talents_info->id != '') {
										
										$files = glob ( 'talents_profilepic/*' ); // get all file names
										foreach ( $files as $file ) {
											if (is_file ( $file )) {
												$picname = "talent" . $talents_info->talents_id . "pic";
												
												$photos_from_direc = explode ( '/', $file );
												$photoname = explode ( '.', $photos_from_direc [1] );
												if ($photoname [0] == $picname) {
													$prinimage = '<img id="myimage"
		src=" ' . base_url () . 'talents_profilepic/' . $picname . '"
		class="img-circle" alt="Profile " width="200" height="200">
    	';
												}
												
												// unlink($file);
											} // delete file
										}
									}
									
									echo $prinimage;
									?>
									
									
									</div>
														<div class="info" style="margin-left: 80px">
															<h3>Change Picture</h3>

														</div>
													</a>
												</div>

											</div>
										</div>
									</div>

									<div class="h_title" style="margin-top: 230px;">
										<span>Add a <b> Professional </b>photo of yourself/company!<br>
											<br> A good photo captures the attention of event planners!
										</span>
									</div>
								</div>

							</div>
							<div class="row">
								<div class="col-md-6"></div>
								<div class="col-md-5">
									<div class="form-group pull-right" style="margin-bottom: 1px">
										<br> <input type="button" name="update" value="Save"
											id="general_info" data-position="top-right"
											class="btn btn-success">
									</div>



									<center>
										<span class="text-success"></span>
									</center>
									<span class="text-danger"></span>

								</div>
								<div class="col-md-2"></div>
							</div>

						</form>
					</div>

				</div>
			</div>

		</div>
	</div>

</section>
<section id="portfolio_div">
	<div class="mj_bottompadder20"
		style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span> Your Portfolio</span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="content " style="background-color: white">
		<div class="container" id="portfolio_section">

			<div class="row">
				<div class="mj_workit">



					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="">

							<br>
							<center>*Remember, one skill per portfolio, we don’t want to
								confuse people!</center>

							<div class="mj_workit_info">
								<form id="portfolio" enctype="multipart/form-data">
									<input type="hidden" name="pid"
										value="<?php echo base_url();?>"> <input type="hidden"
										name="t_id" id="t_id" value="<?php echo $id;?>">
									<div class="row">

										<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">


											<div class="col-sm-6">
												<div class="form-group vali_category">
													<label for="particular">Category*</label> <select
														id="category" name="category" class=" form-control"
														onchange="selectact(this.options[this.selectedIndex].value)">
														<option class="form-control" value="">Select Category</option>

																			<?php
																			
																			foreach ( $talents_category as $talents ) {
																				
																				if ($talents ['id'] == $talents_info->category) {
																					?>
																		
																			<option class="form-control"
															value="<?php echo $talents['id'];?>" selected><?php echo $talents['name'];?></option>
																			
																			<?php }else{?>
																			<option class="form-control"
															value="<?php echo $talents['id'];?>"><?php echo $talents['name'];?></option>
																			
																			<?php }?>
																			<?php }?>
																		</select> <span class="text-danger"></span>
												</div>
											</div>
											

											<div class="col-sm-6" id="other_div">
												<div class="form-group vali_other">
													<label for="particular">Other</label> <input type="text"
														class="form-control" id="other" name="other" value=""> <span
														class="text-danger"></span>
												</div>

											</div>
											<div class="col-sm-6">
												<div class="form-group vali_exp">
													<label for="particular">Year of experience*</label> <input
														type="number" class="form-control" id="exp" name="exp"
														value="<?php echo $talents_info->experience;?>"> <span
														class="text-danger"></span>
												</div>
											</div>



											<div class="col-sm-6">
												<div class="form-group vali_pdf">
													<label for="particular"> OR Upload your document</label> <input
														type="file" name="pdf" id="pdf" class="form-control"
														accept=".pdf" /> <span class="text-danger"></span>
												</div>
												<div class="form-group ">
																
																<?php
																
																$images = glob ( 'talents_images/talents' . $id . '/portfolio/*', GLOB_NOSORT );
																foreach ( $images as $image ) {
																	
																	$exp = explode ( '/', $image );
																	$name = $exp [3];
																	?>
<a href="<?=base_url($image)?>" target="_blank"> <?php echo $name;?> &nbsp;view &nbsp;&nbsp;&nbsp;</a>

													<button class="btn btn-danger docs_remove"
														id="<?php echo $image;?>">Remove</button>
													<br> <br>
																<?php }?>
																
																
																</div>
											</div>




										</div>

										<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center">
											<div class="form-group vali_service">
												<label for="particular">Sell your act/service (write up) *</label>
												<textarea class="form-control" id="act_service" rows="5"
													name="act_service" required><?php echo $talents_info->service;?></textarea>
												<span class="text-danger"></span>
											</div>


										</div>

									</div>
									<div class="row">

										<div class="panel-group" id="accordion">
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title">
															<a data-toggle="collapse" data-parent="#accordion"
																href="#collapseOne"><span
																class="glyphicon glyphicon-camera	Try it
"> </span>Add photos</a>
														</h4>
													</div>
													<div id="collapseOne" class="panel-collapse collapse">
														<div class="panel-body">
															<div class="row">
																<div class="col-sm-6">
																	<div class="form-group">
																		<a href="#" class="btn btn-success"
																			data-toggle="modal" data-target="#gallery_photo"><i
																			class="fa fa-camera" aria-hidden="true"></i><span
																			class="h_title">Add Photos </span></a>
																	</div>
																</div>
																<div class="col-sm-6"></div>
															</div>
															<div class="row">
																<div class="col-sm-12">
																	<div id="">
	
													<?php
													$talents_id = $id;
													$images = glob ( "talents_images/talents" . $talents_id . "/photos/*.*" );
													$sn = 0;
													foreach ( $images as $image ) {
														$sn ++;
														?>

                    <div class="col-md-4 mix mix-all holiday"
																			id="<?php echo $sn;?>_div" data-value="1">
																			<div class="row">
																				<div class="mj_gallary_img">
																					<img
																						src="<?php echo base_url();?><?php echo $image;?>"
																						alt="gallery" class="img-responsive"
																						style="height: 150px !important">
																					<div class="mj_overlay">
																						<div class="mj_gallary_info">
																							<h5 class="animated fadeInDown">
																								<a class="fancybox"
																									data-fancybox-group="gallery"
																									href="<?php echo base_url();?><?php echo $image;?>"
																									title="">View</a>
																							</h5>
																							<button class="btn btn-danger photo_delete"
																								id="<?php echo $image;?>_div<?php echo $sn;?>"
																								class="delete">Delete</button>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
           
            <?php }?>
            
             </div>
																</div>
															</div>


														</div>
													</div>
												</div>
											</div>
											<div class="col-md-6">
												<div class="panel panel-default">
													<div class="panel-heading">
														<h4 class="panel-title">
															<a data-toggle="collapse" data-parent="#accordion"
																href="#collapseTwo"><span
																class="glyphicon glyphicon-facetime-video h_title"> </span>Add
																/ Edit videos</a>
														</h4>
													</div>
													<div id="collapseTwo" class="panel-collapse collapse">
														<div class="panel-body">
															<div class="row">
																<div class="row"
																	style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">



																	<div class="col-sm-8">
																		<div class="form-group vali_youtube">
																			<label>Video <span></span>
																			</label> <input type="text" class="form-control"
																				placeholder="You tube link" id="youtube_link">
																		</div>

																	</div>
																	<div class="col-sm-3">
																		<div class="form-group">
																			<label>&nbsp; </label> <input type="button"
																				class="btn btn-success form-control"
																				data-position="top-right" id="youtube_add"
																				value="Add">
																		</div>
																	</div>
																</div>
																<hr style="border-top: 1px solid #111111;">

																<div id="video_section"></div>
																<div id="video_exists">
                              <?php foreach($videos as $row){?>
                              
                              <div class="row"
																		id="<?php  echo $row['id'];?>">

																		<div class="col-sm-6">

																			<div class="form-group"><?php echo $row['link'];?>
                             </div>


																		</div>

																		<div class="col-sm-6">
																			<div class="form-group">
																				<label>&nbsp; </label>
																				<button class="btn btn-danger link_delete "
																					id="<?php  echo $row['id'];?>"
																					style="margin-top: 50px; margin-left: 60px">Delete</button>
																			</div>
																		</div>


																	</div>
																	<hr style="border-top: 1px solid #111111;">
<?php }?>
        </div>

															</div>

														</div>
													</div>
												</div>
											</div>
										</div>

									</div>
									<div class="row">
										<div class="col-md-6"></div>
										<div class="col-md-5">
											<div class="form-group pull-right" style="margin-bottom: 1px">
												<br> <input type="submit" name="update" id="" value="Save"
													class="btn btn-success form-control">

											</div>



											<center>
												<span class="text-success"></span>
											</center>
											<span class="text-danger"></span>

										</div>
										<div class="col-md-2"></div>
									</div>

								</form>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="rates_div">
	<div class="mj_bottompadder20"
		style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3>
							<span class="title"> Your Rates ! </span>
						</h3>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class=" content" id="price_section"
		style="background-color: white">
		<div class="container">

			<div class="row">
				<div class="mj_workit">



					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="">

							<div class="mj_workit_info">
								<div class="row ">
									<div class="row">

										<div class="col-sm-6">
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<button class="btn btn-info" id="add_package">+ Add New</button>
										</div>
										<div class="col-sm-6"></div>
									</div>
										
											
										
											
											<?php foreach($price as$row){?>
											
														<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
										<div class="panel-group" id="">
											<div class="panel panel-info">
												<div class="panel-heading">
													<h4 class="panel-title" style="color: black">
														<a data-toggle="collapse"
															href="#collapse<?php echo $row['id'];?>" id=""><?php echo $row['title'];?></a>
													</h4>
												</div>
												<div id="collapse<?php echo $row['id'];?>"
													class="panel-collapse collapse">

													<div class="panel-body">

														<input type="hidden" name="tid" id="tid"
															value="<?php echo $id;?>"> <input type="hidden"
															name="price_id" id="price_id<?php echo $row['id'];?>"
															value="<?php echo $row['id'];?>">
														<div class="col-sm-4">
															<div class="form-group ">
																<label>Title * <span></span>
																</label> <input type="text" class="form-control"
																	placeholder="Enter title here" name="title"
																	id="price_title<?php echo $row['id'];?>"
																	value="<?php echo $row['title'];?>">
															</div>

														</div>
														<div class="col-sm-4">
															<div class="form-group ">
																<label>Charge Type<span id="type"> </span>
																</label><select class=" form-control"
																	id="charge_type<?php echo $row['id'];?>">



																	<option value="per hour"
																		<?php if($row['charge_type']=="per hour") echo "selected";?>>Per
																		hour</option>
																	<option value="Per pax"
																		<?php if($row['charge_type']=="Per package") echo "selected";?>>Per
																		package</option>
																	<option value="Per project"
																		<?php if($row['charge_type']=="Per set") echo "selected";?>>Per
																		set</option>


																</select>
															</div>

														</div>
														<div class="col-sm-4">
															<div class="form-group ">
																<label>Price<span> (SGD)</span>
																</label> <input type="number" class="form-control"
																	placeholder="" name="package_price"
																	id="package_price<?php echo $row['id'];?>"
																	value="<?php echo $row['price'];?>">
															</div>

														</div>
														<div class="col-sm-12">
															<div class="form-group ">
																<label>Description<span></span>
																</label>
																<textarea class="form-control" rows="5"
																	id="price_description<?php echo $row['id'];?>"><?php echo $row['description'];?></textarea>
															</div>

														</div>

														<br> <br> <br>
														<div class="row">

															<div class="col-sm-6">

																<div class="form-group ">
																	<button class="btn btn-danger delete_price"
																		id="<?php echo $row['id'];?>">Delete Package</button>

																</div>
															</div>

															<div class="col-sm-6">
																<div class="form-group ">
																	<button class="btn btn-success update_price pull-right"
																		id="<?php echo $row['id'];?>">Update Package</button>
																</div>
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>

									</div>
											
											
											
											
											
								<?php }?>			
											





								<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
										<div class="panel-group" id="package_panel">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" href="#collapse0" id="title">Title
															Here</a>
													</h4>
												</div>
												<div id="collapse0" class="panel-collapse collapse">
													<div class="panel-body">

														<input type="hidden" name="tid" id="tid"
															value="<?php echo $id;?>">
														<div class="col-sm-4">
															<div class="form-group ">
																<label>Title * <span></span>
																</label> <input type="text" class="form-control"
																	placeholder="Enter title here" name="title"
																	id="price_title">
															</div>

														</div>
														<div class="col-sm-4">
															<div class="form-group ">
																<label>Charge Type<span id="type"> </span>
																</label><select class=" form-control" id="charge_type">



																	<option value="per hour" selected>Per hourly</option>
																	<option value="Per pax">Per pax</option>
																	<option value="Per project">Per project</option>


																</select>
															</div>

														</div>
														<div class="col-sm-4">
															<div class="form-group ">
																<label>Price<span> (SGD)</span>
																</label> <input type="number" class="form-control"
																	placeholder="" name="package_price" id="package_price">
															</div>

														</div>
														<div class="col-sm-12">
															<div class="form-group ">
																<label>Description<span></span>
																</label>
																<textarea class="form-control" rows="3"
																	id="price_description"></textarea>
															</div>

														</div>


														<br>
														<div class="row"></div>


														<br> <br>
														<div class="row">

															<div class="col-sm-6">

																<div class="form-group ">
																	<label for="check4"> <input type="checkbox" id="check4"
																		name="checkbox"> <span> I have read, understand and
																			agree to the Talent Portal Terms of Service,
																			including the <a href="#">User Agreement</a> and <a
																			href="#">Privacy Policy</a>.
																	</span></label>
																</div>
															</div>

															<div class="col-sm-6">
																<button style="float: right" class="btn btn-success"
																	id="save_price" disabled>Save Package</button>
															</div>

														</div>
													</div>
												</div>
											</div>
										</div>
									</div>


								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!--      <div class=" mj_bottompadder20 parallax" > -->
<!--        <div class="container"> -->
<!--           <div class="row"> -->
<!--                 <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0"> -->
<!--                     <div class="mj_mainheading  mj_bottompadder50"> -->
<!--                       <div class="mj_workit_innerheader"> -->
<!--                                 <h4> Featured Talents</h4> -->
<!--                             </div> -->
<!--                     </div> -->
<!--                 </div> -->
<!--             </div> -->
<!--             <div class="row"> -->
<!--                 <div class="mj_workit"> -->



<!--                     <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "> -->


<!--                                 <div class="row"> -->
<!--                                     <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center "> -->

<!--                                  		<div -->
<!-- 					class="col-lg-3 col-md-3 col-sm-3 col-xs-12 ih-item circle effect17"> -->
<!-- 					<div class="mj_mainheading"> -->
<!-- 						<div class="row"> -->
<!-- 							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"> -->
<!-- 								<a href="#" > -->

<!-- 									<div class="mj_joblogo img containers"> -->
<!-- 							<img src="http://placehold.it/170X213" class="img-circle"  width="200" height="190" alt=""> -->
<!-- 									<div class="middle"> -->
<!--     <div class="text">John Doesasasasasasasa</div> -->
<!--   </div> -->
<!-- 										</div> -->


<!-- 									</div> -->
<div class="info" style="margin-left: 80px">
	<!-- 											<h3>Change Picture</h3> -->

	<!-- 									</div> -->
	<!-- 								</a> -->
	<!-- 							</div> -->

	<!-- 						</div> -->
	<!-- 					</div> -->
	<!-- 				</div> </div> -->


	<!--                                 </div> -->



	<!--                     </div> -->
	<!--                 </div> -->
	<!--             </div> -->
	<!--         </div> -->


	<div id="sucess_div"
		style="position: fixed; z-index: 10004; top: 30px; right: 0px;">
		<h4 style="white-space: nowrap;" class="sucess_content"></h4>

	</div>





	<div class="modal fade" id="modal_form" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h3 class="modal-title">Person Form</h3>
				</div>
				<div class="modal-body form">
					<form action="#" id="form" class="form-horizontal">
						<input type="hidden" value="<?php echo $id;?>" name="tid" />
						<div class="form-body">
							<div id="photo_upload">
								<div class="mbot error-message-highlight hidden"
									id="progress_report_status"
									style="font-size: 12px; text-transform: none;"></div>
								<label for="profile_photo_upload">Upload Picture</label>
								<div class="">

									<!-- Conditional comments are dropped from IE post IE 10 -->
									<input id="profile_photo_upload" name="photo" type="file"
										class="file-up">

								</div>

								<div
									style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
									JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
									in height and width
								</div>
								<div class="upload_status"></div>

								<div id="progress_report" style="">
									<div id="progress_report_name"></div>

									<div id="progress_report_bar_container"
										style="height: 5px; margin-top: 10px">
										<div id="progress_report_bar"
											style="background-color: #cb202d; width: 0; height: 100%;"></div>
									</div>
								</div>
							</div>

						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnSave" onclick="save()"
						class="btn btn-primary">Upload</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- End Bootstrap modal -->
	<div class="modal fade" id="modal_form" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h3 class="modal-title">Person Form</h3>
				</div>
				<div class="modal-body form">
					<form action="#" id="form" class="form-horizontal">
						<input type="hidden" value="<?php echo $id;?>" name="tid" />
						<div class="form-body">
							<div id="photo_upload">
								<div class="mbot error-message-highlight hidden"
									id="progress_report_status"
									style="font-size: 12px; text-transform: none;"></div>
								<label for="profile_photo_upload">Upload Picture</label>
								<div class="">

									<!-- Conditional comments are dropped from IE post IE 10 -->
									<input id="profile_photo_upload" name="photo" type="file"
										class="file-up">

								</div>

								<div
									style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
									JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
									in height and width
								</div>
								<div class="upload_status"></div>

								<div id="progress_report" style="">
									<div id="progress_report_name"></div>

									<div id="progress_report_bar_container"
										style="height: 5px; margin-top: 10px">
										<div id="progress_report_bar"
											style="background-color: #cb202d; width: 0; height: 100%;"></div>
									</div>
								</div>
							</div>

						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" id="btnSave" onclick="save()"
						class="btn btn-primary">Upload</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- End Bootstrap modal -->

	<div class="modal fade" id="gallery_photo" tabindex="-1" role="dialog"
		aria-labelledby="gallery_photo">
		<div class="modal-dialog" role="document">
			<!-- Modal Content Starts -->
			<div class="modal-content">
				<!-- Modal Header Starts -->

				<!-- Modal Header Ends -->
				<!-- Modal Body Starts -->
				<div class="modal-body">
					<button type="button" class="close" data-dismiss="modal"
						aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<div id="gallery_form">
						Photo Upload

						<form method="POST"
							action="<?php echo base_url();?>talents/photo_upload"
							enctype="multipart/form-data">
							<input type="hidden" value="<?php echo $id;?>" name="tid" />

							<div id="filediv">
								<input name="photo[]" id="file" type="file" />
							</div>
							<br /> <input type="button" id="add_more"
								class="upload btn btn-info" value="Add More Files" /> <input
								type="submit" value="Upload File" id="upload"
								class="upload btn btn-success" />
						</form>

					</div>
				</div>
				<!-- Modal Body Ends -->
			</div>
			<!-- Modal Content Ends -->
		</div>
	</div>


	<div class="modal fade" id="client_edit" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

					</button>
					<h4>Edit Client</h4>
				</div>


				<form id="edit_client_form" enctype="multipart/form-data">
					<div class="reviews-form-box" id="rev"></div>
				</form>



			</div>
		</div>
	</div>


	<div class="modal fade" id="price_file" tabindex="-1" role="dialog"
		aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
		<div class="modal-dialog">
			<div class="modal-content"></div>
		</div>
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">

					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

					</button>
					<h4>Upload file</h4>
				</div>


				<form id="price_upload" method="POST"
					action="<?php echo base_url();?>talents/price_docsupload"
					enctype="multipart/form-data">
					<br> <br>
					<div class="row">
						<div class="col-sm-2"></div>
						<div class="col-sm-6">
							<div class="form-group ">
								<input type="hidden" value="<?php echo $id;?>" name="tid" /> <input
									type="file" name="price_pdf" class="form-control" accept=".pdf">

							</div>
						</div>
						<div class="col-sm-4">

							<div class="form-group ">
								<input type="submit" class="btn btn-success" name="submit"
									value="Save">
							</div>
						</div>
					</div>






				</form>



			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
var abc = 0; //Declaring and defining global increement variable

$(document).ready(function() {

//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
    $('#add_more').click(function() {
        $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                $("<input/>", {name: 'photo[]', type: 'file', id: 'file'}),        
                $("<br/><br/>")
                ));
    });

//following function will executes on change event of file input to select different file	
$('body').on('change', '#file', function(){
            if (this.files && this.files[0]) {
                 abc += 1; //increementing global variable by 1
				
				var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/ width='150' height='150'></div>");
               
			    var reader = new FileReader();
                reader.onload = imageIsLoaded;
                
                reader.readAsDataURL(this.files[0]);
               
			    $(this).hide();
                $("#abcd"+ abc).append($("<img/>", {id: 'img', src: '<?php echo base_url();?>theme_assets/images/x.png', alt: 'delete',width:'30',height:'40'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

//To preview image     
    function imageIsLoaded(e) {
       
       
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
      
        var name = $(":photo").val();
        alert(name);
        if (!name)
        {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });
});


</script>
