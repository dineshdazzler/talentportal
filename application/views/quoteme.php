<style>
.stepwizard-row {
	display: table-row;
}

.stepwizard {
	display: table;
	width: 100%;
	position: relative;
}

.stepwizard-step button[disabled] {
	opacity: 1 !important;
	filter: alpha(opacity = 100) !important;
}

.stepwizard-row:before {
	top: 14px;
	bottom: 0;
	position: absolute;
	content: " ";
	width: 100%;
	height: 1px;
	background-color: #ccc;
	z-order: 0;
}

.stepwizard-step {
	display: table-cell;
	text-align: center;
	position: relative;
}

.btn-circle {
	width: 30px;
	height: 30px;
	text-align: center;
	padding: 6px 0;
	font-size: 12px;
	line-height: 1.428571429;
	border-radius: 15px;
}

.mj_mainheading h2:after {
	border-bottom: 2px solid #f26522;
	width: 200px;
	top: 90%;
	left: 0%;
}

.hover06 figure img {
	-webkit-transform: rotate(15deg) scale(1.4);
	transform: rotate(15deg) scale(1.4);
	-webkit-transition: .3s ease-in-out;
	transition: .3s ease-in-out;
}

.hover06 figure:hover img {
	-webkit-transform: rotate(0) scale(1);
	transform: rotate(0) scale(1);
}

#gallery_list li {
	display: none;
}

#video_list li {
	display: none;
}

.mj_pagetitle2 .mj_pagetitleimg img {
	width: 100%;
	max-height: 140px !important;
	min-height: 270px !important;
}

.mj_social_media_section .mj_mainbtn {
	padding: 15px 45px;
	width: 200px;
}

.mj_joblogo img {
	width: 240px !important;
	height: 260px !important;
}

.mj_social_media_section ul li {
	float: left;
	list-style: none;
	padding-right: 20px;
	text-align: center;
	margin-bottom: 15px;
}

.not-active {
	pointer-events: none;
	cursor: default;
}
</style>


<script type="text/javascript">

$(document).ready(function () {

    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn');

    

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
    });

    $('div.setup-panel div a.btn-primary').trigger('click');
});
$(document).ready(function () {
	
    size_li_1 = $("#video_list li").size();
    
    y=0;
    var size_1=size_li_1 - y;
   
    if(size_1 > 0){
    	size_1=size_1;
    }else{
    	size_1 =0;
    }
    $('#video_list li:lt('+y+')').show();
   $('#loadMore_video').val('Load more');
   
    $('#loadMore_video').click(function () {
        y= (y+5 <= size_li_1) ? y+5 : size_li_1;
        $('#video_list li:lt('+y+')').show();
        var size2=size_li_1-y;
        $('#loadMore_video').val('Load more');
       
    });

  
});
</script>
<script type="text/javascript">
$(document).ready(function () {
	
    size_li_1 = $("#gallery_list li").size();
    
    y=6;
    var size_1=size_li_1 - y;
   
    if(size_1 > 0){
    	size_1=size_1;
    }else{
    	size_1 =0;
    }
    $('#gallery_list li:lt('+y+')').show();
   $('#loadMore_gallery').val('Load more');
   
    $('#loadMore_gallery').click(function () {
        y= (y+5 <= size_li_1) ? y+5 : size_li_1;
        $('#gallery_list li:lt('+y+')').show();
        var size2=size_li_1-y;
        $('#loadMore_gallery').val('Load more');
       
    });

  
});



//Calculation part in price packages

$(document).ready(function(){

$("#s2").addClass('not-active');
$("#s3").addClass('not-active');
$("#change_event").hide();
$("#change_package").hide();
$("#hire_result").hide()
$(".event").click(function(){

var org_count=$(this).attr('id').split('_');
var event_count=org_count[1];
var event_id=$("#eventid_"+event_count).val();

var event_title=$("#event_title"+event_count).text();
var start=$("#start_"+event_count).text();
var end=$("#end_"+event_count).text();
var venue=$("#venue_"+event_count).text();
$("#s2").removeClass('not-active');
$("#change_event").show();
//Set the values to final result
$("#final_title").text(event_title);
$("#final_venue").text(venue);
$("#final_start").text(start);
$("#final_end").text(end);
$("#final_event").val(event_id);
	
});




	
$(".total_div").hide();

$(".duration").change(function () {

var count=$(this).attr('id');
var duration=$("#"+count).val();
var hr_price=$("#hr_price"+count).val();
var total=duration*hr_price;
$("#total_price"+count).text(total.toFixed(2));

$("#total_div"+count).show();
var talent_name=$("#talent_name").text();
var package_name=$("#package_name").text();
$("#s3").removeClass('not-active');
$("#change_package").show();
//Set pacakage details 
$("#final_talentname").text(talent_name);
//$("#final_package").text(package_name);
//$("#final_duration").text(duration);
//$("#final_totalprice").text(total.toFixed(2));
	
});

//package change set final price as per row 
$(".select_package").click(function(){
	
	var org_count=$(this).attr('id').split('_');
	var price_count=org_count[1];

	var duration=$("#"+price_count).val();
	var hr_price=$("#hr_price"+price_count).val();
	var package_name=$("#package_name"+price_count).text();
    var total_price=$("#total_price"+price_count).text();
    $("#final_package").text(package_name);
    $("#final_duration").text(duration);
    $("#final_hrprice").val(hr_price);
    $("#final_totalprice").text(total_price);
	
	
});

//Insert hired talents details into database 

$("#hire_talent").click(function(){

var event_id=$("#final_event").val();
var duration=$("#final_duration").text();
var total_price=$("#final_totalprice").text();
var talent_id=$("#final_talentid").val();
var client_id=$("#final_clientid").val();
var notes=$("#notes").val();
var package_name=$("#final_package").text();
var hr_price= $("#final_hrprice").val();
if(event_id !='' && total_price !=0){

	   var datastring='event_id='+event_id+'&duration='+duration+'&total_price='+total_price+'&talent_id='+talent_id+'&client_id='+client_id+'&notes='+notes+'&package_name='+package_name+'&hr_price='+hr_price;
alert(datastring);
	   $.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>clients/quote_insert/",
		data:datastring,
		success: function(data){
            if(data==1){
               alert("Talent Quoted successfully");
               $("#hire_div").hide();
               $("#hire_result").show();
                }else if(data ==2){
alert("Already this talent Quote same event ");
                }
		
		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});
	
	
}else{

	alert("Something went wrong please try again");
}

	
});
	
});

</script>

<style>
.mj_social_media_section .mj_mainbtn {
	padding: 15px 10px;
}
</style>
<link href="<?php echo base_url();?>css/style1.css" rel="stylesheet"
	type="text/css" />

	<div class="mj_toppadder80"></div>
<div class="mj_toppadder80" style="background-color: white">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="row">
					<div class="col-lg-1 col-md-1 col-sm-12 col-xs-12"></div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="mj_pageheading ">
							<h1 style="font-size: 51px; text-align: left"><span id="talent_name"><?php echo $talents_detail->name;?> </span><span
									style="float: right"><img
									src="<?php echo base_url();?>theme_assets/images/3-out-of-5-stars.png"
									class="img-responsive" width="180" height="30"></span>
							</h1>

							<h4 style="text-align: left"> I'm <?php echo $talents_detail->category;?>
							</h4>
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

						<div class="mj_joblogo">
							<img
								src="<?php echo base_url();?>talents_profilepic/talent<?php echo $talents_detail->talents_id;?>pic"
								class="img-responsive" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class=" mj_bottompadder80 " style="background-color:white">

<div class=" mj_toppadder20 mj_bottompadder80 " >
<div style="background-color: rgba(0, 108, 109, 1)">
<div class="container" >
	<div class="row" >

		<div
			class="mj_social_media_section mj_candidatepage_media mj_toppadder5 mj_bottompadder5">
			<h2 style="text-align: center; color: white; font-weight: bold">QUOTE
				ME</h2>

		</div>

	</div>
	</div>
	</div>
	<br>
	<div class="container" id="hire_div">
		<div class="row">
			<div class="stepwizard">

				<div class="stepwizard-row setup-panel">

					<div class="stepwizard-step">

						<a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
						<p>
							1.Select Event <span id="change_event"> or Change Event </span>
						</p>
					</div>
					<div class="stepwizard-step">
						<a href="#step-2" type="button" class="btn btn-default btn-circle"
							id="s2">2</a>
						<p>
							2.Details for Quotation <span id="change_package">  </span>
						</p>
					</div>
					
				</div>
			</div>
		</div>
		<br>
		<form style="text-align: left">
			<div class="row setup-content" id="step-1">

				
            <?php
												$event_count = 0;
												
												foreach ( $result as $selected ) {
													
													$event_count ++;
													?>
													<div class="col-xs-12">
             <div class="col-md-1">
						

					</div>
					<div class="col-md-11">
                         <u>  <h3 
							id="event_title<?php echo $event_count;?>">
							<?php echo $event_count;?>.<?php echo $selected->event_title;?>
						</h3></u>
						
					<div class="row" style="font-size:18px;textalign:left">
						<div class="col-md-4">Event Type : Bithday Celebration</div>
						<div class="col-md-4">Location :  <span id="venue_<?php echo $event_count;?>"> <?php echo $selected->venue;?></span></div>
						<div class="col-md-4"><button class="btn btn-danger nextBtn event"
							id="event_<?php echo $event_count;?>" type="button">Pick this Event</button> </div>
					</div><br>
					<div class="row" style="font-size:18px;textalign:left">
						<div class="col-md-4">Start Time :<span id="start_<?php echo $event_count;?>"><?php echo $selected->event_start;?></span></div>
						<div class="col-md-4">End Time : <span id="end_<?php echo $event_count;?>"> <?php echo $selected->event_end;?></span></div>
						<div class="col-md-4"></div>
					</div><br>
					<div class="row" style="font-size:18px;textalign:left">
						<div class="col-md-4">No of Pax : <?php echo $selected->pax;?></div>
						<div class="col-md-4">Total Budget :$ <?php echo $selected->budget;?></div>
						<div class="col-md-4"></div>
					</div><br>
					<div class="row" style="font-size:18px;textalign:left">
						<div class="col-md-4">Description :<?php echo $selected->description;?></div>
						<div class="col-md-4"> </div>
						<div class="col-md-4"></div>
					</div>
						

						<input type="hidden" id="eventid_<?php echo $event_count;?>"
							value="<?php echo $selected->id;?>">

						

					</div>
					</div>
				
<?php }?>
				</div>
			</div>
			<div class="container">
			<div class="row setup-content" id="step-2">
				<div class="col-xs-12">
					<div class="col-md-12">
					<div class="col-md-2"></div>
<div class="col-md-8">
							<h3 style="color: #c33f45; font-weight: bold;text-align:left">Details For Quotation
</h3>
							<div class="form-group ">
								
								<textarea name="notes" placeholder="What services are needed? For how many hours? etc" id="notes"
									class="form-control" rows="10"> </textarea>
							</div>
						</div>

<div class="col-md-2"></div>
						
				</div>


<button class="btn btn-success btn-lg " id="hire_talent"
							type="button">Get a Quote</button>

				</div>

			</div>
			</div>
			<div class="row setup-content" id="step-3">
				<div class="col-xs-12">
					<div class="col-md-12">
						<div class="col-md-4">
							<h3 style="color: #c33f45; font-weight: bold">Event Details</h3>
							<div class="form-group">
								<label>Title</label><br> <span id="final_title"
									style="color: #c33f45;"></span>
							</div>
							<div class="form-group">
								<label>Venue</label><br> <span id="final_venue"
									style="color: #c33f45;"></span>
							</div>
							<div class="form-group">
								<label>Start time</label><br> <span id="final_start"
									style="color: #c33f45;"></span>

							</div>
							<div class="form-group">
								<label>End time</label><br> <span id="final_end"
									style="color: #c33f45;"></span>

							</div>
							<input type="hidden" id="final_event" name="final_event"> <input
								type="hidden" id="final_talentid"
								value="<?php echo $talents_detail->talents_id;?>"> <input
								type="hidden" id="final_clientid" value="<?php echo $id;?>">
						</div>

						<div class="col-md-4">
							<h3 style="color: #c33f45; font-weight: bold">Package Details</h3>
							<div class="form-group">
								<label>Talent Name</label><br> <span id="final_talentname"
									style="color: #c33f45;"></span>

							</div>

							<div class="form-group">
								<label>Package </label><br> <span id="final_package"
									style="color: #c33f45;"></span>

							</div>

							<div class="form-group">
								<label>Duration / Package </label><br> <span id="final_duration"
									style="color: #c33f45;"></span>

							</div>
							<input type="hidden" id="final_hrprice" value="">
							<div class="form-group">
								<label>Price </label><br> SGD<span id="final_totalprice"
									style="color: #c33f45;"></span>

							</div>



						</div>


						<div class="col-md-4">
							<h3 style="color: #c33f45; font-weight: bold">Request Details</h3>
							<div class="form-group ">
								<label for="particular">Details</label>
								<textarea name="notes" placeholder="notes" id="notes"
									class="form-control" rows="10"> </textarea>
							</div>
						</div>


						<button class="btn btn-success btn-lg pull-right" id="hire_talent"
							type="button">Quote Talent</button>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div class="container" id="hire_result">
		<div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4">
				<a href="<?php echo base_url();?>home/search"
					class="btn btn-success">Hire talents </a>
			</div>
			<div class="col-md-4"></div>
		</div>
	</div>
</div>
<script src="<?php echo base_url();?>js/index1.js"
	type="text/javascript"></script>