<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>Talent Portal</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description" content="" />
<meta name="keywords" content=""> 
<meta name="author" content="" />
<meta name="MobileOptimized" content="320">
<!--srart theme style -->
<link href="<?php echo base_url();?>theme_assets/css/main.css"
	rel="stylesheet" type="text/css" />

<!-- end theme style -->
<!-- favicon links -->
<link rel="shortcut icon" type="image/png"
	href="<?php echo base_url();?>theme_assets/images/favicon.png" />
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    
   <script src="<?php echo base_url();?>theme_assets/js/jquery-1.11.3.min.js" type="text/javascript"></script>
    <!-- Animation Css -->
    <link href="<?php echo base_url();?>theme_assets/js/plugins/animate-css/animate.css" rel="stylesheet" />
    <script src="<?php echo base_url();?>theme_assets/js/alert.js" type="text/javascript"></script> 
 
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	
	
	<style type="text/css">@media (max-width:991px)  {.col-xs-6 {
    width: 100%;
}.mj_feature_product_img img {
    width: 100% !important;
}  }
.mj_navmenu ul li a {
    
    color: #333333 ;
    }
    
    body{
    font-family:georgia,palatino,book antiqua,palatino linotype,serif !important;
    }
	</style>

<script type="text/javascript">
 $(document).ready(function(){
	 $("#open_signup").click(function() {
	     $("#myModal2").modal('toggle');
	    $("#myModal1").modal('toggle');
	 });
 $("#open_login").click(function() {
     $("#myModal2").modal('toggle');
    $("#myModal1").modal('toggle');
 });
  $("#head_search").click(function(){
	 
	 var search_key=$("#search_key").val();
	 var dataString='search_key='+search_key;

	 window.location.href = "<?php echo base_url();?>home/search/" + search_key; 
 
  });

   $("#client").click(function(){

	   window.location.href="<?php echo base_url();?>";
   });

   /* Signup process */
      $('#check3').click(function() {
        if ($(this).is(':checked')) {
            $('#signup').removeAttr('disabled');
        } else {
           
             $('#signup').attr('disabled', 'disabled');
        }
    });

	 

	 $("#error_alert").hide();
		$("#signup").click(function(){
			var parentURL = window.parent.location.href
       
		
		var signup_email	=$('#signup_email').val();
		var signup_password=$('#signup_password').val();
		var type=1;
		var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var password_filter=/^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i;
		
		if(signup_email == '' || signup_password == '' ){
			
			 $('.error').html("Please enter all fields.");
	         $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
	             $("#error_alert").slideUp(500);
	              }); 

		}else if (!emailFilter.test(signup_email)){
			$('#myModal1 .modal-dialog').addClass('shake');
			 $('.error').html("Please enter Valid email address.");
	        $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
	            $("#error_alert").slideUp(500);
	             }); 
		}else if(!signup_password.match(password_filter)){
			
			 $('.error').html("Password should combination of numbers and characters.");
	         $("#error_alert").fadeTo(4000, 1000).slideUp(1000, function(){
	             $("#error_alert").slideUp(5000);
	              }); 
		}
		else{
			var dataString='signup_email='+signup_email+'&signup_password='+signup_password+'&type='+type;
			
        	$.ajax({
        	type: "POST",
        	url:"<?php echo base_url(); ?>talents/signup",
        	data:dataString,
        
        	success: function(data){
            	
        		if(data=="success"){
        			
        			$('.success').addClass('alert alert-success').html("Please check your mail and verify your account");
        			$("#form_part").hide();
        		}else if(data =="exists"){
        			$('.error').html("This mail id already exists");
        			 $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
        		            $("#error_alert").slideUp(500);
        		             }); 
        			
        		}

        	},
            error: function(jqXHR, textStatus) {
                alert( "Request failed: " + jqXHR );
            }
        	});

		    
		
		}
		return false;

		});
		});


 /* lgoin for talent provider */

 $(document).ready(function(){

 	 $(document).bind('keypress', function(e) {
          if(e.keyCode==13){
               $('#login').trigger('click');
           }
      });

 	$("#repass").click(function(){
 	 	var npass	=$('#npass').val();
 	 	var cpass=$('#cpass').val();
 	 	var tal_id=$('#tal_id').val();
 	
 	 	if( npass == '' || cpass == ''){
 	 		
 	          $('.lerror').addClass('alert alert-danger').html("Please enter all fields.");
 	          $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
 		            $("#error_div").slideUp(500);
 		             }); 

 	 	}else if (npass!=cpass){
 	 		
 	         $('.lerror').addClass('alert alert-danger').html("passwords does not match ");
 	         $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
 		            $("#error_div").slideUp(500);
 		             }); 
 	 	}
 	 	else{


 	 	var dataString='npass='+npass+'&tal_id='+tal_id;
 	 	$.ajax({
 	 	type: "post",
 	 	url:"<?php echo base_url(); ?>talents/password_update",
 	 	data:dataString  ,
 	 	success: function(data){
alert("password Updated Successfully");
window.location.href="<?php echo base_url();?>talents/talents_dashboard";
 	 	},
 	     
 	 	});
 	 	}

 	 	});
 	 
 	$("#talents_login").click(function(){
 	var email	=$('#lemail').val();
 	var password=$('#lpassword').val();

 	var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
 	if( email == '' || password == ''){
 		
          $('.lerror').addClass('alert alert-danger').html("Please enter all fields.");
          $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#error_div").slideUp(500);
	             }); 

 	}else if (!emailFilter.test(email)){
 		
         $('.lerror').addClass('alert alert-danger').html("Please enter Valid email address.");
         $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#error_div").slideUp(500);
	             }); 
 	}
 	else{


 	var dataString='password='+password+'&email='+email;
 	$.ajax({
 	type: "post",
 	url:"<?php echo base_url(); ?>talents/login",
 	data:dataString  ,
 	success: function(data){

 		if(data == "failure"){
 			
              $('.lerror').addClass('alert alert-danger').html("Invalid email/password combination");
              $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#error_div").slideUp(500);
		             }); 
              $('input[type="password"]').val('');

 		}else{
 	 		if(data == 1){
 	 			$('#myModal2').modal('toggle');
 	 			 $('.sucess_content').addClass('alert alert-success').html("Talent Login Successfully");
 	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
 			            $("#sucess_div").slideUp(500);
 			             }); 
 	 	 		setTimeout(function(){
 	 	 			window.location.href="<?php echo base_url();?>talents/talents_dashboard"; },1000);
 			
 	 		}else if(data ==2){
 	 			$('#myModal2').modal('toggle');
 	 			 $('.sucess_content').addClass('alert alert-success').html("Client Login Successfully");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			window.location.href="<?php echo base_url();?>clients";},1000);
 	 			
 	 		}

 		}
 	},
     error: function(jqXHR, textStatus) {
         alert( "Request failed: " + jqXHR );
     }
 	});
 	}
 	return false;

 	});
 	});
 </script>

</head>

<body>
	<!--Loader Start -->
	<div class="mj_preloaded">
		<div class="mj_preloader">
			<div class="lines">
				<div class="line line-1"></div>
				<div class="line line-2"></div>
				<div class="line line-3"></div>
			</div>

			<div class="loading-text">LOADING</div>
		</div>
	</div>
	<!--Loader End -->
	
	<div class="mj_header" style="background-color:#fff !important;color:#333333 !important;">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="mj_logo">
						<a href="<?php echo base_url();?>"><img
							src="<?php echo base_url();?>theme_assets/images/Logo.JPG"
							style="height: 100px !important" class="img-responsive" alt="logo" align="right">
						</a>
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target=".mj_menu"
							aria-expanded="false">
							<span class="sr-only">MENU</span> <span class="icon-bar"></span>
							<span class="icon-bar"></span> <span class="icon-bar"></span>
						</button>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<div class="collapse navbar-collapse mj_navmenu mj_menu"
						id="mj_menu">

						<ul
							class="nav navbar-nav navbar-right mj_right_menu mj_withoutlogin_menu" >
							
							<?php if($type ==""){?>
							<li class="mj_searchbtn1"
								style="width: 800px; margin-top: 9px; margin-right: -320px">

								<div class="input-group col-md-6">
									<input type="text" class="  search-query form-control"
										name="search_key" id="search_key" placeholder="Find an event talent here ! " />  <span
										class="input-group-btn">
										<button class="btn btn-danger" type="button" id="head_search">
											<span class=" glyphicon glyphicon-search"></span>
										</button>
									</span>
								</div>

							</li>
							<li><button id="client" class="btn btn-danger"
									style="margin-top: 8px">Join as a client</button></li>
							<li><a href="#" data-toggle="modal" data-target="#myModal1"><i
									class="fa fa-lock"></i> Sign Up</a></li>
							<li><a href="#" data-toggle="modal" data-target="#myModal2"><i
									class="fa fa-user"></i> Login</a></li>
									<?php }elseif($type==1){?>
	
					<li style="margin-top: 16px !important" class="<?php if($this->uri->segment(2)=="talents_profile"){echo "active";}?>"><a href="<?php echo base_url();?>talents/talents_profile">PROFILE</a>			
					<li style="margin-top: 16px !important" class="<?php if($this->uri->segment(2)=="talents_dashboard"){echo "active";}?>"><a href="<?php echo base_url();?>talents/talents_dashboard">DASHBOARD</a>
					<li style="margin-top: 16px !important" class="<?php if($this->uri->segment(2)=="discover"){echo "active";}?>"><a href="<?php echo base_url();?>talents/discover">FINANCE</a>			
					<li style="margin-top: 16px !important" class="<?php if($this->uri->segment(2)=="support"){echo "active";}?>"><a href="<?php echo base_url();?>talents/support">SUPPORT</a>	
					<li style="margin-top: 16px !important" class="<?php if($this->uri->segment(2)=="contact"){echo "active";}?>"><a href="<?php echo base_url();?>talents/contact">CONTACT</a>
						<li ><a class="mj_profileimg ">
							
							<?php
									
									
			$prinimage='<img id="myimage"
		src="'.base_url().'/theme_assets/images/default_user.png"
		class="img-circle" alt="Cinque Terre" width="60" height="60"  style="border-radius:40px !important">
    	';	
			if($talents_info->id != ''){

				$files = glob('talents_profilepic/*'); //get all file names
				foreach($files as $file){
					if(is_file($file))
					{ $picname="talent".$talents_info->talents_id."pic";

					$photos_from_direc = explode('/',$file);
					$photoname= explode('.',$photos_from_direc[1]);
					if($photoname[0]==$picname){
						$prinimage='<img id="myimage"
		src=" '.base_url().'talents_profilepic/'.$picname.'"
		class="img-circle" alt="Profile " width="60" height="60"  style="border-radius:40px !important">
    	';		
					}

					//unlink($file);
					}//delete file
				}


			}

			echo $prinimage;
			?>
							
									
									
									
									<i class="fa fa-angle-down"></i> </a></li>
									<?php }?>
						</ul>
						
						<div class="mj_profilediv" id="my_profile_div">
							<ul>
								<li><a href="<?php echo base_url();?>talents/talents_profile"><i class="fa fa-user"></i> Profile</a></li>
								<li><a href="<?php echo base_url();?>talents/talents_dashboard"><i class="fa fa-dashboard"></i>Dashboard</a></li>
								<li><a href="#" data-toggle="modal" data-target="#myModal3"><i class="fa fa-cog"></i>Password Change</a></li>
								<li><a href="<?php echo base_url();?>talents/logout">logout</a></li>
							</ul>
						</div>

					</div>
				</div>

			</div>
		</div>
		

		<div class="container" style="padding: 5px 0px;">
			<div class="row" style="color: black !important">
	<div class="col-md-4 "></div>
				<div class="col-md-8 col-sm-12 col-xs-12">
					<div class="collapse navbar-collapse mj_navmenu mj_menu"
						id="mj_menu">
						<ul class="nav navbar-nav" style="float:right">
<?php if($type=="" || $type==2){?>
							<li><a href="<?php echo base_url();?>home/search/Acrobatics">Acrobatics</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Hula hoop performance">Hula hoop performance</a></li>
									<li><a href="<?php echo base_url();?>home/search/Plate spinners">Plate spinners</a></li>
									<li><a href="<?php echo base_url();?>home/search/Human pyramid">Human pyramid</a></li>
									<li><a href="<?php echo base_url();?>home/search/Chair tower">Chair tower</a></li>
									<li><a href="<?php echo base_url();?>home/search/Juggling">Juggling</a></li>
									<li><a href="<?php echo base_url();?>home/search/Balance beam">Balance beam</a></li>
									<li><a href="<?php echo base_url();?>home/search/Acro ball and hoop">Acro ball and hoop</a></li>
									<li><a href="<?php echo base_url();?>home/search/The power duo">The power duo</a></li>
									<li><a href="<?php echo base_url();?>home/search/Aerial performance">Aerial performance</a></li>
								</ul></li>

							<li><a href="<?php echo base_url();?>home/search/Crowd minglers">Crowd minglers</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Clowns and jesters">Clowns and jesters</a></li>
									<li><a href="<?php echo base_url();?>home/search/Plate spinners">Plate spinners</a></li>
									<li><a href="<?php echo base_url();?>home/search/Unicyclist">Unicyclist</a></li>
									<li><a href="<?php echo base_url();?>home/search/Spandex man">Spandex man</a></li>
									<li><a href="<?php echo base_url();?>home/search/Roving musicians">Roving musicians</a></li>
									<li><a href="<?php echo base_url();?>home/search/Human statues">Human statues</a></li>
									<li><a href="<?php echo base_url();?>home/search/Mascots">Mascots</a></li>
									<li><a href="<?php echo base_url();?>home/search/Mimes">Mimes</a></li>

								</ul></li>
							<li><a href="<?php echo base_url();?>home/search/Musical acts">Musical acts</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Taiko drummers">Taiko drummers</a></li>
									<li><a href="<?php echo base_url();?>home/search/Modern Chinese Orchestra">Modern Chinese Orchestra</a></li>
									<li><a href="<?php echo base_url();?>home/search/The String Trio">The String Trio</a></li>
								</ul></li>
							<li><a href="<?php echo base_url();?>home/search/Dances">Dances</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Modern ribbon dancers">Modern ribbon dancers</a></li>
									<li><a href="<?php echo base_url();?>home/search/Chinease ribbon dancers">Chinease ribbon dancers</a></li>
									<li><a href="<?php echo base_url();?>home/search/Samba dancers">Samba dancers </a></li>
									<li><a href="<?php echo base_url();?>home/search/Contemp /jazz dance">Contemp /jazz dance</a></li>
									<li><a href="<?php echo base_url();?>home/search/Hawaiian dancers">Hawaiian dancers</a></li>
									<li><a href="<?php echo base_url();?>home/search/Thousand hand bodhisattva">Thousand hand bodhisattva</a></li>
								</ul></li>
							<li><a href="<?php echo base_url();?>home/search/Illusion and magic">Illusion and magic</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Close up and magic">Close up and magic</a></li>
								</ul></li>
							<li><a href="#">Variety shows</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Chen Lei Impresonator">Chen Lei Impresonator</a></li>
									<li><a href="<?php echo base_url();?>home/search/Elvis Presley Impresonator">Elvis Presley Impresonator</a></li>
									<li><a href="<?php echo base_url();?>home/search/Tersa Teng Impresonator">Tersa Teng Impresonator</a></li>
									<li><a href="<?php echo base_url();?>home/search/Lady Gaga Impresonator">Lady Gaga Impresonator</a></li>
									<li><a href="<?php echo base_url();?>home/search/Fire dancers">Fire dancers</a></li>
									<li><a href="<?php echo base_url();?>home/search/The ballon cocoon">The ballon cocoon</a></li>
									<li><a href="<?php echo base_url();?>home/search/Cyber Freak">Cyber Freak</a></li>
									<li><a href="<?php echo base_url();?>home/search/Fire Show">CAUTION ! Fire Show</a></li>

								</ul></li>
							<li><a href="<?php echo base_url();?>home/search/Interactive attractions">Interactive attractions</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Photo booth">Photo booth</a></li>
									<li><a href="<?php echo base_url();?>home/search/Airbrush nail glitter art">Airbrush /nail/glitter art</a></li>
									<li><a href="<?php echo base_url();?>home/search/Face painting">Face painting</a></li>
									<li><a href="<?php echo base_url();?>home/search/Balloon sculptor">Balloon sculptor</a></li>
									<li><a href="<?php echo base_url();?>home/search/Henna aart">Henna aart</a></li>
									<li><a href="<?php echo base_url();?>home/search/Popcorn and candyfloss">Popcorn and candyfloss</a></li>
									<li><a href="<?php echo base_url();?>home/search/Calligraphy artist">Calligraphy artist</a></li>
									<li><a href="<?php echo base_url();?>home/search/Chinese fortune teller">Chinese fortune teller</a></li>

									<li><a href="<?php echo base_url();?>home/search/Tarot card reader">Tarot card reader</a></li>
								</ul></li>
							<li class=""><a href="<?php echo base_url();?>home/search/Lighted shows">Lighted shows</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/search/Lazer girl">Lazer girl</a></li>
									<li><a href="<?php echo base_url();?>home/search/Laser man">Laser man</a></li>

									<li><a href="<?php echo base_url();?>home/search/Laser mingler">Laser mingler</a></li>
									<li><a href="<?php echo base_url();?>home/search/Led dance poi">Led dance & poi</a></li>
								</ul></li>
								<li class="" style="background-color: green;border-radius:5px"><a href="#">Contact Us</a>
								<ul class="sub_menu">
									<li><a href="<?php echo base_url();?>home/contactus_talent">Talent</a></li>
									<li><a href="<?php echo base_url();?>home/contactus_company">Company</a></li>
								</ul></li>
								<?php }else if($type==1){?>
						
								<?php }?>
						</ul>
					


					</div>
				</div>
			</div>
		</div>
		
	</div>
<div class="modal fade mj_popupdesign" id="myModal2" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-body">
					<div
						class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
						<div class="row">
							<div
								class="mj_pricingtable mj_bluetable mj_freelancer_form_wrapper">

								<h4 style="color: #2bacc1; font-size: 20px">Login</h4>
								
								<div class="row">
									<div class="col-md-2"></div>
									<div class="success col-md-8 "
										style="font--size: 15px !important"></div>
									<div class="col-md-4"></div>
								</div>
								<div class="row" id="error_div">
								<div class="col-sm-2"></div>
								<div class="col-sm-8 lerror" style="font-size: 12px !important"></div>
							</div>
								<form>
									<div class="mj_freelancer_form">
									<div class="form-group">
									<input type="text" placeholder="Username or Email" id="lemail"
										class="form-control">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password" id="lpassword"
										class="form-control">
								</div>
								<div class="form-group"></div>
								<div class="mj_showmore">
									<a href="#" id="talents_login"
										class="mj_showmorebtn mj_bluebtn">login now!</a>
								</div>
								<div class="mj_showmore">
									<a href="#" id="open_signup">Looking to create an account ?</a>
								</div>
									</div>
								</form>

								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<img
										src="<?php echo base_url();?>theme_assets/images/close.png"
										alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade mj_popupdesign" id="myModal1" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-body">
					<div
						class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
						<div class="row">
							<div
								class="mj_pricingtable mj_bluetable mj_freelancer_form_wrapper">

								<h4 style="color: #2bacc1; font-size: 20px">Talents Signup</h4>
								<br>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<a class="btn btn-block btn-social btn-facebook"
											style="color: white !important"> <span class="fa fa-facebook"></span>
											Sign in with Facebook
										</a>
									</div>
									<div class="col-md-2"></div>
								</div>
<br><br>

								<div class="row">
									<div class="col-md-2"></div>
									<div class="success col-md-8 "
										style="font--size: 15px !important"></div><div class="col-md-4"></div></div>
										<div class="row">
										<div class="col-md-2"></div>
									<div class="col-md-8  alert alert-danger" id="error_alert">

										<strong class="error"> </strong>
									</div>
								</div>
								<form>
									<div class="mj_freelancer_form">
										<div id="form_part">
											<div class="form-group ">
												<input type="email" placeholder="Email" id="signup_email"
													class="form-control">
											</div>
											<div class="form-group">
												<input type="password" placeholder="Password"
													id="signup_password" class="form-control">
											</div>




											<div class="form-group mj_toppadder20">
												<div class="mj_checkbox">
													<input type="checkbox" value="1" id="check3"
														name="checkbox"> <label for="check3"></label>
												</div>
												<span> I have read, understand and agree to the Talent
													Portal Terms of Service, including the <a href="#">User
														Agreement</a> and <a href="#">Privacy Policy</a>.
												</span>
											</div>

											<div class="mj_pricing_footer"
												style="background-color: #fff !important">
												<button class="btn btn-success" id="signup" disabled>Sign Up</button>
											</div>
										</div>
											<div class="mj_showmore">
									<a href="#" id="open_login">Login your account ?</a>
								</div>
									</div>
								</form>

								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<img
										src="<?php echo base_url();?>theme_assets/images/close.png"
										alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="modal fade mj_popupdesign" id="myModal3" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-body">
					<div
						class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
						<div class="row">
							<div
								class="mj_pricingtable mj_bluetable mj_freelancer_form_wrapper">

								<h4 style="color: #2bacc1; font-size: 20px">Password Reset</h4>
								
								<div class="row">
									<div class="col-md-2"></div>
									<div class="success col-md-8 "
										style="font--size: 15px !important"></div>
									<div class="col-md-4"></div>
								</div>
								<div class="row" id="error_div">
								<div class="col-sm-2"></div>
								<div class="col-sm-8 lerror" style="font-size: 12px !important"></div>
							</div>
								<form>
								<input type="hidden" value="<?php echo $id?>" id="tal_id">
									<div class="mj_freelancer_form">
									
								<div class="form-group">
									<input type="password" placeholder="Password" id="npass"
										class="form-control">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Confirm Password" id="cpass"
										class="form-control">
								</div>
								<div class="form-group"></div>
								<div class="mj_showmore">
									<a href="#" id="repass"
										class="mj_showmorebtn mj_bluebtn">Change Password</a>
								</div>
								
									</div>
								</form>

								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<img
										src="<?php echo base_url();?>theme_assets/images/close.png"
										alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script>
    

        
    $(document).ready(function(){
        
        /** 
         * This part does the "fixed navigation after scroll" functionality
         * We use the jQuery function scroll() to recalculate our variables as the 
         * page is scrolled/
         */
        $(window).scroll(function(){
            var window_top = $(window).scrollTop() + 12; // the "12" should equal the margin-top value for nav.stick
            var div_top = $('#nav-anchor').offset().top;
                if (window_top > div_top) {
                    $('nav').addClass('stick');
                } else {
                    $('nav').removeClass('stick');
                }
        });
        
        
        /**
         * This part causes smooth scrolling using scrollto.js
         * We target all a tags inside the nav, and apply the scrollto.js to it.
         */
        $("nav a").click(function(evn){
            evn.preventDefault();
            $('html,body').scrollTo(this.hash, this.hash); 
        });
        
        
        
        /**
         * This part handles the highlighting functionality.
         * We use the scroll functionality again, some array creation and 
         * manipulation, class adding and class removing, and conditional testing
         */
        var aChildren = $("nav li").children(); // find the a children of the list items
        var aArray = []; // create the empty aArray
        for (var i=0; i < aChildren.length; i++) {    
            var aChild = aChildren[i];
            var ahref = $(aChild).attr('href');
            aArray.push(ahref);
        } // this for loop fills the aArray with attribute href values
        
        $(window).scroll(function(){
            var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
            var windowHeight = $(window).height(); // get the height of the window
            var docHeight = $(document).height();
            
            for (var i=0; i < aArray.length; i++) {
                var theID = aArray[i];
                var divPos = $(theID).offset().top; // get the offset of the div from the top of page
                var divHeight = $(theID).height(); // get the height of the div in question
                if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                    $("a[href='" + theID + "']").addClass("nav-active");
                } else {
                    $("a[href='" + theID + "']").removeClass("nav-active");
                }
            }
            
            if(windowPos + windowHeight == docHeight) {
                if (!$("nav li:last-child a").hasClass("nav-active")) {
                    var navActiveCurrent = $(".nav-active").attr("href");
                    $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                    $("nav li:last-child a").addClass("nav-active");
                }
            }
        });
    });

</script>