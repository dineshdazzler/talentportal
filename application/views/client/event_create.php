<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>theme_assets/css/jquery.datetimepicker.css" />
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}
</style>

<script type="text/javascript">
function add_profilepic(id)
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit Event Picture'); // Set title to Bootstrap modal title
}


function parseDate(s) {
	  var b = s.split(/\D/);
	  return new Date(b[2], --b[0], b[1]);
	}
/*    General info */
$(document).ready(function(){
	
	$("#error_edit").hide();
	
	
	$("form#form_eventcreate").submit(function(event){
		 
		  //disable the default form submission
		  event.preventDefault();

		   //grab all form data  
		   var formData = new FormData($(this)[0]);
           var starttime=$("#eventstart").val();
           var endtime=$("#eventend").val();

           var today = new Date();
           today.setHours(0,0,0,0);


           if (new Date(starttime) > today ) {
        	   
        	 
			if(new Date(endtime) > new Date(starttime)){
alert("fd");
				 $.ajax({
					    url: '<?php echo base_url();?>clients/event_insert',
					    type: 'POST',
					    data: formData,
					    async: false,
					    cache: false,
					    contentType: false,
					    processData: false,
					    success: function (data) {
						alert("success");	 
						window.location.href="<?php echo base_url();?>clients/clients_dashboard";	 
						
					},
					error: function(jqXHR, textStatus) {
						alert( "Request failed: " + jqXHR );
					}
				});

			}else{
        alert("Please select end date larger than start date");
			}

		

           }else{

alert("Please select startdate and end date larger than today!");
               }

		


	});


});





	</script>



<section id="plan_event">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Plan Your Event!</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="" style="background-color: white !important; text-align: left"> 
<div class="container" >
	<div class="row mj_toppadder20"
		>
		
		<div class="col-md-3"></div>
		<div class="col-md-6">
			<form id="form_eventcreate" enctype="multipart/form-data">
				<input type="hidden" name="c_id" id="c_id" value="<?php echo $id;?>">

				<fieldset>

					<div class="row">
						<div class="col-sm-6">

							<div class="form-group vali_eventtitle">
								<label for="particular">Event Name *</label> <input type="text"
									name="eventtitle" placeholder="Event name" id="eventtitle"
									value="" class="form-control" required> <span class="text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">

							<div class="form-group vali_eventtitle">
								<div class="form-group vali_eventtype">
									<label for="particular">Event Type *</label> <input type="text"
									name="eventtype" placeholder="Event type" id="eventtype"
									value="" class="form-control" required> <span class="text-danger"></span>


								</div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group vali_eventstart">
								<label for="particular">Event Start Time *</label> <input
									type="text" name="eventstart" title="Start Time"
									id="eventstart" value="" class="form-control some_class"> <span
									class="text-danger"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group vali_eventend">
								<label for="particular">Event End Time *</label> <input
									type="text" name="eventend" title="End Time" id="eventend"
									value="" class="form-control some_class"> <span
									class="text-danger"></span>
							</div>
						</div>

					</div>
					<div class="row">

						<div class="col-sm-6">
							<div class="form-group vali_pax">
								<label for="particular">Number of Pax *</label> <input
									type="text" name="pax" title="Number" id="pax" value=""
									class="form-control"> <span class="text-danger"></span>
							</div>
						</div>

						<div class="col-sm-6">
							<div class="form-group vali_budget">
								<label for="Loantype">Budget(S$) *</label><br>
								<div>
									<input type="text" name="budget" title="Budget" id="budget"
										value="" class="form-control">
								</div>

								<span class="text-danger"></span>
							</div>
						</div>

					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="form-group vali_description">
								<label for="particular">Description </label>
								<textarea rows="3" cols="" id="description" name="description"
									class="form-control"></textarea>
								<span class="text-danger"></span>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="form-group vali_venue">
								<label for="particular">Location *</label> <input type="text"
									name="venue" title="Venue" id="venue" value=""
									class="form-control"> <span class="text-danger"></span>
							</div>
						</div>

					</div>

					<div class="row">
						<div class="col-sm-6"></div>
						<div class="col-sm-6">
						
							<div class="form-group pull-right">
<?php if($id!=""){?>
								<input type="submit" class="btn btn-success form-control"
									value="Plan Now !" >
									<?php }else{?>
									<input type="submit" class="btn btn-success"
									value="Plan Now !" disabled="disabled">
									
									<span style="color:red">Please Login First !!!</span><?php }?>
							</div>
						</div>

					</div>
					



					<div class="form-group">
						<br>



					</div>
				</fieldset>
			</form>


		</div>
</div>

	</div>
</div>
</section>



<!-- End Bootstrap modal -->
<script src="<?php echo base_url();?>theme_assets/js/jquery.js"></script>
<script
	src="<?php echo base_url();?>theme_assets/js/jquery.datetimepicker.full.js"></script>
<script>



$('.some_class').datetimepicker();




</script>


		