<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>theme_assets/css/jquery.datetimepicker.css" />
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}
</style>

<script type="text/javascript">
function add_profilepic(id)
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}



/*    General info */
$(document).ready(function(){

	$("#error_edit").hide();
	$("form#form_eventedit").submit(function(event){
		 
		  //disable the default form submission
		  event.preventDefault();

		   //grab all form data  
		   var formData = new FormData($(this)[0]);
         var starttime=$("#eventstart").val();
         var endtime=$("#eventend").val();

         var today = new Date();
         today.setHours(0,0,0,0);


         if (new Date(starttime) > today ) {
      	   
      	 
			if(new Date(endtime) > new Date(starttime)){

				 $.ajax({
					    url: '<?php echo base_url();?>clients/event_update/',
					    type: 'POST',
					    data: formData,
					    async: false,
					    cache: false,
					    contentType: false,
					    processData: false,
					    success: function (data) {
						alert("success");	 
						window.location.href="<?php echo base_url();?>clients/clients_dashboard";	 
						
					},
					error: function(jqXHR, textStatus) {
						alert( "Request failed: " + jqXHR );
					}
				});

			}else{
      alert("Please select end date larger than start date");
			}

		

         }else{

alert("Please select startdate and end date larger than today!");
             }

		


	});


});





	</script>
<section id="plan_event">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Edit Your Event!</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="" style="background-color: white !important; text-align: left;margin-bottom:80px"> 
    <div class="container" >
	<div class="row mj_toppadder20">
		
		<div class="col-md-3"></div>
		<div class="col-md-6">


												<form id="form_eventedit" enctype="multipart/form-data">
												<?php foreach($result as $selected): ?>
													<input type="hidden" name="c_id" id="c_id"
														value="<?php echo $id;?>">
														<input type="hidden" name="e_id" id="e_id"
														value="<?php echo $selected->id;?>">

													<fieldset>

														<div class="row">
															<div class="col-sm-6">
																<div class="form-group vali_eventtitle">
																	<label for="particular">Event Title *</label> <input
																		type="text" name="eventtitle" placeholder="First Name"
																		id="eventtitle" value="<?php echo $selected->event_title;?>" class="form-control"> <span
																		class="text-danger"></span>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group vali_eventtype">
																	<label for="particular">Event Type *</label> 
																	<input type="text"
																		required id="eventtype" name="eventtype"
																		class="Loantype country form-control" value="<?php echo $selected->event_type;?>">
																		 <span class="text-danger" ></span>
																</div>

															</div>

														</div>

														<div class="row">

															<div class="col-sm-6">
																<div class="form-group vali_eventstart">
																	<label for="particular">Event Start Time *</label> <input
																		type="text" name="eventstart" title="Start Time"
																		id="eventstart" value="<?php echo $selected->event_start;?>"
																		class="form-control some_class"> <span
																		class="text-danger"></span>
																</div>
															</div>

															<div class="col-sm-6">
																<div class="form-group vali_eventend">
																	<label for="particular">Event End Time *</label> <input
																		type="text" name="eventend" title="End Time"
																		id="eventend" value="<?php echo $selected->event_end;?>" class="form-control some_class">
																	<span class="text-danger"></span>
																</div>
															</div>

														</div>
														<div class="row">

															<div class="col-sm-6">
																<div class="form-group vali_pax">
																	<label for="particular">Number of Pax *</label> <input
																		type="text" name="pax" title="Phone number" id="pax"
																		value="<?php echo $selected->pax;?>" class="form-control"> <span
																		class="text-danger"></span>
																</div>
															</div>

															<div class="col-sm-6">
																<div class="form-group vali_budget">
																	<label for="Loantype">Budget *</label><br>
																	<div>
																		<input type="text" name="budget" title="Budget"
																			id="budget" value="<?php echo $selected->budget;?>" class="form-control">
																	</div>

																	<span class="text-danger"></span>
																</div>
															</div>

														</div>
														<div class="row">



															
															<div class="col-sm-6">
																<div class="form-group vali_venue">
																	<label for="particular">Venue *</label> <input
																		type="text" name="venue" title="Venue" id="venue"
																		value="<?php echo $selected->venue;?>" class="form-control"> <span
																		class="text-danger"></span>
																</div>
															</div>


														
															<div class="col-sm-6">
																<div class="form-group vali_description">
																	<label for="particular">Description *</label>
																	<textarea rows="" cols="" id="description" name="description"
																		class="form-control"><?php echo $selected->description;?></textarea>
																	<span class="text-danger"></span>
																</div>
															</div>
															
														</div>
														

															<div class="form-group">
																<br> <input type="submit" name="update" value="Save"
																	id="event_update" class="btn btn-success"><a href="<?php echo base_url();?>clients/clients_dashboard">
																	&nbsp;<input type="button" name="cancel" value="Cancel"
																	id="cancel" class="btn btn-danger"></a>
															</div>
													</fieldset>
												</form>
<?php endforeach; ?> 
													<span class="text-success"></span>
												<span class="text-danger"></span>
											</div>
										</div>
									</div>



								</div>   
							</section>



<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Person Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="pid" />
					<div class="form-body">
						<div id="photo_upload">
							<div class="mbot error-message-highlight hidden"
								id="progress_report_status"
								style="font-size: 12px; text-transform: none;"></div>
							<label for="profile_photo_upload">Upload Picture</label>
							<div class="">

								<!-- Conditional comments are dropped from IE post IE 10 -->
								<input id="profile_photo_upload" name="photo" type="file"
									class="file-up">

							</div>

							<div
								style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
								JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
								in height and width
							</div>
							<div class="upload_status"></div>

							<div id="progress_report" style="">
								<div id="progress_report_name"></div>

								<div id="progress_report_bar_container"
									style="height: 5px; margin-top: 10px">
									<div id="progress_report_bar"
										style="background-color: #cb202d; width: 0; height: 100%;"></div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()"
					class="btn btn-primary">Upload</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="<?php echo base_url();?>theme_assets/js/jquery.js"></script>
<script
	src="<?php echo base_url();?>theme_assets/js/jquery.datetimepicker.full.js"></script>
<script>



																																																							$('.some_class').datetimepicker();




																																																							</script>