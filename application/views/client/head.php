
<!DOCTYPE html>
<!-- 
Template Name: MeshJobs Multipurpose Responsive HTML Template
Version: 1.1
Author: DigiSamaritan
Website: digisamaritan.com
Purchase: http://themeforest.net/user/DigiSamaritan
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
<meta charset="utf-8" />
<title>Talent Portal</title>
<meta content="width=device-width, initial-scale=1.0" name="viewport" />
<meta name="description" content="" />
<meta name="keywords" content="">
<meta name="author" content="" />
<meta name="MobileOptimized" content="320">
<!--srart theme style -->
<link href="<?php echo base_url();?>theme_assets/css/main.css"
	rel="stylesheet" type="text/css" />

<!-- end theme style -->
<!-- favicon links -->
<link rel="shortcut icon" type="image/png"
	href="<?php echo base_url();?>theme_assets/images/favicon.png" />
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Animation Css -->
<link
	href="<?php echo base_url();?>theme_assets/js/plugins/animate-css/animate.css"
	rel="stylesheet" />


<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<script type="text/javascript">
 $(document).ready(function(){

   $("#client").click(function(){

	   window.location.href="<?php echo base_url();?>";
   });

   /* Signup process */
      $('#check3').click(function() {
        if ($(this).is(':checked')) {
            $('#signup').removeAttr('disabled');
        } else {
           
             $('#signup').attr('disabled', 'disabled');
        }
    });

	 

	 $("#error_alert").hide();
		$("#signup").click(function(){
			var parentURL = window.parent.location.href
       
		
		var signup_email	=$('#signup_email').val();
		var signup_password=$('#signup_password').val();
		var type=2;
		var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
		var password_filter=/^(?=\D*\d)(?=[^a-z]*[a-z])[0-9a-z]+$/i;
		
		if(signup_email == '' || signup_password == '' ){
			
			 $('.error').html("Please enter all fields.");
	         $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
	             $("#error_alert").slideUp(500);
	              }); 

		}else if (!emailFilter.test(signup_email)){
			$('#myModal1 .modal-dialog').addClass('shake');
			 $('.error').html("Please enter Valid email address.");
	        $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
	            $("#error_alert").slideUp(500);
	             }); 
		}else if(!signup_password.match(password_filter)){
			
			 $('.error').html("Password should combination of numbers and characters.");
	         $("#error_alert").fadeTo(4000, 1000).slideUp(1000, function(){
	             $("#error_alert").slideUp(5000);
	              }); 
		}
		else{
			var dataString='signup_email='+signup_email+'&signup_password='+signup_password+'&type='+type;
			
        	$.ajax({
        	type: "POST",
        	url:"<?php echo base_url(); ?>talents/signup",
        	data:dataString,
        	success: function(data){
        		if(data=="successs"){
        			
        			$('.success').addClass('alert alert-success').html("Please check your mail and verify your account");
        			$("#form_part").hide();
        		}else if(data =="exists"){
        			$('.error').html("This mail id already exists");
        			 $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
        		            $("#error_alert").slideUp(500);
        		             }); 
        			
        		}

        	},
            error: function(jqXHR, textStatus) {
                alert( "Request failed: " + jqXHR );
            }
        	});

		    
		
		}
		return false;

		});
		});


 /* lgoin fortalent provider */

 $(document).ready(function(){

 	 $(document).bind('keypress', function(e) {
          if(e.keyCode==13){
               $('#login').trigger('click');
           }
      });

 	$("#talents_login").click(function(){


 	var email	=$('#lemail').val();
 	var password=$('#lpassword').val();

 	var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
 	if( email == '' || password == ''){
 		
          $('.lerror').addClass('alert alert-danger').html("Please enter all fields.");
          $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#error_div").slideUp(500);
	             }); 

 	}else if (!emailFilter.test(email)){
 		
         $('.lerror').addClass('alert alert-danger').html("Please enter Valid email address.");
         $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
	            $("#error_div").slideUp(500);
	             }); 
 	}
 	else{


 	var dataString='password='+password+'&email='+email;
 	$.ajax({
 	type: "post",
 	url:"<?php echo base_url(); ?>talents/login",
 	data:dataString  ,
 	success: function(data){

 		if(data == "failure"){
 			
              $('.lerror').addClass('alert alert-danger').html("Invalid email/password combination");
              $("#error_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#error_div").slideUp(500);
		             }); 
              $('input[type="password"]').val('');

 		}else{
 	 		if(data == 1){
 			window.location.href="<?php echo base_url();?>talents/talents_dashboard";
 	 		}else if(data ==2){
 	 			window.location.href="<?php echo base_url();?>clients";
 	 		}

 		}
 	},
     error: function(jqXHR, textStatus) {
         alert( "Request failed: " + jqXHR );
     }
 	});
 	}
 	return false;

 	});
 	});
 </script>

</head>
<body>
	<!--Loader Start -->
	<div class="mj_preloaded">
		<div class="mj_preloader">
			<div class="lines">
				<div class="line line-1"></div>
				<div class="line line-2"></div>
				<div class="line line-3"></div>
			</div>

			<div class="loading-text">LOADING</div>
		</div>
	</div>
	<!--Loader End -->
	<div class="mj_header">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
					<div class="mj_logo">
						<a href="#"><img
							src="<?php echo base_url();?>theme_assets/images/logo.png"
							style="height: 80px !important" class="img-responsive" alt="logo">
						</a>
						<button type="button" class="navbar-toggle collapsed"
							data-toggle="collapse" data-target=".mj_menu"
							aria-expanded="false">
							<span class="sr-only">MENU</span> <span class="icon-bar"></span>
							<span class="icon-bar"></span> <span class="icon-bar"></span>
						</button>
					</div>
				</div>
				<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
					<div class="collapse navbar-collapse mj_navmenu mj_menu"
						id="mj_menu">
<ul
							class="nav navbar-nav navbar-right mj_right_menu mj_withoutlogin_menu">
							
							<?php if($type ==""){?>
							<li class="mj_searchbtn1"
								style="width: 800px; margin-top: 9px; margin-right: -320px">

								<div class="input-group col-md-6">
									<input type="text" class="  search-query form-control"
										placeholder="Find an event talent here ! " /> <span
										class="input-group-btn">
										<button class="btn btn-danger" type="button">
											<span class=" glyphicon glyphicon-search"></span>
										</button>
									</span>
								</div>

							</li>
							<li><button id="client" class="btn btn-danger"
									style="margin-top: 8px">Join as a client</button></li>
							<li><a href="#" data-toggle="modal" data-target="#myModal1"><i
									class="fa fa-lock"></i> Sign Up</a></li>
							<li><a class="mj_logintoggle"
								onclick="show_my_div('my_profile_div_login' , 'id')"><i
									class="fa fa-user"></i> Login</a></li>
									<?php }elseif($type==2){?>

							<li><a class="mj_profileimg "><img src="<?php echo base_url();?>theme_assets/images/default_user.png" width="60" height="60" 
									alt="user" style="border-radius:20px !important"><i class="fa fa-angle-down"></i> </a></li>
									<?php }?>
						</ul>
						
						<div class="mj_profilediv" id="my_profile_div_login">
							<div class="row" id="error_div">
								<div class="col-sm-2"></div>
								<div class="col-sm-8 lerror" style="font-size: 12px !important"></div>
							</div>
							<form>
								<div class="form-group">
									<input type="text" placeholder="Username or Email" id="lemail"
										class="form-control">
								</div>
								<div class="form-group">
									<input type="password" placeholder="Password" id="lpassword"
										class="form-control">
								</div>
								<div class="form-group"></div>
								<div class="mj_showmore">
									<a href="#" id="talents_login"
										class="mj_showmorebtn mj_greenbtn">login now!</a>
								</div>
							</form>
						</div>
<div class="mj_profilediv" id="my_profile_div">
							<ul>
								<li><a href="#"><i class="fa fa-user"></i> My account</a></li>
								<li><a href="#"><i class="fa fa-briefcase"></i>Manage jobs</a></li>
								<li><a href="#"><i class="fa fa-cog"></i>settings</a></li>
								<li><a href="<?php echo base_url();?>talents/logout">logout</a></li>
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="container" style="padding: 5px 0px;">
			<div class="row" style="color: black !important">

				<div class="col-md-12 col-sm-12 col-xs-12">
					<div class="collapse navbar-collapse mj_navmenu mj_menu"
						id="mj_menu">
						<ul class="nav navbar-nav">

							<li><a href="#">Acrobatics</a>
								<ul class="sub_menu">
									<li><a href="#">Hula hoop performance</a></li>
									<li><a href="#">Plate spinners</a></li>
									<li><a href="#">Human pyramid</a></li>
									<li><a href="#">Chair tower</a></li>
									<li><a href="#">Juggling</a></li>
									<li><a href="#">Balance beam</a></li>
									<li><a href="#">Acro ball and hoop</a></li>
									<li><a href="#">The power duo</a></li>
									<li><a href="#">Aerial performance</a></li>
								</ul></li>

							<li><a href="#">Crowd minglers</a>
								<ul class="sub_menu">
									<li><a href="#">Clowns and jesters</a></li>
									<li><a href="#">Plate spinners</a></li>
									<li><a href="#">Unicyclist</a></li>
									<li><a href="#">Spandex man</a></li>
									<li><a href="#">Roving musicians</a></li>
									<li><a href="#">Human statues</a></li>
									<li><a href="#">Mascots</a></li>
									<li><a href="#">Mimes</a></li>

								</ul></li>
							<li><a href="#">Musical acts</a>
								<ul class="sub_menu">
									<li><a href="#">Taiko drummers</a></li>
									<li><a href="#">Modern Chinese Orchestra</a></li>
									<li><a href="#">The String Trio</a></li>
								</ul></li>
							<li><a href="#">Dances</a>
								<ul class="sub_menu">
									<li><a href="#">Modern ribbon dancers</a></li>
									<li><a href="#">Chinease ribbon dancers</a></li>
									<li><a href="#">Samba dancers </a></li>
									<li><a href="#">Contemp /jazz dance</a></li>
									<li><a href="#">Hawaiian dancers</a></li>
									<li><a href="#">Thousand hand bodhisattva</a></li>
								</ul></li>
							<li><a href="#">Illusion and magic</a>
								<ul class="sub_menu">
									<li><a href="#">Close up and magic</a></li>
								</ul></li>
							<li><a href="#">Variety shows</a>
								<ul class="sub_menu">
									<li><a href="#">Chen Lei Impresonator</a></li>
									<li><a href="#">Elvis Presley Impresonator</a></li>
									<li><a href="#">Tersa Teng Impresonator</a></li>
									<li><a href="#">Lady Gaga Impresonator</a></li>
									<li><a href="#">Fire dancers</a></li>
									<li><a href="#">The ballon cocoon</a></li>
									<li><a href="#">Cyber Freak</a></li>
									<li><a href="#">CAUTION ! Fire Show</a></li>

								</ul></li>
							<li><a href="#">Interactive attractions</a>
								<ul class="sub_menu">
									<li><a href="#">Photo booth</a></li>
									<li><a href="#">Airbrush /nail/glitter art</a></li>
									<li><a href="#">Face painting</a></li>
									<li><a href="#">Balloon sculptor</a></li>
									<li><a href="#">Henna aart</a></li>
									<li><a href="#">Popcorn and candyfloss</a></li>
									<li><a href="#">Calligraphy artist</a></li>
									<li><a href="#">Chinese fortune teller</a></li>

									<li><a href="#">Tarot card reader</a></li>
								</ul></li>
							<li class=""><a href="#">Lighted shows</a>
								<ul class="sub_menu">
									<li><a href="#">Lazer girl</a></li>
									<li><a href="#">Laser man</a></li>

									<li><a href="#">Laser mingler</a></li>
									<li><a href="#">Led dance & poi</a></li>
								</ul></li>
						</ul>


					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade mj_popupdesign" id="myModal1" tabindex="-1"
		role="dialog" aria-labelledby="myModalLabel1">
		<div class="modal-dialog" role="document">
			<div class="modal-content">

				<div class="modal-body">
					<div
						class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
						<div class="row">
							<div
								class="mj_pricingtable mj_bluetable mj_freelancer_form_wrapper">

								<h4 style="color: #2bacc1; font-size: 20px">Client Signup</h4>
								<br>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<a class="btn btn-block btn-social btn-facebook"
											style="color: white !important"> <span class="fa fa-facebook"></span>
											Sign in with Facebook
										</a>
									</div>
									<div class="col-md-2"></div>
								</div>


								<div class="row">
									<div class="col-md-2"></div>
									<div class="success col-md-8 "
										style="font--size: 15px !important"></div>
									<div class="col-md-4"></div>
								</div>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8  alert alert-danger" id="error_alert">

										<strong class="error"> </strong>
									</div>
								</div>
								<form>
									<div class="mj_freelancer_form">
										<div id="form_part">
											<div class="form-group ">
												<input type="email" placeholder="Email" id="signup_email"
													class="form-control">
											</div>
											<div class="form-group">
												<input type="password" placeholder="Password"
													id="signup_password" class="form-control">
											</div>




											<div class="form-group mj_toppadder20">
												<div class="mj_checkbox">
													<input type="checkbox" value="1" id="check3"
														name="checkbox"> <label for="check3"></label>
												</div>
												<span> I have read, understand and agree to the Talent
													Portal Terms of Service, including the <a href="#">User
														Agreement</a> and <a href="#">Privacy Policy</a>.
												</span>
											</div>

											<div class="mj_pricing_footer"
												style="background-color: #fff !important">
												<button class="btn btn-success" id="signup" disabled>Sign Up</button>
											</div>
										</div>
									</div>
								</form>

								<button type="button" class="close" data-dismiss="modal"
									aria-label="Close">
									<img
										src="<?php echo base_url();?>theme_assets/images/close.png"
										alt="">
								</button>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>