<style>
.title {
	font-family: Brandon Grostesque;
	font-size: 39px;
}
</style>
<link href="<?php echo base_url();?>css/style1.css" rel="stylesheet"
	type="text/css" />
<section id="service_provider">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Your Perfect Choice</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div style="background-color: white; text-align: left">
	<div class="container" >
	<div class="row mj_toppadder20" >
			<div class="mj_articleslider mj_bottompadder50">
				<div class="col-md-1"></div>
				<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
				<div class="row">
				<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/emcee.jpg"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Emcees</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Emcees">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
				<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Entertainers.JPG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Entertainers</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/entertainer">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Photographers.JPG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Photographers</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Photographers">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					
					</div>
					<br>
					
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Event Prof.PNG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Event Professionals</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Event Professionals">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Venue.PNG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Venues</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Venues">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					<div class="col-sm-4">
					<div class="">
						<div class="">

							<img src="<?php echo base_url();?>theme_assets/images/Catering.PNG"
								style="height: 250px; width: 250px" class="img-circle"
								alt="">
							<div class="mj_feature_product_btn">
								<h5 style="color: #fff !important; font-size: 20px !important;text-align:center">Catering</h5>
							</div>
							<div class="mj_feature_product_overlay">

								<div class="mj_feature_product_btn">
									<a href="<?php echo base_url();?>home/search/Catering">view all</a>
								</div>
							</div>
						</div>

					</div>
					</div>
					
					
					
					
					
					
					
					
				</div>
				<div class="col-md-1"></div>
					
				
					
					
					
					
				</div>
				
				
				
				
			</div>
		</div>
		
	</div>
	</div>
	</section>