<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>theme_assets/css/jquery.datetimepicker.css" />
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}

.fa {
	font: normal normal normal 20px/1 FontAwesome;
	font-size: large;
}

.multiple{
font-size: 2rem;
    vertical-align: middle;
    text-align: center;
    font-weight: 700;
    max-width: 20px;
}
</style>

<script type="text/javascript">
function add_profilepic(id)
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}



/*    */
$(document).ready(function(){

	$('#event_list').on('click', '.cancel', function(){
		event.preventDefault();
		var r = confirm("Are you sure to delete this user?");
		if (r == true){
			$(this).closest('tr').hide();
			var user_id=$(this).attr('id');

			var dataString='id='+user_id;
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>clients/event_cancel/",
				data:dataString  ,
				success: function(data){

					alert("Sucessfully Deleted");
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
		}

	});



	//Cancel the talents from event 

    $(".cancel_talent").click(function(){

          var val=$(this).attr('id');

          var val_array=val.split('_');
          var talents_id=val_array[0];
          var event_id=val_array[1];     
          
          var r = confirm("Are you sure to remove this talent?");
  		  if (r == true){
  		

  			var dataString='talents_id='+talents_id+'&event_id='+event_id;
  			$.ajax({
  				type: "post",
  				url:"<?php echo base_url(); ?>clients/talents_cancel/",
  				data:dataString  ,
  				success: function(data){
                   if(data ==1){
  					alert("Sucessfully Cancelled");
  					location.reload(true);
                   }else{
                   alert("Try Again later !!!");
                   }
  				},
  				error: function(jqXHR, textStatus) {
  					alert( "Request failed: " + jqXHR );
  				}
  			});
  		}

        });	

});





	</script>


<section id="plan_event">
	<div class="" style="background-color: rgba(142, 230, 203, 1);">
		<div class="container">
			<div class="row">
				<div
					class="col-lg-8 col-md-8 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-0 col-xs-offset-0">
					<div class="mj_addsection ">
						<h3 class="title">
							<span>Event Summary View!</span>
						</h3>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="" style="background-color: white !important; text-align: left;margin-bottom:80px"> 
    <div class="container" >
	<div class="row mj_toppadder20">
		
		<div class="col-md-1"></div>
		<div class="col-md-10">
                               
								
                                 
                                    <table class="table table-striped"
									id="event_summary">
                                  
                                        <tr>
                                         <?php 
                                        if(file_exists("event_images/client$id/event".$event_details->id."pic")){
                                        	$fileName = "client$id/event".$event_details->id."pic";
                                        }
                                        	else{
                                        		$fileName = "events.png";
                                        	}
                                        		?>
										<td><a
											href="<?php echo base_url();?>clients/event_edit/<?php echo $event_details->id;?>"><i
												style="width: 50px; height: 50px; color: brown"
												class="fa fa-edit edit"></i></a></td>
										
										<td>
											<h4  style="color:#c33f45">
												<?php echo $event_details->event_title;?>
											</h4>
											<h5>
												<b style="color:#c33f45">Start:</b> <?php echo $event_details->event_start;?></h5>
											<h5>
												<b  style="color:#c33f45">End:</b>  <?php echo $event_details->event_end;?></h5>
												<h5>
												<b  style="color:#c33f45">Type:</b>  <?php echo $event_details->event_type;?></h5>
												
												
												<h5>
												<b  style="color:#c33f45">No.Of.Pax:</b>  <?php echo $event_details->pax;?></h5>
												
												
											<h5><?php echo $event_details->description;?></h5> <br> 	
											<h4>
												<b  style="color:#c33f45">Budget:</b> <span  style="color:#c33f45">S$<?php echo $event_details->budget;?></span></h4>
											<br>
											<p></p>


										</td>
										<td><i class="fa fa-map-marker"></i>
											<p><?php echo $event_details->venue;?></p></td>
										<td><span class="label label-default">Pending (<?php echo $event_details->pending;?>)</span> <span
											class="label label-success">Accepted (<?php echo $event_details->accepted;?>)</span> <span
											class="label label-danger">Rejected(<?php echo $event_details->rejected;?>)</span> <br> <br> <a
											href="<?php echo base_url();?>home/search"
											class="mj_btn mj_greenbtn">Hire Talents</a> <a href="#"
											class="mj_btn mj_orangebtn cancel"
											id="<?php echo $event_details->id;?>">Cancel</a></td>

									</tr>
                                     
                                  
                                    </table>
                                    
                                  

							</div>

						</div>

					</div>

<div class="container" >
	<div class="row mj_toppadder20">


	<div class="tab-content" style="background-color: white;">

						<div class="panel-heading"
							style="background-color: #bf1e2d; color: white">
							<h4><b><i>Talents List</i></b></h4>
						</div>

						<div class="panel-body">




							<div class="tab-content"></div>
						</div>

						<div role="tabpanel" class="tab-pane active" id="talents_list">
							<?php foreach($talents_detail as $row){?>
							
							  
                                        <?php 
                                        if(file_exists("talents_profilepic/talent".$row['talents_id']."pic")){
                                        	$fileName = "talent".$row['talents_id']."pic";
                                        }
                                        	else{
                                        		$fileName = "default_user.png";
                                        	}
                                        		
                                        
                                        
                                        
                                        ?>
                               
                              <div class="row">

												<div class="col-sm-2">

													<div class="form-group">&nbsp;&nbsp;&nbsp;
													
													<img src="<?php echo base_url();?>talents_profilepic/<?php echo $fileName;?>"
												class="img-circle" style="border-radius:80px !important" alt="" width="100" height="90"> 
											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											<div style="text-align:center ">
											<?php if($row['status']==0){?>
											<span
											class="label label-info">Pending </span>
											<?php }else if($row['status']==1){?>
											<span
											class="label label-success">Accepted </span>
											<?php }else if($row['status']==2){?>
											<span
											class="label label-danger">Rejected by Client </span>
											<?php }else if($row['status']==3){?>
											<span
											class="label label-danger">Declined </span>
											<?php }?>
											</div>
                                                     </div>


												</div>

												<div class="col-md-10">
													<div class="form-group" >
														<label><?php echo $row['talent_name'];?> (<?php echo $row['cat_name'];?>)</label>
														<br>
														<div class="row">
														<div class="col-sm-1">
													<span style="font-size:24px;width:50px !important"><img src="<?php echo base_url();?>theme_assets/images/dollar.png" width="60" height="70">
													</span></div><div class="col-sm-2">
												 <span style="font-size:12px;align:center;font-weight:bold"> &nbsp;&nbsp;SGD</span>	<br><span style="font-size:30px;color:#c33f45;font-weight:bold"><?php echo $row['hr_price'];?> </span>  <br><span style="font-size:12px;font-weight:bold"> Per Hour</span>
													
													
													</div>
													<div class="col-sm-1 multiple" style="font-size:48px" >X</div>
													
													<div class="col-sm-3">&nbsp;&nbsp;&nbsp;&nbsp;<img src="<?php echo base_url();?>theme_assets/images/clock.png" width="60" height="70"><span style="font-size:20px;color:#c33f45;font-weight:bold"> &nbsp;&nbsp;<?php echo $row['total_hours'];?>Hrs</span>	
													
													</div>
													<div class="col-sm-1 " style="font-size:48px" >=</div><div class="col-sm-1">
													<span style="font-size:24px;width:50px !important"><img src="<?php echo base_url();?>theme_assets/images/dollar.png" width="60" height="70">
													</span></div>
													<div class="col-sm-2"><span style="margin :0 auto;align:center;font-weight:bold"> &nbsp;&nbsp; SGD</span>	<br><span style="font-size:30px;color:#c33f45;font-weight:bold"> &nbsp;<?php echo $row['talents_cost'];?> </span>  <br><span style="font-weight:bold;margin:0 "> &nbsp;&nbsp; Total</span>

													
													</div>
													</div>
													</div>
												</div>


											</div>
											<?php if( $row['status']==0 || $row['status']==1){?>
											<button class="btn  btn-danger btn-xs pull-right cancel_talent" id="<?php echo $row['talents_id'];?>_<?php echo $row['event_id'];?>" >Cancel Talent</button>
											<?php }?>
											<br>
											<hr style="border-top: 1px solid #111111;">
                                 <?php }?>
                                    
                                  

						

						</div>

					</div>
				



				</div>
			</div>


		</div>
	


<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Person Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="pid" />
					<div class="form-body">
						<div id="photo_upload">
							<div class="mbot error-message-highlight hidden"
								id="progress_report_status"
								style="font-size: 12px; text-transform: none;"></div>
							<label for="profile_photo_upload">Upload Picture</label>
							<div class="">

								<!-- Conditional comments are dropped from IE post IE 10 -->
								<input id="profile_photo_upload" name="photo" type="file"
									class="file-up">

							</div>

							<div
								style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
								JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
								in height and width
							</div>
							<div class="upload_status"></div>

							<div id="progress_report" style="">
								<div id="progress_report_name"></div>

								<div id="progress_report_bar_container"
									style="height: 5px; margin-top: 10px">
									<div id="progress_report_bar"
										style="background-color: #cb202d; width: 0; height: 100%;"></div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()"
					class="btn btn-primary">Upload</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="<?php echo base_url();?>theme_assets/js/jquery.js"></script>
<script
	src="<?php echo base_url();?>theme_assets/js/jquery.datetimepicker.full.js"></script>
<script>



$('.some_class').datetimepicker();




</script>