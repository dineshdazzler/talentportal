<link rel="stylesheet" type="text/css"
	href="<?php echo base_url();?>theme_assets/css/jquery.datetimepicker.css" />
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}

.fa {
	font: normal normal normal 20px/1 FontAwesome;
	font-size: large;
}
</style>

<script type="text/javascript">
function add_profilepic(id)
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}



/*    General info */
$(document).ready(function(){
	
	$("#error_edit").hide();
	$("#general_info").click(function(){
		var t_id=$("#t_id").val();
		var eventtitle=$("#eventtitle").val();
		var eventtype=$("#eventtype").val();
		var budget=$("#budget").val();
		var pax=$("#pax").val();
		var attire=$("#attire").val();
		var venue=$("#venue").val();
		var description=$("#description").val();
		var eventstart=$("#eventstart").val();
		var eventend=$("#eventend").val();

        if(eventtitle == '' || eventtype == '' || budget == '' || pax =='' || attire =='' || venue == '' || description =='' || eventstart == '' || eventend ==''){

        	if(eventtitle==""){
				$(".vali_eventtitle").addClass("has-error");
			}
			if(eventtype==""){
				$(".vali_eventtype").addClass("has-error");
			}
			if(pax==""){
				$(".vali_pax").addClass("has-error");
			}
			if(budget==""){
				$(".vali_budget").addClass("has-error");
			}
			if(pax==""){
				$(".vali_pax").addClass("has-error");
			}
			if(description==""){
				$(".vali_description").addClass("has-error");
			}
			if(venue==""){
				$(".vali_venue").addClass("has-error");
			}
			if(attire==""){
				$(".vali_attire").addClass("has-error");
			}
			if(eventstart==""){
				$(".vali_eventstart").addClass("has-error");
			}
			if(eventend==""){
				$(".vali_eventend").addClass("has-error");
			}

			 
		}else{

			var dataString='eventtitle='+eventtitle+'&eventtype='+eventtype+'&budget='+budget+'&pax='+pax+'&attire='+attire+'&t_id='+t_id+'&venue='+venue+'&description='+description+'&eventstart='+eventstart+'&eventend='+eventend;
			 alert(dataString);
			$.ajax({
				type: "POST",
				url:"<?php echo base_url(); ?>clients/event_insert",
				data:dataString,
				success: function(data){
					
					
					
					alert("success");	 
						 
					
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});



		}


	});
	$('#event_list').on('click', '.cancel', function(){
		event.preventDefault();
		var r = confirm("Are you sure to delete this user?");
		if (r == true){
			$(this).closest('tr').hide();
			var user_id=$(this).attr('id');

			var dataString='id='+user_id;
			$.ajax({
				type: "post",
				url:"<?php echo base_url(); ?>clients/event_cancel/",
				data:dataString  ,
				success: function(data){

					alert("Sucessfully Deleted");
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});
		}

	});

});





	</script>

<div class="mj_pagetitle2">
	<div class="mj_pagetitleimg">
		<img
			src="<?php echo base_url();?>theme_assets/images/talents_banner.jpg"
			alt="">

		<div class="mj_mainheading_overlay"></div>


	</div>
	<div class="mj_pagetitle_inner"
		style="margin-bottom: 140px !important; color: #c11010; text-align: center">
		<br>
		<h1 align="center" style="color: #fecb16; weight: 700px">
			<i>Past Event !</i>
		</h1>
		<br> <a href="<?php echo base_url();?>clients/event_create"
			class="btn btn-danger" data-text="Create Event"><span><i
				class="fa fa-plus-circle" aria-hidden="true"></i> Create Event</span></a>
	</div>

</div>
<div class="mj_lightgraytbg mj_bottompadder80"
	style="background: rgba(128, 128, 127, 0.1);">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
				<div
					class="mj_social_media_section mj_candidatepage_media mj_toppadder10 ">


				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3" id="tabs" style="radius: 6px">
				<ul class="nav nav-pills nav-stacked"
					style="background-color: white; color: black!     imporatant;">
					<li ><a
						href="<?php echo base_url();?>clients/clients_dashboard"><i
							class="fa fa-user"></i>&nbsp; Up Coming Events<i
							class="fa fa-angle-right" style="float: right"></i> </a></li>
					<li><a href="#"><i class="fa fa-signal"></i>&nbsp;Schedule<i
							class="fa fa-angle-right" style="float: right"></i> </a></li>
					<li><a href="#"><i class="fa fa-eye"></i>&nbsp; Favourite Talents<i
							class="fa fa-angle-right" style="float: right"></i> </a></li>
					<li><a href="#"><i class="fa fa-signal"></i>&nbsp;Report<i
							class="fa fa-angle-right" style="float: right"></i> </a></li>
					<li class="active"><a href="<?php echo base_url();?>clients/past_event"><i class="fa fa-eye"></i>&nbsp; Past Events<i
							class="fa fa-angle-right" style="float: right"></i> </a></li>
				</ul>

			</div>
			<div class="col-md-9">
				<div class="mj_tabs mj_bottompadder50  ">
					<!-- Nav tabs 
                        <div class="mj_social_media_section ">
                         <ul class="nav nav-tabs mj_joblist" role="tablist">
                            <li role="presentation" ><a href="#recentjobs" aria-controls="recentjobs">Event Summary</a>
                            </li>
                            <li><a href="<?php echo base_url(); ?>clients/event_create" class="mj_mainbtn mj_btnblack" data-text="Create Event"><span>Create Event</span></a>
                            </li>
                        </ul>
                        </div> -->
					<div class="tab-content" style="background-color: white;">

						<div class="panel-heading"
							style="background-color: #76CE32; color: white">
							<h5>Past Events</h5>
						</div>

						<div class="panel-body">




							<div class="tab-content"></div>
						</div>

						<div role="tabpanel" class="tab-pane active" id="recentjobs">
							<div class=" ">
                                <?php
																																$count = count ( $result );
																																if ($count == 0) {
																																	?>
                                <div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8">
										<h2 style="color: red; font-weight: bold">Oops ! You dont have
											any  Past Events !!!!</h2>
									</div>
									<div class="col-md-2"></div>



								</div>
								<div class="row">
									<div class="col-md-2"></div>
									<div class="col-md-8" align="center">
										<a href="<?php echo base_url();?>clients/event_create"
											class="btn btn-danger" data-text="Create Event"><span><i
												class="fa fa-plus-circle" aria-hidden="true"></i> Create
												Event</span></a>
									</div>
									<div class="col-md-2"></div>



								</div>
                                  <?php }?>
                                    <table class="table table-striped"
									id="event_list">
                                    <?php foreach($result as $selected): ?>
                                        <tr>
                                        
                                        <?php 
                                        if(file_exists("event_images/client$id/event".$selected->id."pic")){
                                        	$fileName = "client$id/event".$selected->id."pic";
                                        }
                                        	else{
                                        		$fileName = "events.png";
                                        	}
                                        		
                                        
                                        
                                        
                                        ?>
										<td><a
											href="<?php echo base_url();?>clients/event_edit/<?php echo $selected->id;?>"><i
												style="width: 50px; height: 50px; color: brown"
												class="fa fa-edit edit"></i></a></td>
										<td><a href="#"><img src="<?php echo base_url();?>event_images/
										<?php echo $fileName; ?>"
										
										
												class="img-responsive" alt="" width="200" height="150"> </a></td>
										<td>
											<h4>
												<a href="<?php echo base_url();?>clients/event_summary/<?php echo $selected->id;?>"><?php echo $selected->event_title;?></a>
											</h4>
											<h5>
												<b>Start:</b> <?php echo $selected->event_start;?></h5>
											<h5>
												<b>End:</b>  <?php echo $selected->event_end;?></h5>
											<h5><?php echo $selected->description;?></h5>
											<br> <span>$<?php echo $selected->budget;?></span> <br>
											<p></p>


										</td>
										<td><i class="fa fa-map-marker"></i>
											<p><?php echo $selected->venue;?></p></td>
										<td>
										
										<span class="label label-default">Pending (<?php echo $selected->pending;?>)</span> 
										 <span class="label label-success">Accepted (<?php echo $selected->accepted;?>)</span>
										 <span class="label label-danger">Rejected(<?php echo $selected->rejected;?>)</span> <br>
										 
										<br> <a href="<?php echo base_url();?>clients/event_summary/<?php echo $selected->id;?>" class="mj_btn mj_greenbtn">More View</a>
											<a href="#" class="mj_btn mj_orangebtn cancel"
											id="<?php echo $selected->id;?>">Cancel</a></td>

									</tr>
                                     
                                  <?php endforeach; ?> 
                                    </table>

							</div>

						</div>

					</div>





					<!-- --Edit restaurant div  -->



				</div>
			</div>


		</div>
	</div>
</div>



<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Person Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="" name="pid" />
					<div class="form-body">
						<div id="photo_upload">
							<div class="mbot error-message-highlight hidden"
								id="progress_report_status"
								style="font-size: 12px; text-transform: none;"></div>
							<label for="profile_photo_upload">Upload Picture</label>
							<div class="">

								<!-- Conditional comments are dropped from IE post IE 10 -->
								<input id="profile_photo_upload" name="photo" type="file"
									class="file-up">

							</div>

							<div
								style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
								JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
								in height and width
							</div>
							<div class="upload_status"></div>

							<div id="progress_report" style="">
								<div id="progress_report_name"></div>

								<div id="progress_report_bar_container"
									style="height: 5px; margin-top: 10px">
									<div id="progress_report_bar"
										style="background-color: #cb202d; width: 0; height: 100%;"></div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()"
					class="btn btn-primary">Upload</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->
<script src="<?php echo base_url();?>theme_assets/js/jquery.js"></script>
<script
	src="<?php echo base_url();?>theme_assets/js/jquery.datetimepicker.full.js"></script>
<script>



$('.some_class').datetimepicker();




</script>