<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}
</style>

<script type="text/javascript">
function add_profilepic(id)
{

	$('#modal_form').modal('show'); // show bootstrap modal when complete loaded
	$('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

   
        url = "<?php echo site_url('clients/ajax_add')?>";
   

    // ajax adding data to database
<?php $picname="clients".$clients_info->client_id."pic";?>
    var formData = new FormData($('#form')[0]);
    
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        { 


           
            if(data.status) //if success close modal and reload ajax table
            {location.reload(true);
                alert(data.msg);
                $('#modal_form').modal('hide');
                
               
            }
            else{
            	 alert(data.msg);
                }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //  alert(textStatus+""+errorThrown+""+jqXHR);
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


/*    General info */
$(document).ready(function(){
	$("#error_edit").hide();
	$("#general_info").click(function(){
		var t_id=$("#t_id").val();
		var firstname=$("#fname").val();
		var lastname=$("#lname").val();
		var contact_number=$("#contact_number").val();
		var country=$("#country").val();
		var organization=$("#organization").val();
		var email=$("#email").val();
		var status=0;
		var update=1;


		var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

		if(firstname == '' || lastname == '' || contact_number == '' || country == '' || organization =='' || email =='' ){

			if(firstname==""){
				$(".vali_fname").addClass("has-error");
			}
			if(lastname==""){
				$(".vali_lname").addClass("has-error");
			}
			if(contact_number==""){
				$(".vali_num").addClass("has-error");
			}
			if(country==""){
				$(".vali_country").addClass("has-error");
			}
			if(email==""){
				$(".vali_email").addClass("has-error");
			}
			if(organization==""){
				$(".vali_orgtype").addClass("has-error");
			}
				


		}else if(!emailFilter.test(email)){

			$('.email_error').html("Please enter valid email address");


		}else{

			var dataString='fname='+firstname+'&lname='+lastname+'&contact_number='+contact_number+'&country='+country+'&email='+email+'&t_id='+t_id+'&organization='+organization+'&status='+status+'&update='+update;
				
			$.ajax({
				type: "POST",
				url:"<?php echo base_url(); ?>clients/generalinfo_update",
				data:dataString,
				success: function(data){
					var dataString1='t_id='+t_id+'&update='+update;
					alert(dataString1);
					$.ajax({
						type: "POST",
						url:"<?php echo base_url(); ?>clients/user_update",
						data:dataString1,
						success: function(data){
								
							alert("success");
						
								
								
						},

					});
							
							
							
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});



		}


	});

	$("#pass").click(function(){
		var client_id=$("#client_id").val();
		var newpass=$("#newpass").val();
		var conpass=$("#conpass").val();

		if(newpass == '' || conpass == ''){

			if(newpass==""){
				$(".vali_newpass").addClass("has-error");
			}
			if(conpass==""){
				$(".vali_conpass").addClass("has-error");
			}
			


		}else if(newpass != conpass){

			$(".vali_newpass").addClass("has-error");
			$(".vali_conpass").addClass("has-error");


		}else{

			var dataString='newpass='+newpass+'&client_id='+client_id;
				alert(dataString);
			$.ajax({
				type: "POST",
				url:"<?php echo base_url(); ?>clients/password_update",
				data:dataString,
				success: function(data){
				alert("password Changed");			
							
				},
				error: function(jqXHR, textStatus) {
					alert( "Request failed: " + jqXHR );
				}
			});



		}


	});
});





	</script>

<div class="mj_pagetitle2">
	
	<div class="mj_pagetitle_inner">
		<div class="container">
			<div class="row">
				
			</div>
		</div>
	</div>
</div>

<div class="mj_lightgraytbg mj_bottompadder80"
	style="background-color: white">
	<div class="container">
		<div class="row">
			<div
				class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-3 col-md-offset-3">
				<div
					class="mj_social_media_section mj_candidatepage_media mj_toppadder40 mj_bottompadder40">


				</div>
			</div>
		</div>
		<div class="row" style="margin-bottom:80px">

			<div class="col-md-12">
				<div class="panel panel-default">
					<div class="tab-content">
						<div id="edit_profile" class="tab-pane active">
							<div class="panel-heading"
								style="background-color: #76CE32; color: white">
								<h5>General Information</h5>
							</div>
							<div class="panel-body">

								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#home">Details</a></li>
									<li><a data-toggle="tab" href="#menu1">Password</a></li>

								</ul>


								<div class="tab-content">
									<div id="home" class="tab-pane fade in active">
										<div class="row"
											style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
											<div class="col-md2  ">
<?php foreach($settings as $selected): ?>
												<form>
													<input type="hidden" name="tid" id="t_id"
														value="<?php echo $id;?>">

													<fieldset>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group vali_fname">
																	<label for="particular">First name *</label> <input
																		type="text" name="name" placeholder="First Name"
																		id="fname" value="<?php echo $selected->firstname;?>"
																		class="form-control"> <span class="text-danger"></span>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group vali_lname">
																	<label for="particular">Last name *</label> <input
																		type="text" name="name" placeholder="Last Name"
																		id="lname" value="<?php echo $selected->lastname;?>"
																		class="form-control"> <span class="text-danger"></span>
																</div>
															</div>

														</div>
														<div class="row">
															<div class="col-sm-6">
																<div class="form-group vali_orgtype">
																	<label for="Loantype">Type of Organization *</label><br>
																	<div>
																		<select required id="organization" name="country"
																			class="Loantype country form-control">
																			<option class="form-control" value="">Select Your
																				Organitation</option>

<?php if($selected->organization=="Individual"){?>
<option class="form-control" value="Individual" selected>Individual</option>
																			<option class="form-control" value="Company">Company</option>

<?php  }?>
<?php if($selected->organization=="Company"){?>
<option class="-control" value="Individual">Individual</option>
																			<option class="form-control" value="Company" selected>Company</option>

<?php  }?>
																			
																		</select>
																	</div>

																	<span class="text-danger"></span>
																</div>
															</div>
															<div class="col-sm-6">
																<div class="form-group vali_email">
																	<label for="particular">Contact email *</label> <input
																		type="email" name="email"
																		placeholder="Enter your mail id" id="email"
																		value="<?php echo $selected->email;?>"
																		class="form-control"> <span class="email_error"
																		style="color: red;"></span>
																</div>
															</div>
														</div>
														<div class="row">

															<div class="col-sm-6">
																<div class="form-group vali_num">
																	<label for="particular">Contact Number *</label> <input
																		type="text" name="phone" placeholder="Contact Number"
																		pattern="[0-9]*" title="Phone number"
																		id="contact_number"
																		value="<?php echo $selected->phone;?>"
																		class="form-control"> <span class="text-danger"></span>
																</div>
															</div>

															<div class="col-sm-6">
																<div class="form-group vali_country">
																	<label for="Loantype">Country *</label><br>
																	<div>
																		<select required id="country" name="country"
																			class="Loantype country form-control">
																			<option class="form-control" value="">Select Your
																				country</option>
<?php if($selected->country=="Singapore"){?>

																			<option class="form-control" value="Singapore"
																				selected>Singapore</option>
																			<option class="form-control" value="Malaysia">Malaysia</option>
																			<option class="form-control" value="Srilanka">Srilanka</option>
																			<option class="form-control" value="India">India</option>
<?php  }?>
<?php if($selected->country=="Malaysia"){?>

																			<option class="form-control" value="Singapore">Singapore</option>
																			<option class="form-control" value="Malaysia"
																				selected>Malaysia</option>
																			<option class="form-control" value="Srilanka">Srilanka</option>
																			<option class="form-control" value="India">India</option>
<?php  }?>
<?php if($selected->country=="Srilanka"){?>

																			<option class="form-control" value="Singapore">Singapore</option>
																			<option class="form-control" value="Malaysia">Malaysia</option>
																			<option class="form-control" value="Srilanka"
																				selected>Srilanka</option>
																			<option class="form-control" value="India">India</option>
<?php  }?>
<?php if($selected->country=="India"){?>

																			<option class="form-control" value="Singapore">Singapore</option>
																			<option class="form-control" value="Malaysia">Malaysia</option>
																			<option class="form-control" value="Srilanka">Srilanka</option>
																			<option class="form-control" value="India" selected>India</option>
<?php  }?>
																		</select>
																	</div>

																	<span class="text-danger"></span>
																</div>
															</div>

														</div> <div class="col-sm-4"><div
					class="col-sm-3 ih-item circle effect17">
					<div class="mj_mainheading">
						<div class="row">
							<div class="col-sm-3">
								<a href="javascript:void(0)" onclick="add_profilepic(1)">
									<div class="mj_joblogo img">
										<?php
									
									$prinimage = '<img id="myimage"
		src="' . base_url () . '/theme_assets/images/default_user.png"
		class="img-circle" alt="Cinque Terre" width="200" height="200">
    	';
									if ($clients_info->id != '') {
										
										$files = glob ( 'clients_profilepic/*' ); // get all file names
										foreach ( $files as $file ) {
											if (is_file ( $file )) {
												$picname = "clients" . $clients_info->client_id . "pic";
												
												$photos_from_direc = explode ( '/', $file );
												$photoname = explode ( '.', $photos_from_direc [1] );
												if ($photoname [0] == $picname) {
													$prinimage = '<img id="myimage"
		src=" ' . base_url () . 'clients_profilepic/' . $picname . '"
		class="img-circle" alt="Client Profile " width="200" height="200">
    	';
												}
												
												// unlink($file);
											} // delete file
										}
									}
									
									echo $prinimage;
									?>
									</div>
								</a>
							</div>

						</div>
					</div>
				</div></div>



														<div class="form-group">
															<br> <input type="button" name="update" value="Save"
																id="general_info" class="btn btn-success">
														</div>
													</fieldset>
												</form>
												<center>
													<span class="text-success"></span>
												</center>
												<span class="text-danger"></span>
											</div>
										</div>
									</div>
									<div id="menu1" class="tab-pane fade">
										<form>
										<input type="hidden" name="tid" id="client_id"
														value="<?php echo $id;?>">
											<div class="row"
												style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
												<div class="col-sm-6">
													<div class="form-group vali_newpass">
														<label>New Password <span></span>
														</label> <input type="password" id="newpass"
															class="form-control" placeholder="New Password">
													</div>
												</div>
												<div class="col-sm-6">
													<div class="form-group vali_conpass">
														<label>Confirm Password <span></span>
														</label> <input type="password" id="conpass"
															class="form-control" placeholder="Confirm password">
													</div>
												</div>
											</div>
											<div class="row"
												style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
												<div class="col-sm-12">

													<center>
														<input type="button" name="pass" value="Password Change"
															id="pass" class="btn btn-success">
													</center>
												</div>

											</div>
										</form>
									</div>
								</div>


							</div>
						</div>
					</div>

					<!-- --Edit restaurant div  -->


				</div>
			</div>
		</div>


	</div>
</div>

<?php endforeach; ?>


<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Person Form</h3>
			</div>
			<div class="modal-body form">
				<form id="form" class="form-horizontal" enctype="multipart/form-data">
					<input type="hidden" value="<?php echo $id;?>" name="tid" />
					<div class="form-body">
						<div id="photo_upload">
							<div class="mbot error-message-highlight hidden"
								id="progress_report_status"
								style="font-size: 12px; text-transform: none;"></div>
							<label for="profile_photo_upload">Upload Picture</label>
							<div class="">

								<!-- Conditional comments are dropped from IE post IE 10 -->
								<input id="profile_photo_upload" name="photo" type="file"
									class="file-up">

							</div>

							<div
								style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
								JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
								in height and width
							</div>
							<div class="upload_status"></div>

							<div id="progress_report" style="">
								<div id="progress_report_name"></div>

								<div id="progress_report_bar_container"
									style="height: 5px; margin-top: 10px">
									<div id="progress_report_bar"
										style="background-color: #cb202d; width: 0; height: 100%;"></div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()"
					class="btn btn-primary">Upload</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->