<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
function add_profilepic()
{
	
	  $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
      $('.modal-title').text('Edit profile picture'); // Set title to Bootstrap modal title
}
function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

   
        url = "<?php echo site_url('talents/ajax_add')?>";
   

    // ajax adding data to database
<?php $picname="talents".$talents_info->talents_id."pic";?>
    var formData = new FormData($('#form')[0]);
    
    $.ajax({
        url : url,
        type: "POST",
        data: formData,
        contentType: false,
        processData: false,
        dataType: "JSON",
        success: function(data)
        { 


           
            if(data.status) //if success close modal and reload ajax table
            {location.reload(true);
                alert(data.msg);
                $('#modal_form').modal('hide');
                
               
            }
            else{
            	 alert(data.msg);
                }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          //  alert(textStatus+""+errorThrown+""+jqXHR);
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
}


/*    General info */
 $(document).ready(function(){
	 $("#error_edit").hide();
	 
    $("#general_info").click(function(){
        alert("text");
        var t_id=$("#t_id").val();
        var name=$("#name").val();
        var contact_number=$("#contact_number").val();
        var act_type=$("#act_type").val();
        var email=$("#email").val();
        var editor1=$("#ckeditor").val();
        var dob=$("#dob").val();
        var address=$("#address").val();
        var gender=$("input[name='gender']:checked").val();
		var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;

 

            	var dataString='name='+name+'&contact_number='+contact_number+'&act_type='+act_type+'&email='+email+'&editor1='+editor1+'&dob='+dob+'&gender='+gender+'&t_id='+t_id+'&address='+address;
    			
           	$.ajax({
            	type: "POST",
            	url:"<?php echo base_url(); ?>talents/generalinfo_insert",
            	data:dataString,
            	success: function(data){
                	
            		if(data=="success"){
            			 $('.sucess_content').addClass('alert alert-success').html("Informations successfully saved !!");
            	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
           		            $("#sucess_div").slideUp(500);
           		             }); 
            			
            		}else {
            			
            			 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
           	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
          		            $("#sucess_div").slideUp(500);
          		             });
            		}

            	},
                error: function(jqXHR, textStatus) {
                    alert( "Request failed: " + jqXHR );
                }
            	});

                

       


    });


    /*Bank details insert or update function*/
 $("#bank_submit").click(function(){
        
        var t1_id=$("#t1_id").val();
        var bank_name=$("#bank_name").val();
        var ac_type=$("#ac_type").val();
        var ac_number=$("#ac_number").val();
        var namein_bank=$("#namein_bank").val();
        alert(ac_type);

        if(bank_name == '' || ac_type == '' || ac_number == '' || namein_bank =='' ){

        	if(bank_name==""){
				$(".vali_bname").addClass("has-error");
			}
        	if(ac_type==""){
				$(".vali_actype").addClass("has-error");
			}
        	if(ac_number==""){
				$(".vali_acnumber").addClass("has-error");
			}
        	if(namein_bank==""){
				$(".vali_namein").addClass("has-error");
			}

        }else{

            	var dataString='bank_name='+bank_name+'&ac_type='+ac_type+'&ac_number='+ac_number+'&namein_bank='+namein_bank+'&t1_id='+t1_id;
    			
            	$.ajax({
            	type: "POST",
            	url:"<?php echo base_url(); ?>talents/bankinfo_insert",
            	data:dataString,
            	success: function(data){
            	$('#menu1').load('dashboard.php');
            		if(data=="success"){
            			 $('.sucess_content').addClass('alert alert-success').html("Bank details successfully saved !!");
           	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
          		            $("#sucess_div").slideUp(500);
          		             }); 
            			
            			
            		}else {
            			 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
              	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
             		            $("#sucess_div").slideUp(500);
             		             });
            		}

            	},
                error: function(jqXHR, textStatus) {
                    alert( "Request failed: " + jqXHR );
                }
            	});

                

        } 


    });

    /*Performance info*/

 
 $("#performance_info").click(function(){
        
        var t_id=$("#t_id").val();
        var requirement=$("#requirement").val();
        
      

        if(requirement == '' ){

        	if(requirement==""){
				$(".vali_requirement").addClass("has-error");
				$('.sucess_content').addClass('alert alert-danger').html("Please Enter requirements");
	   	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
	  		            $("#sucess_div").slideUp(500);
	  		             });
			}
        	

        }else{

            	var dataString='requirement='+requirement+'&t_id='+t_id;
    			
            	$.ajax({
            	type: "POST",
            	url:"<?php echo base_url(); ?>talents/performanceinfo_insert",
            	data:dataString,
            	 
            	success: function(data){
            		
            		if(data==1){
            			 $('.sucess_content').addClass('alert alert-success').html("Performance Information successfully saved !!");
              	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
             		            $("#sucess_div").slideUp(500);
             		             }); 
            			
            		}else {
            			 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
              	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
             		            $("#sucess_div").slideUp(500);
             		             });
            			
            		}

            	},
                error: function(jqXHR, textStatus) {
                    alert( "Request failed: " + jqXHR );
                }
            	});

                

        } 


    });
    
 
	
});





</script>

<section class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="panel panel-default">
					<div class="tab-content">
						<div id="edit_profile" class="tab-pane active">
							<div class="panel-heading"
								style="background-color: #76CE32; color: white">
								<h5>General Information</h5>
							</div>
							<div class="panel-body">

								<ul class="nav nav-tabs">
									<li class="active"><a data-toggle="tab" href="#home">General
											Info</a></li>
									<!--  <li><a data-toggle="tab" href="#menu1">Payment Gateway Details</a></li>-->
									<li><a data-toggle="tab" href="#menu2">Performance</a></li>
									<li><a
										href="<?php echo base_url();?>admin/talent_portedit/<?php echo $id;?>">Portfolio</a></li>
								</ul>


								<div class="tab-content">
									<div id="home" class="tab-pane fade in active">
										<div class="row"
											style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
											<div class="col-md2  ">

												<form>
													<input type="hidden" name="tid" id="t_id"
														value="<?php echo $id;?>">

													<fieldset>

														<div class="row">
															<div class="col-sm-6">
																<p>
																	<b>Artist Name</b>
																</p>
																<div class="input-group">

																	<span class="input-group-addon"> <i
																		class="material-icons">person</i>
																	</span>
																	<div class="form-line">
																		<input type="text" class="form-control date"
																			placeholder="Username" id="name"
																			value="<?php echo $talents_info->name;?>">
																	</div>
																</div>
															</div>
															<div class="col-sm-6">
																<p>
																	<b>Contact Number *</b>
																</p>
																<div class="input-group">

																	<span class="input-group-addon"> <i
																		class="material-icons">person</i>
																	</span>
																	<div class="form-line">
																		<input type="text" class="form-control date"
																			placeholder="Username" id="contact_number"
																			value="<?php echo $talents_info->phone_number;?>">
																	</div>
																</div>
															</div>


														</div>
														<div class="row">
															<div class="col-sm-6">

																<p>
																	<b>Act Type</b>
																</p>
																<select class="form-control show-tick" id="act_type"
																	name="country">
																	<option value="">Select Your act</option>

																			<?php
																			
																			foreach ( $talents_type as $talents ) {
																				
																				if ($talents ['id'] == $talents_info->act_type) {
																					?>
																		
																			<option value="<?php echo $talents['id'];?>" selected><?php echo $talents['talent'];?></option>
																			
																			<?php }else{?>
																			<option value="<?php echo $talents['id'];?>"><?php echo $talents['talent'];?></option>
																			
																			<?php }?>
																			<?php }?>
                                    </select>




															</div>


															<div class="col-sm-6">
																<p>
																	<b>Contact email *</b>
																</p>
																<div class="input-group">
																	<span class="input-group-addon"> <i
																		class="material-icons">email</i>
																	</span>
																	<div class="form-line">
																		<input type="text" class="form-control email"
																			placeholder="Ex: example@example.com" id="email"
																			value="<?php echo $talents_info->email;?>">
																	</div>
																</div>
															</div>
														</div>


														<div class="row clearfix">
															<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																<div class="card">

																	<div class="body">
																		<textarea id="ckeditor">
                                <?php echo $talents_info->service;?>
                                 </textarea>
																	</div>
																</div>
															</div>
														</div>
														<div class="row"></div>


														<div class="row">

															<div class="col-sm-6">

																<b>Birthday</b>
																<div class="input-group">
																	<span class="input-group-addon"> <i
																		class="material-icons">date_range</i>
																	</span>
																	<div class="form-line">
																		<input type="date" class="form-control date"
																			value="<?php echo $talents_info->dob;?>" id="dob"
																			placeholder="Ex: 30/07/2016">
																	</div>
																</div>
															</div>

															<div class="col-sm-6">

																<div class="form-group">
																	<input type="radio" name="gender" id="male"
																		class="with-gap"
																		<?php if($talents_info->gender=="male" || $talents_info->gender==""){ echo "checked";}?>>
																	<label for="male">Male</label> <input type="radio"
																		name="gender" id="female" class="with-gap"
																		<?php if($talents_info->gender=="female" ){ echo "checked";}?>>
																	<label for="female" class="m-l-20">Female</label>
																</div>
															</div>

														</div>

														<div class="row">

															<div class="col-sm-8">

																<div class="form-group form-float">
																	<div class="form-line">
																		<textarea name="description" cols="30" rows="5"
																			id="address" class="form-control no-resize" required></textarea>
																		<label class="form-label">Address</label>
																	</div>
																</div>
															</div>



														</div>




														<div class="form-group">
															<br> <input type="button" name="update" value="Save"
																id="general_info" data-position="top-right"
																class="btn btn-success">
														</div>

													</fieldset>
												</form>
												<center>
													<span class="text-success"></span>
												</center>
												<span class="text-danger"></span>
											</div>
										</div>
									</div>
									<div id="menu1" class="tab-pane fade">


										<div class="row"
											style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
											<form>
												<input type="hidden" value="<?php echo  $id;?>" id="t1_id">

												<div class="row">
													<div class="col-sm-6">
														<div class="form-group vali_bname">
															<label for="particular">Bank Name *</label> <input
																type="text" name="bank_name" placeholder="Bank name"
																id="bank_name"
																value="<?php echo $talents_info->bank_name;?>"
																class="form-control"> <span class="text-danger"></span>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group vali_actype">
															<label for="particular">Account Type *</label> <select
																id="ac_type" name="ac_type" class="ac_type form-control">
																<option class="form-control" value="">Select account
																	type</option>

																<option class="form-control" value="Current"
																	<?php if($talents_info->account_type =="Current"){ echo "selected";}?>>Current</option>
																<option class="form-control" value="Savings"
																	<?php if($talents_info->account_type =="Savings"){ echo "selected";}?>>Savings</option>
															</select> <span class="text-danger"></span>
														</div>
													</div>

												</div>

												<div class="row">
													<div class="col-sm-6">
														<div class="form-group vali_acnumber">
															<label for="particular">Account number *</label> <input
																type="number" name="ac_number"
																placeholder="Account number" pattern="[0-9]*"
																id="ac_number"
																value="<?php echo $talents_info->account_number;?>"
																class="form-control"> <span class="text-danger"></span>
														</div>
													</div>
													<div class="col-sm-6">
														<div class="form-group vali_namein">
															<label for="particular">Name in bank account</label> <input
																type="text" name="namein_bank"
																placeholder="Name in Bank Account" id="namein_bank"
																title="" required
																value="<?php echo $talents_info->namein_bank;?>"
																class="form-control"> <span class="text-danger"></span>
														</div>
													</div>

												</div>
												<br>
												<div id="mydiv"></div>



												<div>
													<input class="btn btn-success" type="button"
														id="bank_submit" value="Save">
												</div>

											</form>
										</div>


									</div>
									<div id="menu2" class="tab-pane fade">

										<div class="row"
											style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">


											<input type="hidden" value="89" name="pid">

											<div class="row">
												<div class="col-sm-6">

													<p>
														<b>Logistical Requirement *</b>
													</p>
													<div class="form-line">
														<input type="text" class="form-control "
															placeholder="Requirement" id="requirement"
															value="<?php echo $talents_info->logisitical;?>">
													</div>
												</div>


												<div class="col-sm-6"></div>
											</div>
											<br> <input name="per_info" data-position="top-right"
												id="performance_info" class="btn btn-success" type="button"
												value="Save">
										</div>


									</div>
									
								</div>
							</div>
						</div>

						<!-- --Edit restaurant div  -->


					</div>
				</div>
			</div>
		</div>

</section>
