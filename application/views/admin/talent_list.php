<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
$('#mainTable').on('click', '.delete', function(){
	event.preventDefault();
	var r = confirm("Are you sure to delete this user?");
	if (r == true){
		$(this).closest('tr').hide();
		var user_id=$(this).attr('id');

		var dataString='id='+user_id;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>admin/delete_talent/",
			data:dataString  ,
			success: function(data){

				alert("Sucessfully Deleted");
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});
$(".approve").click(function(){
	var id=$(this).attr('id');
	

	dataString='id='+id;
	
		$.ajax({

			type: "post",	
			url:"<?php echo base_url(); ?>admin/approve/ ",
			data:dataString  ,
			success: function(data){
				alert("Successfully Approved");
				
			},
			
		});

	

});
$(".decline").click(function(){
	var id=$(this).attr('id');
	

	dataString='id='+id;
	
		$.ajax({

			type: "post",	
			url:"<?php echo base_url(); ?>admin/decline/ ",
			data:dataString  ,
			success: function(data){
				alert("Successfully Approved");
				
			},
			
		});

	

});
});
</script>
<section class="content">
	<div class="container-fluid">

		<div class="row">
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header">
						<h2>TALENT LIST</h2>
						<ul class="header-dropdown m-r--5">
							<li class="dropdown"><a href="javascript:void(0);"
								class="dropdown-toggle" data-toggle="dropdown" role="button"
								aria-haspopup="true" aria-expanded="false"> <i
									class="material-icons">more_vert</i>
							</a>
								<ul class="dropdown-menu pull-right">
									<li><a href="javascript:void(0);">Action</a></li>
									<li><a href="javascript:void(0);">Another action</a></li>
									<li><a href="javascript:void(0);">Something else here</a></li>
								</ul></li>
						</ul>
					</div>
					<div class="body">
						<table id="mainTable" class="table table-striped">
							<thead>
								<tr>
									<th>S.No</th>
									<th>Name</th>
									<th>Email</th>
									<th>Gender</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
<?php $i = 0; foreach($talent_list as $selected): ?>
                <tr>

									<td><?php echo ++$i;?></td>
									<td class="nowrap"><?php echo $selected->name; ?></td>
									<td class="nowrap"><?php echo $selected->email; ?></td>
									<td class="nowrap"><?php echo $selected->gender; ?></td>
									<td><?php if($selected->status == 1){ ?><a id="<?php echo $selected->talents_id;?>"
										href=""
										class="btn btn-warning btn-sm "><i
											class="glyphicon glyphicon-thumbs-up icon-white "></i>Approved</a><?php } else{?>
											<a id="<?php echo $selected->talents_id;?>"
										href=""
										class="btn btn-success btn-sm approve"><i
											class="glyphicon glyphicon-ok icon-white approve"></i>Approve</a> <?php }?>	&nbsp;<a
										href="<?php echo base_url();?>admin/talent_edit/<?php echo $selected->talents_id;?>"
										id="<?php echo $selected->id;?>" class="btn btn-info btn-sm "><i
											class="glyphicon glyphicon-edit icon-white"></i>Edit</a> &nbsp; <a
										id="<?php echo $selected->id; ?>"
										class="btn btn-danger btn-sm delete"><i
											class="glyphicon glyphicon-trash icon-white"></i>Delete</a></td>

								</tr>
                
                <?php endforeach; ?> 
</tbody>

						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
