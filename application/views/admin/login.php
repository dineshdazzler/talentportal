<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Talent Portal</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?php echo base_url();?>admin_assets/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

      <!-- Waves Effect Css -->
    <link href="<?php echo base_url();?>admin_assets/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?php echo base_url();?>admin_assets/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Sweet Alert Css -->
    <link href="<?php echo base_url();?>admin_assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?php echo base_url();?>admin_assets/css/style.css" rel="stylesheet">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

     
   
    
</head>

<script type="text/javascript">
  
$(document).ready(function(){
	
	$("#error_alert").hide();
	$("#sign_in").click(function(){
		
		var username=$('#username').val();
		var password=$('#password').val();

		if(username == '' || password == ''){
			if(username == '' && password == ''){
			 $("#username_val").addClass('error focused');
			 $(".username_msg").html("This field is required");
			 $("#password_val").addClass('error focused');
			 $(".password_msg").html("This field is required");
			}else if(username == ''){
				$("#username_val").addClass('error focused');
		}else if(password == '' ){
			$("#password_val").addClass('error focused');
		}
		}else{

           var datastring='username='+username+'&password='+password;
           $.ajax({
               type:"POST",
               url:"<?php echo base_url();?>admin/login_check/",
               data:datastring,
               success:function(data){

                   if(data=="fail"){
                	   $('.error').html("Invalid username and password");
   				    
   				       $("#error_alert").fadeTo(2000, 500).slideUp(500, function(){
   			            $("#error_alert").slideUp(500);
   			             }); 
                   }else{
                      
                     location.reload(true);
                   }

               },
               error: function(jqXHR, textStatus) {
   		        alert( "Request failed: " + textStatus );
   		    }
                       

           });



		}
		
	});


$("#username").focusout(function(){
	var username=$("#username").val();
	if(username != ''){
		$("#username_val").removeClass('error');
		$(".username_msg").hide();
	}
	
});
	
$("#password").focusout(function(){
	var username=$("#password").val();
	if(username != ''){
		$("#password_val").removeClass('error');
		$(".password_msg").hide();
	}
	
});

	
});

</script>




<body class="login-page">

	<div class="login-box">



		<div class="card">
			<div class="header bg-pink">

				<h2 style="text-align: center">
					<div class="image">
						<h4>Login</h4>
					</div>
				</h2>

			</div>
			 <form>
			<div class="body">

				<div class="msg"> <div class="alert bg-orange" style="border-radius: 3px !important;display:none" id="error_alert">
			                  <button type="button" class="close" data-dismiss="alert">x</button>
  <strong class="error"> </strong>
</div></div>

				<div class="form-group form-float">
					<div class="form-line username_val" id="username_val">
						<input type="text" id="username" class="form-control"> <label
							class="form-label glyphicon glyphicon-user "> Username</label>

					</div>
					<div class="username_msg" style="color: red"></div>
				</div>

				<div class="form-group form-float">
					<div class="form-line" id="password_val">
						<input type="password" id="password" class="form-control"> <label
							class="form-label glyphicon glyphicon-lock"> Password</label>

					</div>
					<div class="password_msg" style="color: red"></div>
				</div>

				<div class="row">
					<div class="col-xs-8"></div>
					<div class="col-xs-4">
						<input type="button" class="btn btn-block bg-pink waves-effect" id="sign_in" value="SIGN IN">
							
					</div>
				</div>
				<div class="row m-t-15 m-b--20">
					<div class="col-xs-6">
						<a href="#">Register Now!</a>
					</div>
					<div class="col-xs-6 align-right">
						<a href="#">Forgot Password?</a>
					</div>
				</div>

			</div>
			 </form>
		</div>
	</div>
	
	 <!-- Footer -->
            <div class="legal" style="color: white;margin-top:auto !important;text-align:center">
                <div class="copyright">
                    &copy; 2017 <a style="color: #ffc107 !important" href="#" target="_blank">Talent Portal</a><b>Version: </b> 1.0.0
                </div>
               
                    
               
            </div>
            <!-- #Footer -->
	 <!-- Jquery Core Js -->
    <script src="<?php echo base_url();?>admin_assets/plugins/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core Js -->
    <script src="<?php echo base_url();?>admin_assets/plugins/bootstrap/js/bootstrap.js"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="<?php echo base_url();?>admin_assets/plugins/node-waves/waves.js"></script>

    <!-- Validation Plugin Js -->
    <script src="<?php echo base_url();?>admin_assets/plugins/jquery-validation/jquery.validate.js"></script>
<script src="<?php echo base_url();?>admin_assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <!-- Custom Js -->
    <script src="<?php echo base_url();?>admin_assets/js/admin.js"></script>
    <script src="<?php echo base_url();?>admin_assets/js/pages/examples/sign-in.js"></script>
     <script src="<?php echo base_url();?>admin_assets/js/pages/ui/notifications.js"></script>
</body>

</html>