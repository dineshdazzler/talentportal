
<style>
.mj_pagetitle2 .mj_pagetitleimg img {
	max-height: 220px;
}
</style>
<style type="text/css">
@media ( max-width :991px) {
	.mj_pagetitle2 .mj_pagetitle_inner {
		position: absolute;
		bottom: -20%;
		left: 0;
		width: 60%;
	}
	.mj_feature_product_img img {
		width: 100% !important;
	}
	iframe {
		width: 100% !important;
	}
}
</style>
<script
	src="<?php echo base_url();?>admin_assets/js/pages/forms/advanced-form-elements.js"></script>
<!-- Waves Effect Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/node-waves/waves.css"
	rel="stylesheet" />

<!-- Animation Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/animate-css/animate.css"
	rel="stylesheet" />

<!-- Colorpicker Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.css"
	rel="stylesheet" />

<!-- Dropzone Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/dropzone/dropzone.css"
	rel="stylesheet">

<!-- Multi Select Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/multi-select/css/multi-select.css"
	rel="stylesheet">

<!-- Bootstrap Spinner Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/jquery-spinner/css/bootstrap-spinner.css"
	rel="stylesheet">

<!-- Bootstrap Tagsinput Css -->
<link
	href="<?php echo base_url();?>admin_assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"
	rel="stylesheet">

<!-- Custom Css -->
<link href="<?php echo base_url();?>admin_assets/css/style.css"
	rel="stylesheet">

<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
<link
	href="<?php echo base_url();?>admin_assets/css/themes/all-themes.css"
	rel="stylesheet" />
<script
	src="http://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.5.4/bootstrap-select.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
	var countrydropdown = document.getElementById("category").value;

	 if(countrydropdown!="")
	 { 
		 loadData('act_type',countrydropdown,<?php echo $talents_info->act_type;?>);

		 }
	});
</script>
<script type="text/javascript">



function selectact(category_id){
  	if(category_id!=""){
		loadData('act_type',category_id,<?php echo  $talents_info->act_type;?>);
			
	}else{
		$("#act_type").html("<option value=''>Select act first</option>");
			
	}
}




function loadData(loadType,loadId,$id){
	    
	var dataString = 'loadType='+ loadType +'&loadId='+ loadId+'&id='+$id;
		$.ajax({
		type: "POST",
		url: "<?php echo base_url();?>admin/loaddata",
		data: dataString,
	
		success: function(result){
	      	$("#"+loadType).html("<option value=''>Select "+loadType+" first </option>");  
			$("#"+loadType).append(result);  
			$("#"+loadType).val(4);
			$("#"+loadType).selectpicker("refresh");
		}
	});
}

</script>

<script type="text/javascript">
$(document).ready(function(){
	$("#other_div").hide();

	$("#act_type").change(function(){

		var val=$("#act_type").val();

		 if(val == "others"){
             $("#other_div").show();
		 }else{
			 $("#other_div").hide();
		 }
	   
	});
	$("form#portfolio").submit(function(event){
       
		  //disable the default form submission
		  event.preventDefault();

		  //grab all form data  
		  var formData = new FormData($(this)[0]);
		 
		 $.ajax({
		    url: '<?php echo base_url();?>talents/portfolio_update',
		    type: 'POST',
		    data: formData,
		    async: false,
		    cache: false,
		    contentType: false,
		    processData: false,
		    success: function (data) {
              
           		if(data==1){
           		 $('.sucess_content').addClass('alert alert-success').html("sucessfully Updated");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           			
           			
           		}else {
           		 $('.sucess_content').addClass('alert alert-danger').html("try again later !!");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           			
           			
           		}

           	},
               error: function(jqXHR, textStatus) {
                   alert( "Request failed: " + jqXHR );
               }
           	});

               

       


   });
	
$('#home').on("click", ".docs_remove", function (e) {
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){
		var id=$(this).attr('id');
		 var dataString='id='+id;
		//$(this).closest("tr").hide();
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/docs_delete",
			data:dataString  ,
			success: function(data){
				
            if(data == 1){

            	 $('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
	 	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             }); 
	 	 		setTimeout(function(){
	 	 			location.reload(true);},3000);
           	
			
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});
//Client Edit
$('#menu3').on("click", ".edit_client", function (e) {

	var id = $(this).attr('id');

	var dataString='id='+id;
	$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>talents/client_edit/",
		data:dataString  ,
		timeout: 5000,
		success: function(data){
			$("#client_edit").modal();
			$("#rev").html(data);

		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});

});
/*Photo delete delete */
$('.photo_delete').click( function(){
	event.preventDefault();
	var r = confirm("Are you sure want to delete this?");
	if (r == true){

		

		var id=$(this).attr('id');
        var arr=id.split('_div');
        var org_photo=arr[0];
       
		var dataString='id='+org_photo;
		$.ajax({
			type: "post",
			url:"<?php echo base_url(); ?>/talents/photo_delete",
			data:dataString  ,
			success: function(data){
		  $("#"+arr[1]+"_div").hide();
            if(data == 1){
				
				$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
				  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });
              }
			},
			error: function(jqXHR, textStatus) {
				alert( "Request failed: " + jqXHR );
			}
		});
	}

});
/*You tube link delete */

$('#menu1').on("click", ".link_delete", function (e) {

event.preventDefault();
var r = confirm("Are you sure want to delete this?");
if (r == true){

	var id=$(this).attr('id');
  
	var dataString='id='+id;
	$.ajax({
		type: "post",
		url:"<?php echo base_url(); ?>/talents/youtube_delete",
		data:dataString  ,
		success: function(data){
	   $("#"+id).hide();
        if(data == 1){
        	$('.sucess_content').addClass('alert alert-success').html("Sucessfully Deleted");
			  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             });
			
          }
		},
		error: function(jqXHR, textStatus) {
			alert( "Request failed: " + jqXHR );
		}
	});
}

});
$(".approve").click(function(){
	var id=$(this).attr('id');
	

	dataString='id='+id;
	
		$.ajax({

			type: "post",	
			url:"<?php echo base_url(); ?>admin/approve/ ",
			data:dataString  ,
			success: function(data){
				alert("Successfully Approved");
				
			},
			
		});

	

});
$(".decline").click(function(){
	var id=$(this).attr('id');
	

	dataString='id='+id;
	
		$.ajax({

			type: "post",	
			url:"<?php echo base_url(); ?>admin/decline/ ",
			data:dataString  ,
			success: function(data){
				alert("Successfully Declined");
				
			},
			
		});

	

});
});

</script>
<style>
@import url(http://fonts.googleapis.com/css?family=Droid+Sans);

.upload {
	background-color: #ff0000;
	border: 1px solid #ff0000;
	color: #fff;
	border-radius: 5px;
	padding: 10px;
	text-shadow: 1px 1px 0px green;
	box-shadow: 2px 2px 15px rgba(0, 0, 0, .75);
}

.upload:hover {
	cursor: pointer;
	background: #c20b0b;
	border: 1px solid #c20b0b;
	box-shadow: 0px 0px 5px rgba(0, 0, 0, .75);
}

#file {
	color: green;
	padding: 5px;
	border: 1px dashed #123456;
	background-color: #f9ffe5;
}

#upload {
	margin-left: 45px;
}

#noerror {
	color: green;
	text-align: left;
}

#error {
	color: red;
	text-align: left;
}

#img {
	width: 40px;
	border: none;
	height: 50px;
	margin-left: -20px;
	margin-bottom: 91px;
}

.abcd {
	text-align: center;
}

.abcd img {
	height: 100px;
	width: 100px;
	padding: 5px;
	border: 1px solid rgb(232, 222, 189);
}

#formget {
	float: right;
}

.carousel-inner>.item>a>img, .carousel-inner>.item>img, .img-responsive,
	.thumbnail a>img, .thumbnail>img {
	display: block;
	max-width: 250px;
	height: 250px !important;
}

.mj_gallary_img {
	float: left;
	width: 250px;
	position: relative;
	margin: 10px;
}
</style>

<section class="content">


	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="panel panel-default">
				<div class="tab-content">
					<div id="edit_profile" class="tab-pane active">
						<div class="panel-heading"
							style="background-color: #76CE32; color: white">
							<h5>Portfolio</h5><?php if($talent_list1->status == 1){ ?><a
								id="<?php echo $talent_list1->talents_id;?>" href=""
								class="btn btn-warning btn-sm decline"><i
								class="glyphicon glyphicon-remove icon-white decline"></i>Decline</a>
								 <?php } else if($talent_list1->status == 0){?>
										<a id="<?php echo $talent_list1->talents_id;?>"
										href=""
										class="btn btn-success btn-sm approve"><i
											class="glyphicon glyphicon-ok icon-white approve"></i>Approve</a>
											 <?php }?> 
						</div>
						<div class="panel-body">

							<ul class="nav nav-tabs">
								<li class="active"><a data-toggle="tab" href="#home">Details</a></li>
								<li><a data-toggle="tab" href="#menu1">Videos</a></li>
								<li><a data-toggle="tab" href="#menu2">Photos</a></li>
								<li><a data-toggle="tab" href="#menu3">Clients</a></li>
								<li><a data-toggle="tab" href="#menu4">Pricing</a></li>
								<li><a data-toggle="tab" href="#menu5">Reviews</a></li>
							</ul>


							<div class="tab-content">
								<div id="home" class="tab-pane fade in active">
									<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
										<div class="col-md2  ">

											<form id="portfolio" enctype="multipart/form-data">
												<input type="hidden" name="pid"
													value="<?php echo base_url();?>">

												<fieldset>

													<div class="row">
														<div class="col-sm-4">
															<div class="form-group">
																<label for="particular"></label> 
																<?php
																
																$prinimage = '<img id="myimage"
		src="' . base_url () . '/theme_assets/images/default_user.png"
		class="img-circle" alt="Cinque Terre" width="200" height="200">
    	';
																if ($talents_info->id != '') {
																	
																	$files = glob ( 'talents_profilepic/*' ); // get all file names
																	foreach ( $files as $file ) {
																		if (is_file ( $file )) {
																			$picname = "talent" . $talents_info->talents_id . "pic";
																			
																			$photos_from_direc = explode ( '/', $file );
																			$photoname = explode ( '.', $photos_from_direc [1] );
																			if ($photoname [0] == $picname) {
																				$prinimage = '<img id="myimage"
		src=" ' . base_url () . 'talents_profilepic/' . $picname . '"
		class="img-circle" alt="Profile " width="200" height="200">
    	';
																			}
																			
																			// unlink($file);
																		} // delete file
																	}
																}
																
																echo $prinimage;
																?>
																	
																	 <span class="text-danger"></span>
															</div>
														</div>
														<div class="col-sm-6">
															<h3 style="color: #337ab7">Portfolio Details</h3>
															</br>
															<p>
																Choose a skill and get some cred by listing your years
																of experience, along with a short write up of how
																awesome you are. Multi-talented? Don’t worry, you can
																add more another portfolio showcasing a different skill
																later. <br> <br>*Remember, one skill per portfolio, we
																don’t want to confuse people!
															</p>
															<input type="hidden" name="t_id" id="t_id"
																value="<?php echo $id;?>">
															<div class="form-group vali_category">
																<label for="particular">Category*</label> <select
																	id="category" name="category" class=" form-control"
																	onchange="selectact(this.options[this.selectedIndex].value)">
																	<option class="form-control" value="">Select Category</option>

																			<?php
																			
																			foreach ( $talents_category as $talents ) {
																				
																				if ($talents ['id'] == $talents_info->category) {
																					?>
																		
																			<option class="form-control"
																		value="<?php echo $talents['id'];?>" selected><?php echo $talents['name'];?></option>
																			
																			<?php }else{?>
																			<option class="form-control"
																		value="<?php echo $talents['id'];?>"><?php echo $talents['name'];?></option>
																			
																			<?php }?>
																			<?php }?>
																		</select> <span class="text-danger"></span>
															</div>
															<div class="form-group vali_act">
																<label for="particular">Type of act *</label> <select
																	id="act_type" name="act_type" class=" form-control">
																	<option class="form-control" value="">Select Your act</option>


																</select> <span class="text-danger"></span>
															</div>
															<div class="form-group vali_other" id="other_div">
																<p>
																	<b>Other *</b>
																</p>
																<div class="input-group">
																	<div class="form-line">
																		<input type="text" class="form-control date"
																			placeholder="Others" id="other" name="other" value="">
																	</div>
																</div>
															</div>



															<div class="form-group vali_exp">
																<p>
																	<b>Year of experience*</b>
																</p>
																<div class="input-group">
																	<div class="form-line">
																		<input type="number" class="form-control"
																			placeholder="Others" id="exp" name="exp"
																			value="<?php echo $talents_info->experience;?>">
																	</div>
																</div>

															</div>
															<div class="form-group vali_service">
																<p>
																	<b>Sell your act/service (write up) *</b>
																</p>
																<div class="card">

																	<div class="body">

																		<textarea class="form-control" id="ckeditor"
																			name="editor1"><?php echo $talents_info->service;?></textarea>
																	</div>
																</div>


															</div>
															<div class="form-group vali_pdf">
																<label for="particular">Upload your document</label> <input
																	type="file" name="pdf" id="pdf" class="form-control"
																	accept=".pdf" /> <span class="text-danger"></span>
															</div>
															<div class="form-group ">
																
																<?php
																
																$images = glob ( 'talents_images/talents' . $id . '/portfolio/*', GLOB_NOSORT );
																foreach ( $images as $image ) {
																	
																	$exp = explode ( '/', $image );
																	$name = $exp [3];
																	?>
<a href="<?=base_url($image)?>" target="_blank"> <?php echo $name;?> &nbsp;view &nbsp;&nbsp;&nbsp;</a>

																<input type="button" class="btn btn-danger docs_remove"
																	id="<?php echo $image;?>" value="Remove"> <br> <br>
																<?php }?>
																
																
																</div>





															<div class="form-group">
																<input type="submit" name="update" id="" value="Save"
																	class="btn btn-success form-control">
															</div>
														</div>


													</div>









												</fieldset>
											</form>
											<center>
												<span class="text-success"></span>
											</center>
											<span class="text-danger"></span>
										</div>
									</div>
								</div>
								<div id="menu1" class="tab-pane fade">


									<div id="video_section"></div>
									<div id="video_exists">
                              <?php foreach($videos as $row){?>
                              
                              <div class="row"
											id="<?php  echo $row['id'];?>">

											<div class="col-sm-6">

												<div class="form-group"><?php echo $row['link'];?>
                             </div>


											</div>

											<div class="col-sm-6">
												<div class="form-group">
													<label>&nbsp; </label>
													<button class="btn btn-danger link_delete "
														id="<?php  echo $row['id'];?>" style="margin-top: 50px">Delete</button>
												</div>
											</div>


										</div>
										<hr style="border-top: 1px solid #111111;">
<?php }?>
        </div>








								</div>
								<div id="menu2" class="tab-pane fade">
									<br>
									<div class="row">
										<div class="col-sm-6" style="display: none">
											<div class="form-group">
												<a href="#" class="btn btn-success" data-toggle="modal"
													data-target="#gallery_photo"><i class="fa fa-camera"
													aria-hidden="true"></i>Add Photos </a>
											</div>
										</div>
										<div class="col-sm-6"></div>
									</div>


									<div class="row">


										<div class="col-md-12">
											<div id="mj_grid"
												style="width: :250px !important; height: 250px !important">
	
													<?php
													$talents_id = $id;
													$images = glob ( "talents_images/talents" . $talents_id . "/photos/*.*" );
													$sn = 0;
													foreach ( $images as $image ) {
														$sn ++;
														?>

                    <div class="col-md-4 mix mix-all holiday"
													id="<?php echo $sn;?>_div" data-value="1">
													<div class="row">
														<div class="mj_gallary_img">
															<img src="<?php echo base_url();?><?php echo $image;?>"
																alt="gallery" class="img-responsive">
															<div class="mj_overlay">
																<div class="mj_gallary_info">
																	<h5 class="animated fadeInDown">
																		<a class="fancybox" data-fancybox-group="gallery"
																			href="<?php echo base_url();?><?php echo $image;?>"
																			title="">View</a>
																	</h5>
																	<button class="btn btn-danger photo_delete"
																		id="<?php echo $image;?>_div<?php echo $sn;?>"
																		class="delete">Delete</button>
																</div>
															</div>
														</div>
													</div>
												</div>
           
            <?php }?>
            
             </div>
										</div>
									</div>

								</div>




								<div id="menu3" class="tab-pane fade">

									<!--		<div class="row"
											style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">

											<form id="client_form" enctype="multipart/form-data">
												<input type="hidden" name="id" value="<?php echo $id;?>">
												<div class="col-sm-6">
													<div class="form-group vali_youtube">
														<label>Client Name <span></span>
														</label> <input type="text" class="form-control"
															placeholder="Enter Client Name" name="name"
															id="client_name">
													</div>

												</div>
												<div class="col-sm-4">
													<div class="form-group vali_youtube">
														<label>Photo<span></span>
														</label> <input type="file" class="form-control"
															id="client_photo" name="client_photo" accept="image/*">
													</div>

												</div>
												<div class="col-sm-2">
													<div class="form-group">
														<label>&nbsp; </label> <input type="submit"
															class="btn btn-success form-control" id="" value="Add">
													</div>
												</div>

											</form>
										</div> -->

									<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">


										<table class="table table-hover" id="client_list">
											<thead>
												<tr>
													<th>Client Name</th>
													<th>Logo</th>
													<th>Actions</th>
												</tr>
											</thead>
											<tbody id="cleint_exists">
                                                <?php foreach($clients as $row){?>
													<tr>
													<td><?php echo $row['client_name'];?></td>
													<td><img
														src="<?php echo base_url();?>talents_images/talents<?php echo $id;?>/clients/<?php echo $row['logo']; ?>"
														width="150" height="150"></td>
													<td>
												<!-- 		<button type="button" class="btn btn-info edit_client"
															id="<?php echo $row['id'];?>" data-toggle="modal"
															data-target="#client_edit">
															<span class="glyphicon glyphicon-edit"></span> Edit
														</button> -->
														<button type="button" class="btn btn-danger client_delete"
															id="<?php echo $row['id'];?>">
															<span class="glyphicon glyphicon-trash"></span> Delete
														</button>
													</td>
												</tr>
													<?php }?>
												</tbody>
											<tbody id="cleint_new"></tbody>

											<tbody id="cleint_update"></tbody>
										</table>


									</div>
								</div>

								<div id="menu4" class="tab-pane fade">
									<h3>Packages</h3>
									<!--  	<div class="row">
										<div class="col-sm-6"></div>
										<div class="col-sm-2">
											<button class="btn btn-info " id="add_package">Add New</button>
										</div>

										<div class="col-sm-4">
											<button class="btn btn-danger " id="file_package"
												data-toggle="modal" data-target="#price_file">Upload Price
												Pakage</button>
										</div>
									</div> -->
										
											
										
											
											<?php foreach($price as$row){?>
											
														<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
										<div class="panel-group" id="">
											<div class="panel panel-info">
												<div class="panel-heading">
													<h4 class="panel-title" style="color: black">
														<a data-toggle="collapse"
															href="#collapse<?php echo $row['id'];?>" id=""><?php echo $row['title'];?></a>
													</h4>
												</div>
												<div id="collapse<?php echo $row['id'];?>"
													class="panel-collapse collapse">

													<div class="panel-body">

														<input type="hidden" name="tid" id="tid"
															value="<?php echo $id;?>"> <input type="hidden"
															name="price_id" id="price_id<?php echo $row['id'];?>"
															value="<?php echo $row['id'];?>">
														<div class="col-sm-12">
															<div class="form-group ">
																<label>Title * <span></span>
																</label> <input type="text" class="form-control"
																	placeholder="Enter title here" name="title"
																	id="price_title<?php echo $row['id'];?>"
																	value="<?php echo $row['title'];?>">
															</div>

														</div>
														<div class="col-sm-12">
															<div class="form-group ">
																<label>Description<span></span>
																</label>
																<textarea class="form-control" rows="5"
																	id="price_description<?php echo $row['id'];?>"><?php echo $row['description'];?></textarea>
															</div>

														</div>
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group ">
																	<label for="particular">CANCELLATION POLICY </label>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group ">
																	<input type="radio" class="policy "
																		name="policy<?php echo $row['id'];?>"
																		id="Flexible<?php echo $row['id'];?>" value="Flexible"
																		<?php if($row['cancel_policy']=="Flexible") echo "checked";?>><label
																		for="Flexible<?php echo $row['id'];?>">Flexible</label>
																</div>
															</div>

															<div class="col-sm-4">
																<div class="form-group ">
																	<input type="radio" class="policy "
																		name="policy<?php echo $row['id'];?>"
																		id="Moderate<?php echo $row['id'];?>" value="Moderate"
																		<?php if($row['cancel_policy']=="Moderate") echo "checked";?>><label
																		for="Moderate<?php echo $row['id'];?>">Moderate</label>
																</div>
															</div>

															<div class="col-sm-4">
																<div class="form-group ">
																	<input type="radio" class="policy "
																		name="policy<?php echo $row['id'];?>"
																		id="Strict<?php echo $row['id'];?>" value="Strict"
																		<?php if($row['cancel_policy']=="Strict") echo "checked";?>><label
																		for="Strict<?php echo $row['id'];?>">Strict</label>
																</div>
															</div>
														</div>

														<br>
														<div class="row">
															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Charge Type<span id="type"> </span>
																	</label><select class=" form-control"
																		id="charge_type<?php echo $row['id'];?>">

																		<option value="per day"
																			<?php if($row['charge_type']=="per day") echo "selected";?>>Per
																			ful day</option>
																		<option value="per half day"
																			<?php if($row['charge_type']=="per half day") echo "selected";?>>Per
																			half day</option>
																		<option value="per hour"
																			<?php if($row['charge_type']=="per hour") echo "selected";?>>Per
																			hour</option>
																		<option value="Per package"
																			<?php if($row['charge_type']=="Per package") echo "selected";?>>Per
																			package</option>
																		<option value="Per set"
																			<?php if($row['charge_type']=="Per set") echo "selected";?>>Per
																			set</option>
																		<option value="Per show"
																			<?php if($row['charge_type']=="Per show") echo "selected";?>>Per
																			show</option>
																		<option value="Per walk"
																			<?php if($row['charge_type']=="Per walk") echo "selected";?>>Per
																			walk</option>

																	</select>
																</div>

															</div>

															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Minimum<span> </span>
																	</label> <input type="number" class="form-control"
																		placeholder="Enter mininum" name="min"
																		id="min<?php echo $row['id'];?>"
																		value="<?php echo $row['min'];?>">

																</div>

															</div>
															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Maximum<span> </span>
																	</label> <input type="number" class="form-control"
																		placeholder="Enter maximum" name="max"
																		id="max<?php echo $row['id'];?>"
																		value="<?php echo $row['max'];?>">

																</div>

															</div>
														</div>

														<div class="row">
															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Price<span> (SGD)</span>
																	</label> <input type="number" class="form-control"
																		placeholder="" name="package_price"
																		id="package_price<?php echo $row['id'];?>"
																		value="<?php echo $row['price'];?>">
																</div>

															</div>

															<div class="col-sm-4">
																<div class="form-group vali_youtube">
																	<label>Talent Portal Fees<span> </span>
																	</label> <input type="text" class="form-control"
																		placeholder="" name="percentage"
																		id="percentage<?php echo $row['id'];?>" value="15%"
																		readonly>

																</div>

															</div>
															<div class="col-sm-4">
																<div class="form-group vali_youtube">
																	<label>Talent Reciveable (SGD)<span> </span>
																	</label> <input type="number" class="form-control"
																		placeholder="" name="total"
																		id="total_price<?php echo $row['id'];?>" readonly
																		value="<?php echo $row['talent_receiveable'];?>">

																</div>

															</div>
														</div>
														<br> <br>
														<div class="row">

															<div class="col-sm-6">

																<div class="form-group ">
																	<button style="float: left"
																		class="btn btn-danger delete_price"
																		id="<?php echo $row['id'];?>">Delete Package</button>

																</div>
															</div>

															<div class="col-sm-6">
																<button style="float: right"
																	class="btn btn-success update_price"
																	id="<?php echo $row['id'];?>">Update Package</button>
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>

									</div>
											
											
											
											
											
								<?php }?>			
											<h3>Pdf files</h3>
									<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
										<div class="col-sm-12">

											<div class="form-group ">
																
																<?php
																
																$images = glob ( 'talents_images/talents' . $id . '/price/*', GLOB_NOSORT );
																foreach ( $images as $image ) {
																	
																	$exp = explode ( '/', $image );
																	$name = $exp [3];
																	?>
<a href="<?=base_url($image)?>" target="_blank"> <?php echo $name;?> &nbsp;view &nbsp;&nbsp;&nbsp;</a>

												<button class="btn btn-danger price_remove"
													id="<?php echo $image;?>">Remove</button>
												<br> <br>
																<?php }?>
																
																
																</div>
										</div>
									</div>





									<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">
										<div class="panel-group" id="package_panel">
											<div class="panel panel-default">
												<div class="panel-heading">
													<h4 class="panel-title">
														<a data-toggle="collapse" href="#collapse0" id="title">Title
															Here</a>
													</h4>
												</div>
												<div id="collapse0" class="panel-collapse collapse">
													<div class="panel-body">

														<input type="hidden" name="tid" id="tid"
															value="<?php echo $id;?>">
														<div class="col-sm-12">
															<div class="form-group ">
																<label>Title * <span></span>
																</label> <input type="text" class="form-control"
																	placeholder="Enter title here" name="title"
																	id="price_title">
															</div>

														</div>
														<div class="col-sm-12">
															<div class="form-group ">
																<label>Description<span></span>
																</label>
																<textarea class="form-control" rows="5"
																	id="price_description"></textarea>
															</div>

														</div>
														<div class="row">
															<div class="col-sm-12">
																<div class="form-group ">
																	<label for="particular">CANCELLATION POLICY </label>
																</div>
															</div>
															<div class="col-sm-4">
																<div class="form-group ">
																	<input type="radio" class="policy " name="policy"
																		id="Flexible" value="Flexible"><label for="Flexible">Flexible</label>
																</div>
															</div>

															<div class="col-sm-4">
																<div class="form-group ">
																	<input type="radio" class="policy " name="policy"
																		id="Moderate" value="Moderate"><label for="Moderate">Moderate</label>
																</div>
															</div>

															<div class="col-sm-4">
																<div class="form-group ">
																	<input type="radio" class="policy " name="policy"
																		id="Strict" value="Strict"><label for="Strict">Strict</label>
																</div>
															</div>
														</div>

														<br>
														<div class="row">
															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Charge Type<span id="type"> </span>
																	</label><select class=" form-control" id="charge_type">


																		<option value="per day">Per ful day</option>
																		<option value="per half day">Per half day</option>
																		<option value="per hour" selected>Per hour</option>
																		<option value="Per package">Per package</option>
																		<option value="Per set">Per set</option>
																		<option value="Per show">Per show</option>
																		<option value="Per walk">Per walk</option>

																	</select>
																</div>

															</div>

															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Minimum<span> </span>
																	</label> <input type="number" class="form-control"
																		placeholder="Enter mininum" name="min" id="min">

																</div>

															</div>
															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Maximum<span> </span>
																	</label> <input type="number" class="form-control"
																		placeholder="Enter maximum" name="max" id="max">

																</div>

															</div>
														</div>

														<div class="row">
															<div class="col-sm-4">
																<div class="form-group ">
																	<label>Price<span> (SGD)</span>
																	</label> <input type="number" class="form-control"
																		placeholder="" name="package_price" id="package_price">
																</div>

															</div>

															<div class="col-sm-4">
																<div class="form-group vali_youtube">
																	<label>Talent Portal Fees<span> </span>
																	</label> <input type="text" class="form-control"
																		placeholder="" name="percentage" id="percentage"
																		value="15%" readonly>

																</div>

															</div>
															<div class="col-sm-4">
																<div class="form-group vali_youtube">
																	<label>Talent Reciveable (SGD)<span> </span>
																	</label> <input type="number" class="form-control"
																		placeholder="" name="total" id="total_price" readonly>

																</div>

															</div>
														</div>
														<br> <br>
														<div class="row">

															<div class="col-sm-6">

																<div class="form-group ">
																	<label for="check4"> <input type="checkbox" id="check4"
																		name="checkbox"> <span> I have read, understand and
																			agree to the Talent Portal Terms of Service,
																			including the <a href="#">User Agreement</a> and <a
																			href="#">Privacy Policy</a>.
																	</span></label>
																</div>
															</div>

															<div class="col-sm-6">
																<button style="float: right" class="btn btn-success"
																	id="save_price" disabled>Save Package</button>
															</div>

														</div>
													</div>

												</div>
											</div>
										</div>

									</div>


								</div>


								<div id="menu5" class="tab-pane fade">
									<h3>Your Reviews</h3>
									<div class="row"
										style="padding-left: 50px; margin-right: 50px; padding-top: 20px;">


									</div>


								</div>


							</div>
						</div>
					</div>

					<!-- --Edit restaurant div  -->


				</div>
			</div>
		</div>
	</div>
	</div>
</section>
<div class="modal fade" id="modal_form" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h3 class="modal-title">Person Form</h3>
			</div>
			<div class="modal-body form">
				<form action="#" id="form" class="form-horizontal">
					<input type="hidden" value="<?php echo $id;?>" name="tid" />
					<div class="form-body">
						<div id="photo_upload">
							<div class="mbot error-message-highlight hidden"
								id="progress_report_status"
								style="font-size: 12px; text-transform: none;"></div>
							<label for="profile_photo_upload">Upload Picture</label>
							<div class="">

								<!-- Conditional comments are dropped from IE post IE 10 -->
								<input id="profile_photo_upload" name="photo" type="file"
									class="file-up">

							</div>

							<div
								style="font-size: 11px; line-height: 18px; margin-top: 10px; color: #aeaeae;">
								JPG/PNG formats only<br>Maximum size 5 MB<br>Greater than 400px
								in height and width
							</div>
							<div class="upload_status"></div>

							<div id="progress_report" style="">
								<div id="progress_report_name"></div>

								<div id="progress_report_bar_container"
									style="height: 5px; margin-top: 10px">
									<div id="progress_report_bar"
										style="background-color: #cb202d; width: 0; height: 100%;"></div>
								</div>
							</div>
						</div>

					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSave" onclick="save()"
					class="btn btn-primary">Upload</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<!-- End Bootstrap modal -->

<div class="modal fade" id="gallery_photo" tabindex="-1" role="dialog"
	aria-labelledby="gallery_photo">
	<div class="modal-dialog" role="document">
		<!-- Modal Content Starts -->
		<div class="modal-content">
			<!-- Modal Header Starts -->

			<!-- Modal Header Ends -->
			<!-- Modal Body Starts -->
			<div class="modal-body">
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<div id="gallery_form">
					Photo Upload

					<form method="POST"
						action="<?php echo base_url();?>talents/photo_upload"
						enctype="multipart/form-data">
						<input type="hidden" value="<?php echo $id;?>" name="tid" />

						<div id="filediv">
							<input name="photo[]" id="file" type="file" />
						</div>
						<br /> <input type="button" id="add_more"
							class="upload btn btn-info" value="Add More Files" /> <input
							type="submit" value="Upload File" id="upload"
							class="upload btn btn-success" />
					</form>

				</div>
			</div>
			<!-- Modal Body Ends -->
		</div>
		<!-- Modal Content Ends -->
	</div>
</div>


<div class="modal fade" id="client_edit" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

				</button>
				<h4>Edit Client</h4>
			</div>


			<form id="edit_client_form" enctype="multipart/form-data">
				<div class="reviews-form-box" id="rev"></div>
			</form>



		</div>
	</div>
</div>


<div class="modal fade" id="price_file" tabindex="-1" role="dialog"
	aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content"></div>
	</div>
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal">
					<span aria-hidden="true" class="">× </span><span class="sr-only"><?php echo $this->lang->line('Close'); ?></span>

				</button>
				<h4>Upload file</h4>
			</div>


			<form id="price_upload" method="POST"
				action="<?php echo base_url();?>talents/price_docsupload"
				enctype="multipart/form-data">
				<br> <br>
				<div class="row">
					<div class="col-sm-2"></div>
					<div class="col-sm-6">
						<div class="form-group ">
							<input type="hidden" value="<?php echo $id;?>" name="tid" /> <input
								type="file" name="price_pdf" class="form-control" accept=".pdf">

						</div>
					</div>
					<div class="col-sm-4">

						<div class="form-group ">
							<input type="submit" class="btn btn-success" name="submit"
								value="Save">
						</div>
					</div>
				</div>






			</form>



		</div>
	</div>



	<script type="text/javascript">
var abc = 0; //Declaring and defining global increement variable

$(document).ready(function() {

//To add new input file field dynamically, on click of "Add More Files" button below function will be executed
    $('#add_more').click(function() {
        $(this).before($("<div/>", {id: 'filediv'}).fadeIn('slow').append(
                $("<input/>", {name: 'photo[]', type: 'file', id: 'file'}),        
                $("<br/><br/>")
                ));
    });

//following function will executes on change event of file input to select different file	
$('body').on('change', '#file', function(){
            if (this.files && this.files[0]) {
                 abc += 1; //increementing global variable by 1
				
				var z = abc - 1;
                var x = $(this).parent().find('#previewimg' + z).remove();
                $(this).before("<div id='abcd"+ abc +"' class='abcd'><img id='previewimg" + abc + "' src=''/ width='150' height='150'></div>");
               
			    var reader = new FileReader();
                reader.onload = imageIsLoaded;
                
                reader.readAsDataURL(this.files[0]);
               
			    $(this).hide();
                $("#abcd"+ abc).append($("<img/>", {id: 'img', src: '<?php echo base_url();?>theme_assets/images/x.png', alt: 'delete',width:'30',height:'40'}).click(function() {
                $(this).parent().parent().remove();
                }));
            }
        });

//To preview image     
    function imageIsLoaded(e) {
       
       
        $('#previewimg' + abc).attr('src', e.target.result);
    };

    $('#upload').click(function(e) {
      
        var name = $(":photo").val();
        alert(name);
        if (!name)
        {
            alert("First Image Must Be Selected");
            e.preventDefault();
        }
    });
});


</script>