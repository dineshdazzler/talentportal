
<section class="content">
	<div class="container-fluid">
		<div class="block-header">
			<h2>Event Detail</h2>
		</div>
		<!-- Basic Example -->
		<div class="row clearfix">

			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
				<div class="card">
					<div class="header bg-red">
						<h2>
							Event Details <small></small>
						</h2>

					</div>
					<div class="body">
						<div class="row" style="font-size: 16px">
							<div class="col-sm-3">
							   <?php 
                                        if(file_exists("event_images/client$id/event".$event_detail->event_id."pic")){
                                        	$fileName = "client$id/event".$event_detail->event_id."pic";
                                        }
                                        	else{
                                        		$fileName = "events.png";
                                        	}
                                        		
                                        
                                        
                                        
                                        ?>
								<img
									src="<?php echo base_url();?>event_images/<?php echo $fileName; ?>"
									class="img-responsive" alt="" style="height: 200px !important">
							</div>
							<div class="col-sm-4">
								<b>Event Title:</b> <?php echo $event_detail->event_title;?>
							</div>
							<div class="col-sm-4">
								<b>Event Type:</b>  <?php echo $event_detail->event_type;?>
							</div>
							<div class="col-sm-4">
								<b>Event Start:</b>  <?php echo $event_detail->event_start;?>
							</div>
							<div class="col-sm-4">
								<b>Event End:</b>  <?php echo $event_detail->event_end;?>
							</div>
							<div class="col-sm-4">
								<b>Venue:</b>  <?php echo $event_detail->venue;?>
							</div>
							<div class="col-sm-4">
								<b>Budget:</b>  <?php echo $event_detail->budget;?>
							</div>
							<div class="col-sm-4">
								<b>Pax:</b>  <?php echo $event_detail->pax;?>
							</div>
							<div class="col-sm-4">
								<b>Attire:</b>  <?php echo $event_detail->attire;?>
							</div>
							<div class="col-sm-4">
								<b>Status:</b> <?php if ($event_detail->status==0){?><span class="label label-info">Pending</span><?php }else if($event_detail->status==1){?><span class="label label-success">Accepted</span><?php }else if($event_detail->status==2){?><span class="label label-danger">Reject by client</span><?php }else if($event_detail->status==3){ ?><span class="label label-danger">Rejected by talent</span><?php }?>
							</div>
						</div>
					</div>
				</div>
			</div>


		</div>
		<div class="row">

			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="card">
					<div class="header bg-orange">
						<h2>
							Client Details <small></small>
						</h2>

					</div>
					<div class="body">
						<div class="row"
							style="font-size: 16px; text-align: left !important">
							<div class="col-sm-4">
								<b>Client Name:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->client_name;?></div>
							<div class="col-sm-4">
								<b>Mail id:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->client_email;?></div>
							<div class="col-sm-4">
								<b>Mobile Number:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->client_phone;?></div>
							<div class="col-sm-4">
								<b>Country:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->client_country;?></div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
				<div class="card">
					<div class="header bg-blue-grey">
						<h2>
							Talent Provider Details<small></small>
						</h2>

					</div>
					<div class="body">
					<div class="row"
							style="font-size: 16px; text-align: left !important">
							<div class="col-sm-4">
								<b>Talent Name:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->talent_name;?></div>
							<div class="col-sm-4">
								<b>Mail id:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->talent_email;?></div>
							<div class="col-sm-4">
								<b>Mobile Number:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->talent_number;?></div>
							<div class="col-sm-4">
								<b>Act type:</b>
							</div>
							<div class="col-sm-8"><?php echo $event_detail->act_type;?></div>
						</div>
					</div>
				</div>
			</div>


			<!-- #END# Basic Example -->


			<!-- #END# Colored Card - With Loading -->
		</div>

</section>