<script type="text/javascript">
$(document).ready(function(){

	$(".accepted_jobs").dataTable();


	
});
</script>  
  
  
  <section class="content">
        <div class="container-fluid">

<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Event Details
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#profile_animation_1" data-toggle="tab"><i class="material-icons">event_available</i>Accepted Jobs</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab"><i class="material-icons">event_busy</i>Declined Jobs</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane   active" id="profile_animation_1">
                                        
                                         <b>Job Details</b>
                                              <table class="table table-bordered table-striped table-hover js-basic-example accepted_jobs">
                                <thead>
													<tr>
														<th>S.No</th>
														<th>Event Details</th>

														<th>Total Hr's</th>
														<th>Price</th>
														<th>Location</th>
														<th>action</th>

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($accepted_jobs as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><b>Event:</b><?php echo $row['event_name'];?></br>
														<b>Start:</b><?php echo $row['event_start'];?></br>
														<b>End:</b><?php echo $row['event_end'];?>
														
														
														
														</td>
														<td><?php echo $row['total_hours'];?></td>
														<td><?php echo $row['talents_cost'];?></td>
														<td><?php echo $row['venue'];?></td>
														<td><a href="<?php echo base_url();?>admin/event_detail/<?php echo $row['detail_id'];?>" class="btn btn-success btn-sm job_accept" id="<?php echo $row['event_id'];?>">View Talent & Client Details</a>


													</tr>

<?php }?>

												</tbody>
											</table>
                                        </div>
                                       
                                        <div role="tabpanel" class="tab-pane animated " id="messages_animation_1">
                                            <b>Message Content</b>
                                           <table class="table table-bordered table-striped table-hover js-basic-example accepted_jobs">
                                <thead>
													<tr>
														<th>S.No</th>
														<th>Event Details</th>

														<th>Total Hr's</th>
														<th>Price</th>
														<th>Location</th>
														<th>action</th>

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($declined_jobs as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><b>Event:</b><?php echo $row['event_name'];?></br>
														<b>Start:</b><?php echo $row['event_start'];?></br>
														<b>End:</b><?php echo $row['event_end'];?>
														
														
														
														</td>
														<td><?php echo $row['total_hours'];?></td>
														<td><?php echo $row['talents_cost'];?></td>
														<td><?php echo $row['venue'];?></td>
														<td><a href="<?php echo base_url();?>admin/event_detail/<?php echo $row['detail_id'];?>" class="btn btn-success btn-sm job_accept" id="<?php echo $row['event_id'];?>">View Talent & Client Details</a>


													</tr>

<?php }?>

												</tbody>
											</table>
                                        </div>
                                       
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </section>