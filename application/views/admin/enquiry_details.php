<script type="text/javascript">
$(document).ready(function(){

	$(".enquiry_table").dataTable();


	
});
</script>  
  
  
  <section class="content">
        <div class="container-fluid">

<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                Enquiry Details 
                            </h2>
                            <ul class="header-dropdown m-r--5">
                                
                            </ul>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#profile_animation_1" data-toggle="tab"><i class="material-icons">account_circle</i>Enquiry from Company</a></li>
                                        <li role="presentation"><a href="#messages_animation_1" data-toggle="tab"><i class="material-icons">account_box</i>Enquiry from talent</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane   active" id="profile_animation_1">
                                        
                                         <b>Enquiry Details</b>
                                              <table class="table table-bordered table-striped table-hover js-basic-example enquiry_table">
                                <thead>
													<tr>
														<th>S.No</th>
														<th>Name</th>
														<th>Email</th>
														<th>Contact Number</th>
														<th>Enq Type</th>
														<th>Mesage</th>

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($company as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><?php echo $row['name'];?></td>
														<td><?php echo $row['email'];?></td>
														<td><?php echo $row['contact_number'];?></td>
														<td><?php echo $row['type'];?></td>
														<td><?php echo $row['message'];?></td>

													</tr>

<?php }?>

												</tbody>
											</table>
                                        </div>
                                       
                                        <div role="tabpanel" class="tab-pane animated " id="messages_animation_1">
                                            <b>Enquiry Details </b>
                                           <table class="table table-bordered table-striped table-hover js-basic-example enquiry_table">
                                <thead>
													<tr>
														<th>S.No</th>
														<th>Name</th>
														<th>ACt Type</th>
														<th>Email</th>
														<th>Phone</th>
														<th>Other details</th>

													</tr>
												</thead>
												<tbody>
													
													<?php $count=0;
													foreach($talent as $row){
													
														$count++;
													?>
													<tr>
														<td><?php echo $count;?></td>
														<td><?php echo $row['name'];?></td>
														<td><?php echo $row['act_type'];?></td>
														<td><?php echo $row['email'];?></td>
														<td><?php echo $row['phone'];?></td>
														<td><?php echo $row['email1'];?> - <?php echo $row['phone1'];?> </td>

													</tr>

<?php }?>

												</tbody>
											</table>
                                        </div>
                                       
                                    </div>
                                </div>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
            </section>