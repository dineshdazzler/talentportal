<script>   //no need to specify the language
 $(function(){
  $("#send").click(function(e){  // passing down the event 
	
		var name=$('#name').val();
		 var email	=$('#email').val();
			var contact_number=$('#contact_number').val();
			var enquiry=$('#enquiry').val();
			  var message	=$('#message').val();
			var emailFilter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
			if( name == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Name");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( email == '' || !emailFilter.test(email)){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Valid Email");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( contact_number == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Contact Number");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( enquiry == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Enquiry Type");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else if( message == ''){
		          $('.sucess_content').addClass('alert alert-danger').html("Please Enter Your Message");
		 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
			            $("#sucess_div").slideUp(500);
			             });

		 	}
			else{
    $.ajax({
       url:'<?php echo base_url();?>home/send_mail',
       type: 'POST',
       data: $("#company_contact").serialize(),
       success: function(){
    	   $('.sucess_content').addClass('alert alert-success').html("Message Successfully Sent");
	 		  $("#sucess_div").fadeTo(2000, 500).slideUp(500, function(){
		            $("#sucess_div").slideUp(500);
		             });
	 		 setTimeout(function(){
	 	 			window.location.href="<?php echo base_url();?>home/contactus_company" ;},1000);
	 		
       },
       error: function(){
           alert("Fail")
       }
   });
			}
   e.preventDefault(); // could also use: return false;
   });
});
</script>
<div  id="sucess_div"
	style="position: fixed; z-index: 10004; top: 30px; right: 0px;">
	<h4 style="white-space: nowrap;" class="sucess_content"></h4>
	
</div>
<div class="container">
	<div class="row">
		<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="">
				<div
					class="">
					<div class="mj_mainheading mj_toppadder80 mj_bottompadder50">
						<h3>
							C<span>ontact</span> F<span>orm</span>
						</h3>
						<p></p>
					</div>
				</div>
				<div
					class="">
					<div class="row">
						<form id="company_contact" class="mj_bottompadder80">
							<div class="form-group col-lg-6 name">
							<label>Name</label>
								<input type="text" name="name" class="form-control" id="name" placeholder="Name">
							</div>
							<div class="form-group col-lg-6 email">
							<label>Email</label>
								<input type="email" name="email" id="email" class="form-control"
									placeholder="Email Address">
							</div>
							<div class="form-group col-lg-6 contact_number">
							<label>Contact Number</label>
								<input type="text" name="contact_number" id="contact_number" class="form-control" placeholder="Contact NUmber">
							</div>
							<div class="form-group col-lg-6 enquiry">
							<label>Enquiry Type</label>
                                    <select name="enquiry" id="enquiry" class="form-control">
                                        <option value="">Choose a Enquiry type</option>
                                        <option value="Technical Issues">Technical Issues</option>
                                        <option value="Suggestion">Suggestion</option>
                                        <option value="General Feedback">General Feedback</option>
                                    </select>
                     
                                </div>
							<div class="form-group col-lg-12 message">
							<label>Message</label>
								<textarea placeholder="Message" id="message" name="message" class="form-control tr_textarea"
									 rows="7"></textarea>
							</div>
							<center><button type="button" id="send" class="btn btn-success">send a message</button></center>
							
						</form>
					</div>
				</div>
			</div>
		</div>

		
	<div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
			<div class="mj_mainheading mj_toppadder80 mj_bottompadder50">
				<h3>
					c<span>ontact</span> i<span>nformation</span>
				</h3>
				
			</div>
			     <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                   <h4>Singapore Office:</h4>
				<p style="font-size: 15px;">31 Woodlands Close,</p>
				<p style="font-size: 15px;">Woodlands Horizon Bizhub,</p>
				<p style="font-size: 15px;">#06-01 Singapore 737855.</p>
			<h5>TEL/FAX:</h5>	
				<p style="font-size: 15px;">Company Mobile : (65) 98455562</p>
                                </div>
                                
                                <div class="col-lg-6 col-md-4 col-sm-12 col-xs-12">
                                    <h4>Malaysia Office:</h4>
				<p style="font-size: 15px;">27 A & B, Jalan Sutera Tanjung 8/3,</p>
				<p style="font-size: 15px;">Taman Suetra Utama 81300 Skudai,</p>
				<p style="font-size: 15px;">Johor Bahru Malaysia.</p>
			<h5>TEL/FAX:</h5>	
				<p style="font-size: 15px;">(607) 558 1812</p>
			<h5>Email:</h5>	
				<p style="font-size: 15px;"><a href="admin@heartneucircus.com" target="_top">admin@heartneucircus.com</a></p>
				
                                </div>
			</div>

	</div>
	
</div>


       