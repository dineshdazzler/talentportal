
 <div class="mj_top_footer  " >
        <div class="container" style="margin-bottom: 10px">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
					<div class="mj_weight_wrapper ">
                       
						<div class="mj_sociallink">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-behance"></i></a>
                                </li>
                                <li><a href="#"><i class="fa fa-dribbble"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="margin-top: 12px;text-align:left"> 
					<div class="mj_weight_wrapper ">
					<p>	 © 2017  All rights reserved  <a href="#">Heartnewcircus</a></p>
					</div>
				</div>
				
				
            </div>
          
        </div>
    </div>
	

     <!-- Script Start -->
  
    <script src="<?php echo base_url();?>theme_assets/js/bootstrap.js" type="text/javascript"></script>
     <link rel="stylesheet"
href="<?php echo base_url(); ?>theme_assets/css/jquery.dataTables.min.css">
<script type="text/javascript"
src="<?php echo base_url(); ?>theme_assets/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo base_url();?>theme_assets/js/modernizr.custom.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url();?>theme_assets/js/plugins/rsslider/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>theme_assets/js/plugins/rsslider/jquery.themepunch.revolution.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>theme_assets/js/plugins/rsslider/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>theme_assets/js/plugins/rsslider/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>theme_assets/js/plugins/rsslider/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>theme_assets/js/plugins/rsslider/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/countto/jquery.countTo.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/owl/owl.carousel.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/bootstrap-slider/bootstrap-slider.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/fancybox/jquery.fancybox.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>theme_assets/js/jquery.mixitup.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/jquery-ui/jquery-ui.js"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/isotop/isotope.pkgd.js"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/ckeditor/ckeditor.js"></script>
    <script src="<?php echo base_url();?>theme_assets/js/plugins/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo base_url();?>theme_assets/js/custom.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>theme_assets/js/notify.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>theme_assets/js/admin.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>theme_assets/js/notifications.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>theme_assets/js/alert.js" type="text/javascript"></script>
</body>

</html>